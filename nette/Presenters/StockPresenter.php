<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\Queries\IngredientsQuery;
use App\Entities\Warehouses\Stock;
use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\InventoryCheck;
use App\Services\DateService;
use Nette\Utils\DateTime;



class StockPresenter extends BasePresenter
{

    /**
     * @var DateTime
     */
    private $date;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->date = $this->date;
        $this->template->dayBefore = $this->date->modifyClone('-1 day');
        $this->template->dayAfter = $this->date->modifyClone('+1 day');
        $this->template->today = (new DateTime())->setTime(0, 0, 0);
    }



    public function actionDefault(string $date = null)
    {
        $this->date = DateService::parseDateOrToday($date);

        $this->prefetchIngredients();
        $this->template->stocks = $this->fetchStocks();
    }



    /**
     * @return Ingredient[]
     */
    private function prefetchIngredients() : array
    {
        return $this->entityManager->fetch(new IngredientsQuery())->toArray();
    }



    /**
     * @return Stock[]
     */
    private function fetchStocks() : array
    {
        $qb = $this->entityManager->getRepository(Stock::class)->createQueryBuilder('es')
            ->addSelect('esi')
            ->addSelect('w')
            ->leftJoin('es.items', 'esi')
            ->leftJoin('es.warehouse', 'w')
            ->leftJoin('esi.ingredient', 'i')
            ->andWhere('es.date = :date')->setParameter('date', $this->date)
            ->andWhere('i.deleted = false')
            ->orderBy('es.warehouse', 'ASC')
            ->addOrderBy('esi.ingredient', 'ASC');

        if (!$this->getEmployee()->isManagement()) {
            $qb->andWhere('i.onlyInCentralWarehouse = FALSE');
        }

        return $qb->getQuery()->getResult();
    }

}
