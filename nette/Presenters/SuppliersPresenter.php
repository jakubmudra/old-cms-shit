<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Employees\Employee;
use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\InventoryItem;
use App\Entities\Warehouses\Recipe;
use App\Entities\Warehouses\Replenishment;
use App\Entities\Warehouses\Services\WarehouseService;
use App\Entities\Warehouses\Supplier;
use App\Forms\BaseForm;
use App\Helpers\Filters;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\QueryBuilder;
use Nette\Forms\Form;
use Nette\Utils\ArrayHash;
use Ublaboo\DataGrid\DataGrid;



class SuppliersPresenter extends BasePresenter
{

    /**
     * @inject
     * @var WarehouseService
     */
    public $warehouseService;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isManagement()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function renderDefault()
    {
        $this->template->suppliers = $this->entityManager->getRepository(Supplier::class)->findAll();
    }



    public function renderDetail(int $id)
    {
        $this->template->supplier = $this->getSupplier();
    }



    protected function createComponentForm()
    {
        /** @var Supplier|NULL $supplier */
        $supplier = NULL;

        if ($this->getParameter('id') !== NULL) {
            $supplier = $this->entityManager->find(Supplier::class, $this->getParameter('id'));
        }

        $form = new BaseForm();
        $form->addProtection();

        $form->addText('companyRegistrationNumber', 'IČO')
            ->setRequired('IČO je povinné');

        $form->addText('name', 'Název')
            ->setRequired('Název je povinný');

        $form->addText('taxId', 'DIČ');

        $form->addCheckbox('taxPayer', 'Plátce DPH');

        $form->addSubmit('send', 'Uložit');

        if ($supplier !== NULL) {
            $form['companyRegistrationNumber']->setDefaultValue($supplier->getCompanyRegistrationNumber());
            $form['name']->setDefaultValue($supplier->getName());
            $form['taxId']->setDefaultValue($supplier->getTaxId());
            $form['taxPayer']->setDefaultValue($supplier->isTaxPayer());
        }

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) use ($supplier) {
            if ($supplier === NULL) {
                $supplier = new Supplier($values->companyRegistrationNumber);
            }

            $supplier->setCompanyRegistrationNumber($values->companyRegistrationNumber);
            $supplier->setTaxId($values->taxId);
            $supplier->setName($values->name);
            $supplier->setTaxPayer($values->taxPayer);

            $this->entityManager->persist($supplier);
            $this->entityManager->flush();

            $this->successFlashMessage('Dodavatel uložen');
            $this->redirect('default');
        };

        return $form;
    }



    public function handleRemove(int $id)
    {
        $supplier = $this->getSupplier();

        try {
            $this->entityManager->remove($supplier);
            $this->entityManager->flush();

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->dangerFlashMessage('Nejprve musíte daného dodavatele odstranit ze všech naskladnění.');
            $this->redirect('this');
        }

        $this->successFlashMessage(sprintf('Dodavatel "%s %s" odstraněn.', $supplier->getCompanyRegistrationNumber(), $supplier->getName()));
        $this->redirect('this');
    }



    protected function createComponentDataGrid()
    {
        $grid = new DataGrid();
        $grid->setItemsPerPageList([50, 100, 500]);

        $qb = $this->entityManager->getRepository(Supplier::class)->createQueryBuilder('s')
            ->orderBy('s.id');

        $grid->setDataSource($qb);
        $grid->addColumnLink('id', 'ID', 'detail')
            ->setFilterText('id')
            ->setExactSearch(TRUE);

        $grid->addColumnText('companyRegistrationNumber', 'IČO')
            ->setFilterText('name');

        $grid->addColumnText('name', 'Název')
            ->setFilterText('name');

        $grid->addColumnText('taxId', 'DIČ')
            ->setFilterText('taxId');

        $grid->addColumnText('taxPayer', 'Plátce DPH', 'taxPayer')
            ->setRenderer(function (Supplier $item) {
                return Filters::boolYesNo($item->isTaxPayer());
            })
            ->setFilterSelect([0 => 'Vše', 1 => 'Ano', 2 => 'Ne'])
            ->setCondition(function (QueryBuilder $fluent, string $value) {
                if ($value === "0") {
                    return;
                }

                $taxPayer = $value === "1";
                $fluent->andWhere('s.taxPayer = :taxPayer')->setParameter('taxPayer', $taxPayer);
            });

        $grid->addAction('remove', 'Smazat', 'remove!', ['id' => 'id'])
            ->setTitle('Smazat')
            ->setClass('btn btn-xs btn-danger');

        return $grid;
    }



    protected function createComponentReplenishmentsDataGrid()
    {
        $grid = new DataGrid();
        $grid->setItemsPerPageList([50, 100, 500]);

        $qb = $this->entityManager->getRepository(Replenishment::class)->createQueryBuilder('r')
            ->addSelect('ri')
            ->innerJoin('r.supplier', 's')
            ->leftJoin('r.items', 'ri')
            ->leftJoin('r.warehouse', 'w')
            ->where('s.id = :supplierId')->setParameter('supplierId', $this->getParameter('id'))
            ->orderBy('r.date', 'DESC');

        $grid->setDataSource($qb);

        if (!$this->getEmployee()->isWarehouseman() && !$this->getEmployee()->isShopManager()) {
            $qb->andWhere('r.createdBy = :employee')->setParameter('employee', $this->getEmployee());
        }

        if ($this->getEmployee()->isShopManager()) {
            $qb->andWhere('r.warehouse IN (:warehouses) OR r.createdBy = :employee')
                ->setParameter('warehouses', $this->warehouseService->getMySubordinateWarehouses())
                ->setParameter('employee', $this->getEmployee());
        }

        if (!$this->getEmployee()->isWarehouseman()) {
            $qb->andWhere('w.shop IS NOT NULL');
        }

        $grid->addColumnLink('id', 'ID', ':Warehouse:Replenishment:detail')
            ->setFilterText('id')
            ->setExactSearch(TRUE);

        $grid->addColumnText('invoice', 'Invoice')
            ->setFilterText('invoice');

        $grid->addColumnText('warehouse', 'Sklad')
            ->setRenderer(function (Replenishment $item) {
                return $item->getWarehouse()->getName();
            })
            ->setFilterSelect($this->getWarehousesWithPrompt(), 'warehouse')
            ->setCondition(function (QueryBuilder $fluent, int $value) {
                if ($value <= 0) {
                    return;
                }

                $fluent->andWhere('r.warehouse = :warehouse')->setParameter('warehouse', $value);
            });

        $grid->addColumnText('totalPrice', 'Cena')
            ->setRenderer(function (Replenishment $item) {
                return Filters::money($item->getTotalPrice());
            })
            ->setFilterRange('r.totalPrice')
            ->setCondition(function (QueryBuilder $fluent, $value) {
                if (isset($value['from']) && !empty($value['from'])) {
                    $fluent->andWhere('r.totalPrice >= :minPrice')->setParameter('minPrice', $value['from'] * 100);
                }

                if (isset($value['to']) && !empty($value['to'])) {
                    $fluent->andWhere('r.totalPrice <= :maxPrice')->setParameter('maxPrice', $value['to'] * 100);
                }
            });

        $grid->addColumnText('createdBy', 'Vytvořil(a)')
            ->setRenderer(function (Replenishment $item) {
                return $item->getCreatedBy()->__toString();
            })
            ->setFilterSelect($this->getEmployeesWithPrompt())
            ->setCondition(function (QueryBuilder $fluent, int $value) {
                if ($value <= 0) {
                    return;
                }

                $fluent->andWhere('r.createdBy = :createdBy')->setParameter('createdBy', $value);
            });

        $grid->addColumnDateTime('date', 'Datum', 'date')
            ->setRenderer(function (Replenishment $item) {
                return $item->getDate()->format('Y-m-d');
            })
            ->setFilterDateRange('r.date');

        $grid->addColumnText('checkedByFinances', 'Kontrola financemi', 'checkedByFinances')
            ->setRenderer(function (Replenishment $item) {
                return Filters::boolYesNo($item->isCheckedByFinances());
            })
            ->setFilterSelect([0 => 'Vše', 1 => 'Ano', 2 => 'Ne'])
            ->setCondition(function (QueryBuilder $fluent, string $value) {
                if ($value === "0") {
                    return;
                }

                $checkedByFinances = $value === "1";
                $fluent->andWhere('r.checkedByFinances = :checkedByFinances')->setParameter('checkedByFinances', $checkedByFinances);
            });

        $grid->addColumnText('checkedByShopManager', 'Kontrola MB', 'checkedByShopManager')
            ->setRenderer(function (Replenishment $item) {
                return Filters::boolYesNo($item->isCheckedByShopManager());
            })
            ->setFilterSelect([0 => 'Vše', 1 => 'Ano', 2 => 'Ne'])
            ->setCondition(function (QueryBuilder $fluent, string $value) {
                if ($value === "0") {
                    return;
                }

                $checkedByShopManager = $value === "1";
                $fluent->andWhere('r.checkedByShopManager = :checkedByShopManager')->setParameter('checkedByShopManager', $checkedByShopManager);
            });

        return $grid;
    }



    private function getSupplier(): Supplier
    {
        /** @var Supplier|NULL $supplier */
        $supplier = $this->entityManager->find(Supplier::class, $this->getParameter('id'));

        if ($supplier === NULL) {
            $this->dangerFlashMessage('Tento dodavatel neexistuje');
            $this->redirect('default');
        }

        return $supplier;
    }



    /**
     * @return array(id: int => name: string)
     */
    private function getWarehousesWithPrompt(): array
    {
        $warehousesPairs = [0 => 'Vše'];
        $warehousesPairs += $this->warehouseService->getAvailableWarehouses();

        return $warehousesPairs;
    }



    /**
     * @return array(id: int => name: string)
     */
    private function getEmployeesWithPrompt(): array
    {
        $employeesPairs = [0 => 'Všichni'];

        /** @var Employee[] $employees */
        $employees = $this->entityManager->getRepository(Employee::class)->findAll();
        foreach ($employees as $employee) {
            $employeesPairs[$employee->getId()] = $employee->__toString();
        }

        ksort($employeesPairs);

        return $employeesPairs;
    }

}
