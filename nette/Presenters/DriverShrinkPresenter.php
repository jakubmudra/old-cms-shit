<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Shops\Services\ShopService;
use App\Entities\Shrinks\IngredientShrink;
use App\Entities\Warehouses\Closing;
use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\Services\ClosingService;
use App\Entities\Warehouses\Services\TransfersService;
use App\Entities\Warehouses\Transfer;
use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use App\Services\DateService;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Ramsey\Uuid\Uuid;



class DriverShrinkPresenter extends BasePresenter
{

    /**
     * @inject
     * @var TransfersService
     */
    public $transfersService;

    /**
     * @inject
     * @var ShopService
     */
    public $shopService;

    /**
     * @inject
     * @var ClosingService
     */
    public $closingService;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * Only for caching
     * @var Closing|NULL
     */
    private $closing;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isDriver() && !$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->date = $this->date;
        $this->template->dayBefore = $this->date->modifyClone('-1 day');
        $this->template->dayAfter = $this->date->modifyClone('+1 day');
        $this->template->today = (new DateTime())->setTime(0, 0, 0);
    }



    public function actionDefault(string $date = NULL)
    {
        $this->date = DateService::parseDateOrToday($date);
        $this->template->shops = $this->shopService->getAvailableShops();
    }



    public function actionDetail(string $id, string $date = NULL)
    {
        $this->date = DateService::parseDateOrToday($date);
        $this->template->closing = $this->getClosing();
    }



    protected function createComponentForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addText('pastryDeparted', 'Těsto')
            ->setRequired('Vyplňte prosím odvezené těsto')
            ->setOption('unit', 'kg')
            ->setDisabled($this->getClosing()->isShippingDone())
            ->setValue($this->getClosing()->getPastryDeparted())
            ->setType('number')
            ->setAttribute('step', 'any')
            ->addRule($form::FLOAT)
            ->addRule($form::RANGE, 'Těsta může být odvezeno od %d do %d kg', [0, 30]);

        $form->addText('trdelnikDeparted', 'Trdelníky')
            ->setRequired('Vyplňte prosím odvezené trdelníky')
            ->setType('number')
            ->setAttribute('step', 'any')
            ->setOption('unit', 'ks')
            ->setDisabled($this->getClosing()->isShippingDone())
            ->setValue($this->getClosing()->getTrdelnikDeparted())
            ->addRule($form::FLOAT, 'Trdelníků musí být číslo')
            ->addRule($form::RANGE, 'Trdelníků může být odvezeno od %d do %d ks', [0, 400]);

        $form->addInteger('coneDeparted', 'Kornoutky')
            ->setRequired('Vyplňte prosím odvezené trdelníky')
            ->setOption('unit', 'ks')
            ->setDisabled($this->getClosing()->isShippingDone())
            ->setValue($this->getClosing()->getConeDeparted())
            ->addRule($form::RANGE, 'Trdelníků může být odvezeno od %d do %d ks', [0, 400]);

        /** @var Ingredient $coffeeGrounded */
        $coffeeGrounded = $this->entityManager->find(Ingredient::class, Ingredient::COFFEE_GROUNDED_ID);
        $form->addText('coffeeGroundedDeparted', 'Mletá káva')
            ->setRequired('Vyplňte prosím odvezenou mletou kávu')
            ->setOption('unit', 'g')
            ->setDisabled($this->getClosing()->isShippingDone())
            ->setValue($this->getClosing()->getCoffeeGroundedDeparted() !== NULL ? $this->getClosing()->getCoffeeGroundedDeparted() * $coffeeGrounded->getUnitsPerPieceNetto() : NULL)
            ->setType('number')
            ->setAttribute('step', 'any')
            ->addRule($form::FLOAT)
            ->addRule($form::RANGE, 'Namleté kávy může být odvezeno od %d do %d g', [0, 100e3]);

        if (!$this->getClosing()->isShippingDone()) {
            $form->addSubmit('send', 'Uložit');
        }

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            if ($this->getClosing()->isShippingDone()) {
                return;
            }

            $this->createShrinkTransfers($values->pastryDeparted, $values->coffeeGroundedDeparted);
            $this->getClosing()->setTrdelnikDeparted($values->trdelnikDeparted);
            $this->getClosing()->setConeDeparted($values->coneDeparted);
            $this->getClosing()->recountUsed();

            $this->entityManager->flush();
            $this->successFlashMessage('Suroviny označeny jako odvezené');
            $this->redirect('this');
        };

        return $form;
    }



    private function createShrinkTransfers(float $pastryDepartedInKilograms, float $coffeeGroundedDepartedInGrams)
    {
        /** @var Ingredient $mixture */
        $mixture = $this->entityManager->find(Ingredient::class, Ingredient::PASTRY_ID);

        $shrink = new IngredientShrink(Uuid::uuid4(), $this->getWarehouse(), $mixture);
        $shrink->setCreatedAtCashDesk($this->date);
        $shrink->setAmount($pastryDepartedInKilograms);
        $shrink->setReason("Odpis řidič");
        $shrink->markChecked($this->getEmployee());

        $this->entityManager->persist($shrink);
        $this->entityManager->persist($this->transfersService->createTransfer($shrink, $this->getEmployee()));

        /** @var Ingredient $coffeeGrounded */
        $coffeeGrounded = $this->entityManager->find(Ingredient::class, Ingredient::COFFEE_GROUNDED_ID);

        $shrink = new IngredientShrink(Uuid::uuid4(), $this->getWarehouse(), $coffeeGrounded);
        $shrink->setCreatedAtCashDesk($this->date);
        $shrink->setAmount($coffeeGroundedDepartedInGrams / $coffeeGrounded->getUnitsPerPieceNetto());
        $shrink->setReason("Odpis řidič");
        $shrink->markChecked($this->getEmployee());

        $this->entityManager->persist($shrink);
        $this->entityManager->persist($this->transfersService->createTransfer($shrink, $this->getEmployee()));
    }



    private function getClosing() : Closing
    {
        if ($this->closing === null) {
            $today = $this->date;
            $this->closing = $this->closingService->findOrCreateClosing($this->getWarehouse(), $today);
        }

        return $this->closing;
    }



    private function getWarehouse() : Warehouse
    {
        /** @var Warehouse|null $warehouse */
        $warehouse = $this->entityManager->getRepository(Warehouse::class)->findOneBy(['code' => $this->getParameter('id')]);
        if ($warehouse === null) {
            $this->dangerFlashMessage('Tento sklad/obchod neexistuje');
            $this->redirect('default');
        }

        return $warehouse;
    }

}
