<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Employees\Employee;
use App\Entities\Warehouses\Replenishment;
use App\Entities\Warehouses\Services\ClosingService;
use App\Entities\Warehouses\Services\SafeBagService;
use App\Entities\Warehouses\Services\WarehouseService;
use App\Entities\Warehouses\Supplier;
use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use App\Helpers\Filters;
use App\WarehouseModule\Controls\ReplenishmentItems\ReplenishmentItemsControl;
use App\WarehouseModule\Controls\ReplenishmentItems\ReplenishmentItemsControlFactory;
use App\WarehouseModule\Controls\ReplenishmentReceipts\ReplenishmentReceiptsControl;
use App\WarehouseModule\Controls\ReplenishmentReceipts\ReplenishmentReceiptsControlFactory;
use Doctrine\ORM\QueryBuilder;
use Nette\Utils\ArrayHash;
use Nextras\Forms\Controls\DatePicker;
use Ublaboo\DataGrid\DataGrid;



class ReplenishmentPresenter extends BasePresenter
{

    private const GRID_SESSION_SECTION_NAME = 'Warehouse:Replenishment:dataGrid';

    /**
     * @inject
     * @var WarehouseService
     */
    public $warehouseService;

    /**
     * @inject
     * @var ClosingService
     */
    public $closingService;

    /**
     * @inject
     * @var SafeBagService
     */
    public $safeBagService;

    /**
     * @inject
     * @var ReplenishmentItemsControlFactory
     */
    public $replenishmentControlFactory;

    /**
     * @inject
     * @var ReplenishmentReceiptsControlFactory
     */
    public $replenishmentReceiptsControlFactory;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isSeller() && !$this->getEmployee()->isDriver() && !$this->getEmployee()->isWarehouseman()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function actionDefault()
    {
        $this->template->suppliers = $this->getSuppliers();
    }



    public function actionDetail(int $id)
    {
        $this->template->replenishment = $this->getReplenishment();
        $this->template->suppliers = $this->getSuppliers();
    }



    protected function createComponentReplenishmentItems()
    {
        $control = $this->replenishmentControlFactory->create($this->getReplenishment());
        $control->onSuccess[] = function (ReplenishmentItemsControl $self, string $message) {
            $this->successFlashMessage($message);
            $this->redirect('this');
        };

        $control->onError[] = function (ReplenishmentItemsControl $self, string $message) {
            $this->dangerFlashMessage($message);
            $this->redirect('this');
        };

        return $control;
    }



    protected function createComponentReplenishmentReceipts()
    {
        $control = $this->replenishmentReceiptsControlFactory->create($this->getReplenishment());
        $control->onSuccess[] = function (ReplenishmentReceiptsControl $self, string $message) {
            $this->successFlashMessage($message);
            $this->redirect('this');
        };

        $control->onError[] = function (ReplenishmentReceiptsControl $self, string $message) {
            $this->dangerFlashMessage($message);
            $this->redirect('this');
        };

        return $control;
    }



    protected function createComponentDataGrid()
    {
        $grid = new DataGrid();
        $grid->setItemsPerPageList([20, 50, 100, 500]);

        $this->filterFiltersForUser();

        $qb = $this->entityManager->getRepository(Replenishment::class)->createQueryBuilder('r')
            ->addSelect('ri')
            ->leftJoin('r.items', 'ri')
            ->leftJoin('r.warehouse', 'w')
            ->orderBy('r.date', 'DESC')
            ->orderBy('r.id', 'DESC');

        if (!$this->getEmployee()->isWarehouseman() && !$this->getEmployee()->isShopManager()) {
            $qb->andWhere('r.createdBy = :employee')->setParameter('employee', $this->getEmployee());
        }

        if ($this->getEmployee()->isShopManager()) {
            $qb->andWhere('r.warehouse IN (:warehouses) OR r.createdBy = :employee')
                ->setParameter('warehouses', $this->warehouseService->getMySubordinateWarehouses())
                ->setParameter('employee', $this->getEmployee());
        }

        if (!$this->getEmployee()->isWarehouseman()) {
            $qb->andWhere('w.shop IS NOT NULL');
        }

        $grid->setDataSource($qb);
        $grid->addColumnLink('id', 'ID', 'detail')
            ->setFilterText('id')
            ->setExactSearch(TRUE);

        $grid->addColumnText('invoice', 'Invoice')
            ->setFilterText('invoice');

        $grid->addColumnText('warehouse', 'Sklad')
            ->setRenderer(function (Replenishment $item) {
                return $item->getWarehouse()->getName();
            })
            ->setFilterSelect($this->getWarehousesWithPrompt(), 'warehouse')
            ->setCondition(function (QueryBuilder $fluent, int $value) {
                if ($value <= 0) {
                    return;
                }

                $fluent->andWhere('r.warehouse = :warehouse')->setParameter('warehouse', $value);
            });

        $grid->addColumnText('totalPrice', 'Cena')
            ->setRenderer(function (Replenishment $item) {
                return Filters::money($item->getTotalPrice());
            })
            ->setFilterRange('r.totalPrice')
            ->setCondition(function (QueryBuilder $fluent, $value) {
                if (isset($value['from']) && !empty($value['from'])) {
                    $fluent->andWhere('r.totalPrice >= :minPrice')->setParameter('minPrice', $value['from'] * 100);
                }

                if (isset($value['to']) && !empty($value['to'])) {
                    $fluent->andWhere('r.totalPrice <= :maxPrice')->setParameter('maxPrice', $value['to'] * 100);
                }
            });

        $grid->addColumnText('createdBy', 'Vytvořil(a)')
            ->setRenderer(function (Replenishment $item) {
                return $item->getCreatedBy()->__toString();
            })
            ->setFilterSelect($this->getEmployeesWithPrompt())
            ->setCondition(function (QueryBuilder $fluent, int $value) {
                if ($value <= 0) {
                    return;
                }

                $fluent->andWhere('r.createdBy = :createdBy')->setParameter('createdBy', $value);
            });

        $grid->addColumnDateTime('date', 'Datum', 'date')
            ->setRenderer(function (Replenishment $item) {
                return $item->getDate()->format('Y-m-d');
            })
            ->setFilterDateRange('r.date');

        $grid->addColumnText('checkedByFinances', 'Kontrola financemi', 'checkedByFinances')
            ->setRenderer(function (Replenishment $item) {
                return Filters::boolYesNo($item->isCheckedByFinances());
            })
            ->setFilterSelect([0 => 'Vše', 1 => 'Ano', 2 => 'Ne'])
            ->setCondition(function (QueryBuilder $fluent, string $value) {
                if ($value === "0") {
                    return;
                }

                $checkedByFinances = $value === "1";
                $fluent->andWhere('r.checkedByFinances = :checkedByFinances')->setParameter('checkedByFinances', $checkedByFinances);
            });
        $grid->addColumnText('checkedByShopManager', 'Kontrola MB', 'checkedByShopManager')
            ->setRenderer(function (Replenishment $item) {
                return Filters::boolYesNo($item->isCheckedByShopManager());
            })
            ->setFilterSelect([0 => 'Vše', 1 => 'Ano', 2 => 'Ne'])
            ->setCondition(function (QueryBuilder $fluent, string $value) {
                if ($value === "0") {
                    return;
                }

                $checkedByShopManager = $value === "1";
                $fluent->andWhere('r.checkedByShopManager = :checkedByShopManager')->setParameter('checkedByShopManager', $checkedByShopManager);
            });

        if ($this->getEmployee()->isShopManager()) {
            $grid->addAction('remove', 'Smazat', 'removeReplenishment!', ['replenishmentId' => 'id'])
                ->setTitle('Smazat')
                ->setClass('btn btn-xs btn-danger');
        }

        return $grid;
    }



    public function handleRemoveReplenishment(int $replenishmentId)
    {
        if (!$this->getEmployee()->isShopManager()) {
            $this->dangerFlashMessage('Na mazání naskladnění nemáte dostatečná práva. Napište prosím vedení');
            $this->redirect('this');
        }

        /** @var Replenishment|null $replenishment */
        $replenishment = $this->entityManager->find(Replenishment::class, $replenishmentId);
        if ($replenishment === NULL) {
            $this->dangerFlashMessage('Tato položka neexistuje');
            $this->redirect('this');
        }

        if ($replenishment->getItems()->count() > 0) {
            $this->dangerFlashMessage('Toto naskladnění není prázdné. Smažte prvně všechny položky a poté můžete snazat i naskladnění.');
            $this->redirect('this');
        }

        if ($replenishment->isCentralWarehouse() && !$this->getEmployee()->isManagement()) {
            $this->dangerFlashMessage('Toto naskladnění nemáte právo editovat');
            $this->redirect('this');
        }

        $this->entityManager->remove($replenishment);
        $this->entityManager->flush();

        if ($replenishment->getWarehouse()->isShop()) {
            $this->safeBagService->findOrCreateSafeBag($replenishment->getWarehouse()->getShop(), $replenishment->getDate()); // update price of replenishments
        }
        $this->entityManager->flush();

        $this->successFlashMessage('Naskladnění smazáno');
        $this->redirect('this');
    }



    protected function createComponentReplenishmentForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addSelect('warehouseId', 'Sklad', $this->getAvailableWarehouses())
            ->setPrompt('Vyberte')
            ->setRequired('Prosím vyberte sklad');

        $form->addText('supplierCompanyRegistrationNumber', 'Dodavatel IČO');

        $form->addText('invoice', 'Název faktury')
            ->setAttribute('placeholder', 'Zvolte vhodný název - např. "starobrněnská-okay-jahody-2017-07-17"')
            ->setRequired();

        $date = (new DatePicker())
            ->setValue(new \DateTime());
        $form->addComponent($date, 'date');

        $form->addCheckbox('paidOutsideCashDesk', 'Neplaceno z kasy (= platila kancelář / sklad / franšízant...)');

        if ($this->getEmployee()->isFinances()) {
            $form->addCheckbox('checkedByFinances', 'Zkontrolovaná financemi');
        }
        if ($this->getEmployee()->isShopManager()) {
            $form->addCheckbox('checkedByShopManager', 'Zkontrolováno MB');
        }

        if ($this->getParameter('id')) {
            $replenishment = $this->getReplenishment();
            $form['warehouseId']->setDisabled(TRUE);
            $form['warehouseId']->setDefaultValue($replenishment->getWarehouse()->getId());
            $form['invoice']->setDefaultValue($replenishment->getInvoice());
            $form['date']->setDefaultValue($replenishment->getDate());
            $form['paidOutsideCashDesk']->setDefaultValue($replenishment->isPaidOutsideCashDesk());

            if ($replenishment->getSupplier() !== NULL) {
                $form['supplierCompanyRegistrationNumber']->setDefaultValue($replenishment->getSupplier()->getCompanyRegistrationNumber());
            }

            if ($this->getEmployee()->isFinances()) {
                $form['checkedByFinances']->setDefaultValue($replenishment->isCheckedByFinances());
            }
            if ($this->getEmployee()->isShopManager()) {
                $form['checkedByShopManager']->setDefaultValue($replenishment->isCheckedByShopManager());
            }
        }

        $form->addSubmit('send', 'Uložit');

        $form->onValidate[] = function (BaseForm $form, ArrayHash $values) {
            $date = $values->date;
            if ($date === NULL) {
                $form->addError('Prosím vyplňte datum');
                return;
            }

            // @TEMPORARY
            if ($date < (new \DateTime())->modify('-3 month')) {
                $form->addError('Nelze naskladňovat tři měsíce staré faktury');
                return;
            }

            if ($date > (new \DateTime())->modify('+ 7 days')) {
                $form->addError('Nelze naskladňovat faktury týden napřed');
                return;
            }

            $replenishment = NULL;
            if ($this->getParameter('id')) {
                $replenishment = $this->getReplenishment();
            }

            if ($replenishment !== NULL) {
                if ($replenishment->isCheckedByFinances() && !$this->getEmployee()->isFinances()) {
                    $this->dangerFlashMessage('Tato faktura už byla potvrzená financemi. Manipulovat s ní už můžou opět pouze ty');
                    $this->redirect('this');
                } else if ($replenishment->isCheckedByShopManager() && !$this->getEmployee()->isShopManager()) {
                    $this->dangerFlashMessage('Tato faktura už byla potvrzená vedoucím. Manipulovat s ní už můžou opět pouze MB nebo management');
                    $this->redirect('this');
                }
            }

            $criteria = ['invoice' => $values->invoice];
            if ($replenishment !== NULL) {
                $criteria['id !='] = $replenishment->getId();
            }

            $replenishmentWithSameInvoiceName = $this->entityManager->getRepository(Replenishment::class)->findOneBy($criteria);
            if ($replenishmentWithSameInvoiceName !== NULL) {
                $form->addError(sprintf('Tato faktura byla již zařazena do systému (Id #%d). Vymyslete jiný název faktury.', $replenishmentWithSameInvoiceName->getId()));
                return;
            }
        };

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $date = $values->date;

            if ($this->getParameter('id')) {
                $replenishment = $this->getReplenishment();
                $replenishment->setInvoice($values->invoice);
                $replenishment->setDate($values->date);
                $message = 'Naskladnění upraveno';

            } else {
                /** @var Warehouse|null $warehouse */
                $warehouse = $this->entityManager->find(Warehouse::class, $values->warehouseId);
                if ($warehouse === NULL) {
                    $form->addError('Tento sklad neexistuje');
                    return;
                }

                $closing = $this->closingService->findOrCreateClosing($warehouse, $date);
                $replenishment = new Replenishment($closing, $values->invoice, $this->getEmployee());
                $this->entityManager->persist($replenishment);
                $message = 'Naskladnění vytvořeno. Nyní prosím vyberte položky z faktury.';
            }

            $replenishment->setPaidOutsideCashDesk($values->paidOutsideCashDesk);

            if ($this->getEmployee()->isFinances()) {
                $replenishment->setCheckedByFinances($values->checkedByFinances);
            }
            if ($this->getEmployee()->isShopManager()) {
                $replenishment->setCheckedByShopManager($values->checkedByShopManager);
            }

            $supplier = NULL;
            $companyRegistrationNumber = $values->supplierCompanyRegistrationNumber;

            if ($companyRegistrationNumber !== NULL && !empty($companyRegistrationNumber)) {
                /** @var Supplier|NULL $supplier */
                $supplier = $this->entityManager->getRepository(Supplier::class)->findOneBy(['companyRegistrationNumber' => $companyRegistrationNumber]);

                if ($supplier === NULL) {
                    $supplier = new Supplier($companyRegistrationNumber);
                    $this->entityManager->persist($supplier);
                }
            }

            $replenishment->setSupplier($supplier);

            if ($replenishment->getWarehouse()->isShop()) {
                $this->safeBagService->findOrCreateSafeBag($replenishment->getWarehouse()->getShop(), $replenishment->getDate()); // update price of replenishments
            }

            $this->entityManager->flush();
            $this->successFlashMessage($message);
            $this->redirect('detail', ['id' => $replenishment->getId()]);
        };

        return $form;
    }



    private function getReplenishment(): Replenishment
    {
        /** @var Replenishment|null $replenishment */
        $replenishment = $this->entityManager->find(Replenishment::class, $this->getParameter('id'));
        if ($replenishment === NULL) {
            $this->dangerFlashMessage('Toto naskladnění neexistuje');
            $this->redirect('default');
        }

        if (!$this->getEmployee()->isShopManager() && !$this->getEmployee()->isWarehouseman() && $replenishment->getCreatedBy() !== $this->getEmployee()) {
            $this->dangerFlashMessage('Nejste MB ani skladník a toto naskladnění jste nevytvářel. Nemůžete s ním manipulovat.');
            $this->redirect('default');
        }

        if (!in_array($replenishment->getWarehouse(), $this->warehouseService->getMySubordinateWarehouses(), TRUE) && $replenishment->getCreatedBy() !== $this->getEmployee()) {
            $this->dangerFlashMessage('Nemáte přístup na tento sklad.');
            $this->redirect('default');
        }

        if (!$this->getEmployee()->isWarehouseman() && !$replenishment->getWarehouse()->isShop()) {
            $this->dangerFlashMessage('Toto naskladnění bylo pro centrální sklad, nemůžete s ním manipulovat');
            $this->redirect('default');
        }

        return $replenishment;
    }



    /**
     * @return array(id: int => name: string)
     */
    private function getEmployeesWithPrompt(): array
    {
        $employeesPairs = [0 => 'Všichni'];

        /** @var Employee[] $employees */
        $employees = $this->entityManager->getRepository(Employee::class)->findAll();
        foreach ($employees as $employee) {
            $employeesPairs[$employee->getId()] = $employee->__toString();
        }

        ksort($employeesPairs);

        return $employeesPairs;
    }



    /**
     * @return array(id: int => name: string)
     */
    private function getWarehousesWithPrompt(): array
    {
        $warehousesPairs = [0 => 'Vše'];
        $warehousesPairs += $this->warehouseService->getAvailableWarehouses();

        return $warehousesPairs;
    }



    /**
     * @return Supplier[]
     */
    private function getSuppliers(): array
    {
        return $this->entityManager->getRepository(Supplier::class)->findAll();
    }



    /**
     * @return Warehouse[]
     */
    private function getAvailableWarehouses(): array
    {
        $warehouses = $this->warehouseService->getAvailableWarehouses();

        if ($this->getParameter('id')) {
            $currentWarehouse = $this->getReplenishment()->getWarehouse();
            $warehouses[$currentWarehouse->getId()] = $currentWarehouse;
        }

        return $warehouses;
    }



    /**
     * RM can't see other warehouses => they can't have it filtered
     */
    private function filterFiltersForUser(): void
    {
        $gridSessionSection = $this->getSession(self::GRID_SESSION_SECTION_NAME);

        if (!in_array($gridSessionSection->warehouse, array_keys($this->getWarehousesWithPrompt()))) {
            $gridSessionSection->warehouse = NULL;
        }
    }

}
