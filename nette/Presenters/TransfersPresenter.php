<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\IngredientRepository;
use App\Entities\Warehouses\Services\ClosingService;
use App\Entities\Warehouses\Services\WarehouseService;
use App\Entities\Warehouses\Transfer;
use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Nextras\Forms\Controls\DatePicker;



class TransfersPresenter extends BasePresenter
{

    /**
     * @inject
     * @var ClosingService
     */
    public $closingService;

    /**
     * @inject
     * @var WarehouseService
     */
    public $warehouseService;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function renderDefault()
    {
        $this->template->transfers = $this->fetchTransfers();
    }



    public function handleRemoveTransfer(int $transferId)
    {
        /** @var Transfer|null $transfer */
        $transfer = $this->entityManager->find(Transfer::class, $transferId);
        if ($transfer === null) {
            $this->dangerFlashMessage('Tento transfer neexistuje');
            $this->redirect('this');
        }

        if (!$this->getEmployee()->isManagement() && $transfer->getIngredient()->isOnlyInCentralWarehouse()) {
            $this->dangerFlashMessage('Tento transfer nemáte právo mazat');
            $this->redirect('this');
        }

        $this->entityManager->remove($transfer);
        $this->entityManager->flush();

        $this->successFlashMessage('Transfer smazán');
        $this->redirect('this');
    }



    protected function createComponentTransferForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $warehouses = $this->warehouseService->getAllWarehouses();

        $form->addSelect('oldWarehouseId', 'Původní sklad', $warehouses)
            ->setPrompt('Vyberte položku')
            ->setDefaultValue(Warehouse::CENTRAL_WAREHOUSE_ID)
            ->setRequired();

        $form->addSelect('newWarehouseId', 'Nový sklad', $warehouses)
            ->setPrompt('Vyberte položku')
            ->setRequired();

        $form->addSelect('ingredientId', 'Surovina', $this->fetchIngredients())
            ->setPrompt('Vyberte')
            ->setRequired();

        $form->addText('pieces', 'Množství')
            ->setRequired()
            ->setType('number')
            ->setAttribute('step', 'any')
            ->addRule($form::FLOAT)
            ->addRule($form::RANGE, 'Množství musí být v rozsahu %d a %d', [0, 1e6]);

        $date = (new DatePicker())
            ->setValue(new \DateTime());
        $form->addComponent($date, 'date');

        $form->addSubmit('send', 'Přidat');

        $form->onValidate[] = function (BaseForm $form, ArrayHash $values) {
            if ($values->oldWarehouseId === $values->newWarehouseId) {
                $form->addError('Sklady musí být různé');
                return;
            }

            $date = $values->date;
            if ($date === null) {
                $form->addError('Prosím vyplňte datum');
                return;
            }

            if ($date < (new \DateTime())->modify('-1 month')) {
                $form->addError('Nelze dělat transfery měsíc dozadu');
                return;
            }

            if ($date > (new \DateTime())->modify('+ 7 days')) {
                $form->addError('Nelze dělat transfery týden napřed');
                return;
            }

            /** @var Warehouse|null $oldWarehouse */
            $oldWarehouse = $this->entityManager->find(Warehouse::class, $values->oldWarehouseId);
            if ($oldWarehouse === null) {
                $form->addError('Sklad, ze kterého se snažíte převézt suroviny, neexistuje');
                return;
            }

            /** @var Warehouse|null $newWarehouse */
            $newWarehouse = $this->entityManager->find(Warehouse::class, $values->newWarehouseId);
            if ($newWarehouse === null) {
                $form->addError('Sklad, ze kterého se snažíte převézt suroviny, neexistuje');
                return;
            }

            /** @var Ingredient|null $ingredient */
            $ingredient = $this->entityManager->find(Ingredient::class, $values->ingredientId);
            if ($ingredient === null) {
                $form->addError('Surovina neexistuje');
                return;
            }
        };

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            /** @var Warehouse $oldWarehouse */
            $oldWarehouse = $this->entityManager->find(Warehouse::class, $values->oldWarehouseId);
            $closingFrom = $this->closingService->findOrCreateClosing($oldWarehouse, $values->date);

            /** @var Warehouse $newWarehouse */
            $newWarehouse = $this->entityManager->find(Warehouse::class, $values->newWarehouseId);
            $closingTo = $this->closingService->findOrCreateClosing($newWarehouse, $values->date);

            /** @var Ingredient $ingredient */
            $ingredient = $this->entityManager->find(Ingredient::class, $values->ingredientId);

            $transfer = new Transfer($closingFrom, $closingTo, $ingredient, $values->pieces, $this->getEmployee());
            $this->entityManager->persist($transfer);
            $this->entityManager->flush();

            $this->successFlashMessage('Transfer vytvořen');
            $this->redirect('this');
        };

        return $form;
    }



    /**
     * @return Transfer[]
     */
    private function fetchTransfers() : array
    {
        $qb = $this->entityManager->getRepository(Transfer::class)->createQueryBuilder('t')
            ->innerJoin('t.ingredient', 'i')
            ->andWhere('t.supplyPackageItem IS NULL')
            ->andWhere('t.date >= :start')->setParameter('start', (new \DateTime())->modify('-1 month'))
            ->andWhere('t.newWarehouse != :shrinkWarehouse')->setParameter('shrinkWarehouse', Warehouse::SHRINK_WAREHOUSE_ID)
            ->addOrderBy('t.date', 'DESC')
            ->addOrderBy('t.id', 'DESC');

        if (!$this->getEmployee()->isManagement()) {
            $qb->andWhere('i.onlyInCentralWarehouse = false');
        }

        return $qb->getQuery()->getResult();
    }


    /**
     * @return [id:int => name:string]
     */
    private function fetchIngredients(): array
    {
        /** @var IngredientRepository $ingredientsRepository */
        $ingredientsRepository = $this->entityManager->getRepository(Ingredient::class);
        return $ingredientsRepository->fetchIngredientsForSelect($this->getEmployee()->isManagement(), false, true);
    }

}
