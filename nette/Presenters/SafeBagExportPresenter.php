<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Shops\Shop;
use App\Entities\Warehouses\SafeBag;
use App\Forms\BaseForm;
use Nette\Utils\ArrayHash;



// @FIXME: @safebag should have Shop association, not Warehouse !
class SafeBagExportPresenter extends BasePresenter
{

    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isFinances()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    protected function createComponentForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $date = new \DateTime();
        $safeBags = $this->fetchSafeBags();
        foreach ($safeBags as $safeBag) {
            if ($date->format('Y-m-d') !== $safeBag->getDate()->format('Y-m-d')) {
                $date = $safeBag->getDate();
                $form->addGroup($date->format('d.m.Y'));
            }

            $shop = $safeBag->getShop();
            $name = sprintf("%s %s - #%s",
                $shop->getCode(),
                $shop->getName(),
                $safeBag->getNumber()
            );
            $form->addCheckbox('safeBag' . $safeBag->getId(), $name);
        }

        $form->addGroup('');
        $form->addSubmit('send', 'Exportovat');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $output = '';

            foreach ($values as $key => $value) {
                if ($value === FALSE) {
                    continue;
                }

                $id = (int) substr($key, 7);

                /** @var SafeBag|NULL $safeBag */
                $safeBag = $this->entityManager->find(SafeBag::class, $id);
                if ($safeBag === NULL) {
                    $form->addError('Tento SafeBag neexistuje');
                    return;
                }

                $output .= $safeBag->getNumber() . ';';
                $output .= (new \DateTime())->format('d.m.Y') . ';';
                $output .= (new \DateTime())->format('H:i:s') . ';';
                $output .= $safeBag->getShop()->getCode() . ';';
                $output .= $safeBag->getDate()->format('d.m.Y') . ';';
                $output .= $safeBag->getFiftyCrown() . ';';
                $output .= $safeBag->getTwentyCrown() . ';';
                $output .= $safeBag->getTenCrown() . ';';
                $output .= $safeBag->getFiveCrown() . ';';
                $output .= $safeBag->getTwoCrown() . ';';
                $output .= $safeBag->getOneCrown() . ';';
                $output .= $safeBag->getFiveThousandCrown() . ';';
                $output .= $safeBag->getTwoThousandCrown() . ';';
                $output .= $safeBag->getOneThousandCrown() . ';';
                $output .= $safeBag->getFiveHundredCrown() . ';';
                $output .= $safeBag->getTwoHundredCrown() . ';';
                $output .= $safeBag->getOneHundredCrown() . ';';
                $output .= str_replace('.', ',',$safeBag->getEuros()->getAmount() / 100) . ';';
                $output .= str_replace('.', ',',$safeBag->getMealVouchers()->getAmount() / 100) . ';';
                $output .= str_replace('.', ',',$safeBag->getCards()->getAmount() / 100) . ';';
                $output .= str_replace('.', ',',$safeBag->getWithdrawals()->getAmount() / 100) . ';';
                $output .= str_replace('.', ',',$safeBag->getReplenishments()->getAmount() / 100) . ';';
                $output .= str_replace('.', ',',$safeBag->getPromoVouchers()->getAmount() / 100) . ';';
                $output .= "\n";
            }

            $filename = "safebagy-" . (new \DateTime())->format('Y-m-d') . ".csv";
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=" . $filename);
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $output;
            exit;
        };

        return $form;
    }



    /**
     * @return SafeBag[]
     */
    private function fetchSafeBags(): array
    {
        return $this->entityManager->getRepository(SafeBag::class)->createQueryBuilder('sb')
            ->innerJoin('sb.shop', 's')
            ->innerJoin('s.warehouse', 'w')
            ->andWhere('sb.date > :lastWeek')->setParameter('lastWeek', (new \DateTime())->setTime(0, 0, 0)->modify('-90 days'))
            ->andWhere('sb.moneyStatus = :status OR sb.papersStatus = :status')->setParameter('status', SafeBag::STATUS_COUNTED)
            ->orderBy('sb.date')
            ->addOrderBy('sb.shop')
            ->getQuery()->getResult();
    }

}
