<?php

namespace App\WarehouseModule\Controls\SafeBag;

use App\Entities\Employees\Employee;
use App\Entities\Orders\Order;
use App\Entities\Warehouses\SafeBag;
use App\Entities\Warehouses\SafeBagFile;
use App\Entities\Warehouses\SafeBagLog;
use App\Entities\Warehouses\Services\SafeBagService;
use App\Forms\BaseForm;
use App\Helpers\Filters;
use App\Security\User;
use App\Services\CurrencyRateService;
use Doctrine\ORM\EntityManager;
use Money\Currency;
use Money\Money;
use Nette\Application\UI\Control;
use Nette\Forms\IControl;
use Nette\Utils\ArrayHash;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;
use Tracy\ILogger;


/**
 * @method onSuccess(SafeBagControl $this)
 * @method onError(SafeBagControl $this, string $message)
 */
class SafeBagControl extends Control
{

    /**
     * @var array
     */
    public $onSuccess = [];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var SafeBagService
     */
    private $safeBagService;

    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var SafeBag
     */
    private $safeBag;

    /**
     * @var ILogger
     */
    private $logger;



    public function __construct(
        EntityManager $entityManager,
        SafeBagService $safeBagService,
        ILogger $logger,
        User $user,
        SafeBag $safeBag
    )
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->safeBagService = $safeBagService;
        $this->logger = $logger;
        $this->employee = $user->getEntity();
        $this->safeBag = $safeBag;
    }



    public function render()
    {
        $this->template->safeBag = $this->safeBag;
        $this->template->reports = $this->entityManager->getRepository(SafeBagFile::class)
            ->findBy(['safeBag' => $this->safeBag], ['uploadedAt' => 'DESC']);
        $this->template->employee = $this->employee;

        $file = $this->isCzk() ? 'czk.latte' : 'eur.latte';
        $this->template->setFile(__DIR__ . '/' . $file);
        $this->template->render();
    }



    protected function createComponentSafeBagForm()
    {
        $safeBag = $this->safeBag;

        $form = new BaseForm();
        $form->addProtection();

        $form->addCheckbox('inWrongBag', 'Chybí mi trezor (sáček)')
            ->addCondition($form::FILLED)
            ->toggle('inWrongBagDescription');

        $form->addText('number', 'Číslo trezoru (sáčku)')
            ->setAttribute('placeholder', 'Např.: CZ0100655309123456')
            ->setRequired('Prosím vyplňte číslo trezoru')
            ->addRule([$this, 'uniqueSafeBagNumber'], 'SafeBag s takovým číslem už v systému existuje')
            ->addConditionOn($form['inWrongBag'], $form::BLANK)
            ->addRule($form::PATTERN, "Číslo sáčku musí být ve formátu \"CZ0100xxxxxxxxxxxx\". Např.: CZ0100655309123456", "(CZ|cz|Cz|cZ)0100[0-9]{12}");

        $form->addSelect('moneyStatus', 'Status peněz', [
            SafeBag::STATUS_IN_SHOP => 'Na prodejně',
            SafeBag::STATUS_TRAVELING => 'Na cestě',
            SafeBag::STATUS_IN_OFFICE => 'V kanceláři',
            SafeBag::STATUS_MISSING => 'Pohřešovaný',
            SafeBag::STATUS_STOLEN => 'Ukradený',
            SafeBag::STATUS_IN_BANK => 'V bance',
            SafeBag::STATUS_COUNTED => 'Přepočítáno v kanceláři',
        ])->setDefaultValue(SafeBag::STATUS_IN_SHOP);

        $form->addSelect('papersStatus', 'Status ostatního', [
            SafeBag::STATUS_IN_SHOP => 'Na prodejně',
            SafeBag::STATUS_TRAVELING => 'Na cestě',
            SafeBag::STATUS_IN_OFFICE => 'V kanceláři',
            SafeBag::STATUS_MISSING => 'Pohřešovaný',
            SafeBag::STATUS_STOLEN => 'Ukradený',
            SafeBag::STATUS_COUNTED => 'Přepočítáno v kanceláři',
        ])->setDefaultValue(SafeBag::STATUS_IN_SHOP);

        if ((!$this->employee->isDriver() && !$this->employee->isShopManager()) || !$this->safeBag->isFilled()) {
            $form['moneyStatus']->setDisabled();
            $form['papersStatus']->setDisabled();
        }

        if ($this->isCzk()) {
            $form->addInteger('oneCrown', '1 kč')
                ->setRequired('Prosím vyplňte počet korunových mincí')
                ->addRule($form::RANGE, 'Korunových mincí můžete mít od %d do %d', [0, 1000]);

            $form->addInteger('twoCrown', '2 kč')
                ->setRequired('Prosím vyplňte počet dvoukorun')
                ->addRule($form::RANGE, 'Dvoukoruno můžete mít od %d do %d', [0, 1000]);

            $form->addInteger('fiveCrown', '5 kč')
                ->setRequired('Prosím vyplňte počet pětikorun')
                ->addRule($form::RANGE, 'Pětikorun můžete mít od %d do %d', [0, 1000]);

            $form->addInteger('tenCrown', '10 kč')
                ->setRequired('Prosím vyplňte počet desetikorun')
                ->addRule($form::RANGE, 'Desetikorun můžete mít od %d do %d', [0, 1000]);

            $form->addInteger('twentyCrown', '20 kč')
                ->setRequired('Prosím vyplňte počet dvacetikorun')
                ->addRule($form::RANGE, 'Dvacetikorun můžete mít od %d do %d', [0, 1000]);

            $form->addInteger('fiftyCrown', '50 kč')
                ->setRequired('Prosím vyplňte počet padesátikorun')
                ->addRule($form::RANGE, 'Padesátikorun můžete mít od %d do %d', [0, 600]);

            $form->addInteger('oneHundredCrown', '100 kč')
                ->setRequired('Prosím vyplňte počet stokorun')
                ->addRule($form::RANGE, 'Stokorun můžete mít od %d do %d', [0, 300]);

            $form->addInteger('twoHundredCrown', '200 kč')
                ->setRequired('Prosím vyplňte počet dvousetkorun')
                ->addRule($form::RANGE, 'Dvousetkorun můžete mít od %d do %d', [0, 150]);

            $form->addInteger('fiveHundredCrown', '500 kč')
                ->setRequired('Prosím vyplňte počet pětisetkorun')
                ->addRule($form::RANGE, 'Pětisetkorun můžete mít od %d do %d', [0, 60]);

            $form->addInteger('oneThousandCrown', '1000 kč')
                ->setRequired('Prosím vyplňte počet tisícikorun')
                ->addRule($form::RANGE, 'Tisícikorun můžete mít od %d do %d', [0, 30]);

            $form->addInteger('twoThousandCrown', '2000 kč')
                ->setRequired('Prosím vyplňte počet dvoutisícikorun')
                ->addRule($form::RANGE, 'Dvoutisíckorun můžete mít od %d do %d', [0, 15]);

            $form->addInteger('fiveThousandCrown', '5000 kč')
                ->setRequired('Prosím vyplňte počet pětitisícikorun')
                ->addRule($form::RANGE, 'Pětitisícikorun můžete mít od %d do %d', [0, 6]);
        }

        $form->addInteger('fiveHundredEuro', '500 €')
            ->setDisabled();

        $form->addInteger('twoHundredEuro', '200 €')
            ->setDisabled();

        $form->addInteger('oneHundredEuro', '100 €')
            ->setDisabled();

        $form->addInteger('fiftyEuro', '50 €')
            ->setRequired('Prosím vyplňte počet padesátieurových bankovek')
            ->addRule($form::RANGE, 'Padesátieurových bankovek můžete mít od %d do %d', [0, 10]);

        $form->addInteger('twentyEuro', '20 €')
            ->setRequired('Prosím vyplňte počet dvacetieurových bankovek')
            ->addRule($form::RANGE, 'Dvacetieurových bankovek můžete mít od %d do %d', [0, 25]);

        $form->addInteger('tenEuro', '10 €')
            ->setRequired('Prosím vyplňte počet desetieurových bankovek')
            ->addRule($form::RANGE, 'Desetieurových bankovek můžete mít od %d do %d', [0, 50]);

        $form->addInteger('fiveEuro', '5 €')
            ->setRequired('Prosím vyplňte počet pětieurových bankovek')
            ->addRule($form::RANGE, 'pětieurových bankovek můžete mít od %d do %d', [0, 100]);

        $form->addInteger('twoEuro', '2 €')
            ->setRequired('Prosím vyplňte počet dvouerových mincí')
            ->addRule($form::RANGE, 'Dvouerových mincí můžete mít od %d do %d', [0, 250]);

        $form->addInteger('oneEuro', '1 €')
            ->setRequired('Prosím vyplňte počet jednoeurových mincí')
            ->addRule($form::RANGE, 'Jednoeurových mincí můžete mít od %d do %d', [0, 500]);

        $form->addInteger('fiftyEuroCent', '0.5 €')
            ->setRequired('Prosím vyplňte počet padesátieurocentových mincí')
            ->addRule($form::RANGE, 'Padesátieurocentových mincí můžete mít od %d do %d', [0, 500]);

        $form->addInteger('twentyEuroCent', '0.2 €')
            ->setRequired('Prosím vyplňte počet dvacetieurocentových mincí')
            ->addRule($form::RANGE, 'Dvacetieurocentových mincí můžete mít od %d do %d', [0, 500]);

        $form->addInteger('tenEuroCent', '0.1 €')
            ->setRequired('Prosím vyplňte počet desetieurocentových mincí')
            ->addRule($form::RANGE, 'Desetieurocentových mincí můžete mít od %d do %d', [0, 500]);

        $form->addInteger('fiveEuroCent', '0.05 €')
            ->setRequired('Prosím vyplňte počet pětieurocentových mincí')
            ->addRule($form::RANGE, 'Pětieurocentových mincí můžete mít od %d do %d', [0, 500]);

        $form->addInteger('twoEuroCent', '0.02 €')
            ->setRequired('Prosím vyplňte počet dvoueurocentových mincí')
            ->addRule($form::RANGE, 'Dvoueurocentových mincí můžete mít od %d do %d', [0, 500]);

        $form->addInteger('oneEuroCent', '0.01 €')
            ->setRequired('Prosím vyplňte počet jednoeurocentových mincí')
            ->addRule($form::RANGE, 'Jednoeurocentových mincí můžete mít od %d do %d', [0, 500]);

        $form->addText('cards', 'Uzávěrka z terminálu')
            ->addRule($form::FLOAT)
            ->setRequired('Prosím vyplňte peníze z terminálu')
            ->addRule($form::RANGE, 'Uzávěrka z terminálu může být od %d do %d', [0, 30000]);

        $form->addText('mealVouchers', 'Stravenky')
            ->addRule($form::FLOAT)
            ->setRequired('Prosím vyplňte celkovou hodnotu stravenek')
            ->addRule($form::RANGE, 'Stravenky mohou být od %d do %d', [0, 10000]);

        $form->addText('promoVouchers', 'Promo vouchery')
            ->addRule($form::FLOAT)
            ->setRequired('Prosím vyplňte vouchery')
            ->addRule($form::RANGE, 'Promo vouchery mohou být od %d do %d', [0, 10000]);

        $form->addText('withdrawals', 'Výběry')
            ->addRule($form::FLOAT)
            ->setRequired('Prosím vyplňte výběry')
            ->addRule($form::RANGE, 'Výběry mohou být od %d do %d', [0, 10000]);

        $form->addText('replenishments', 'Nákupy na prodejně')
            ->addRule($form::FLOAT)
            ->setDisabled()
            ->setRequired(false)
            ->setValue(round($safeBag->getReplenishments()->getAmount() / 100, 2));

        $form->addText('loyaltyCards', 'Věrnostních kartiček')
            ->setDisabled()
            ->setRequired(false)
            ->setValue($this->getLoyaltyCardsCount());

        $form->addInteger('cashdesk', 'Základ v pokladně')
            ->setDisabled(TRUE)
            ->setValue($this->getCashRegisterBaseAmount());

        $form->addSubmit('send', 'Uložit')
            ->setAttribute('id', 'safeBagForm-send');

        if ($safeBag->isFilled()) {
            $form['number']->setValue($safeBag->getNumber());
            $form['inWrongBag']->setValue($safeBag->isInWrongBag());

            if (!empty($safeBag->getMoneyStatus()) && $safeBag->getMoneyStatus() !== SafeBag::STATUS_UNKNOWN) {
                $form['moneyStatus']->setValue($safeBag->getMoneyStatus());
            }

            if (!empty($safeBag->getPapersStatus()) && $safeBag->getPapersStatus() !== SafeBag::STATUS_UNKNOWN) {
                $form['papersStatus']->setValue($safeBag->getPapersStatus());
            }

            if ($this->isCzk()) {
                $form['oneCrown']->setValue($safeBag->getOneCrown());
                $form['twoCrown']->setValue($safeBag->getTwoCrown());
                $form['fiveCrown']->setValue($safeBag->getFiveCrown());
                $form['tenCrown']->setValue($safeBag->getTenCrown());
                $form['twentyCrown']->setValue($safeBag->getTwentyCrown());
                $form['fiftyCrown']->setValue($safeBag->getFiftyCrown());
                $form['oneHundredCrown']->setValue($safeBag->getOneHundredCrown());
                $form['twoHundredCrown']->setValue($safeBag->getTwoHundredCrown());
                $form['fiveHundredCrown']->setValue($safeBag->getFiveHundredCrown());
                $form['oneThousandCrown']->setValue($safeBag->getOneThousandCrown());
                $form['twoThousandCrown']->setValue($safeBag->getTwoThousandCrown());
                $form['fiveThousandCrown']->setValue($safeBag->getFiveThousandCrown());
            }

            $form['oneEuroCent']->setValue($safeBag->getOneEuroCent());
            $form['twoEuroCent']->setValue($safeBag->getTwoEuroCent());
            $form['fiveEuroCent']->setValue($safeBag->getFiveEuroCent());
            $form['tenEuroCent']->setValue($safeBag->getTenEuroCent());
            $form['twentyEuroCent']->setValue($safeBag->getTwentyEuroCent());
            $form['fiftyEuroCent']->setValue($safeBag->getFiftyEuroCent());
            $form['oneEuro']->setValue($safeBag->getOneEuro());
            $form['twoEuro']->setValue($safeBag->getTwoEuro());
            $form['fiveEuro']->setValue($safeBag->getFiveEuro());
            $form['tenEuro']->setValue($safeBag->getTenEuro());
            $form['twentyEuro']->setValue($safeBag->getTwentyEuro());
            $form['fiftyEuro']->setValue($safeBag->getFiftyEuro());

            $form['cards']->setValue($safeBag->getCards()->getAmount() / 100);
            $form['mealVouchers']->setValue($safeBag->getMealVouchers()->getAmount() / 100);
            $form['promoVouchers']->setValue($safeBag->getPromoVouchers()->getAmount() / 100);
            $form['withdrawals']->setValue($safeBag->getWithdrawals()->getAmount() / 100);
        }

        $form->onSuccess[] = [$this, 'formSuccess'];

        return $form;
    }



    public function formSuccess(BaseForm $form, ArrayHash $values)
    {
        $safeBag = $this->safeBag;

        $canonized = $this->canonizeValues($values);

        if ($safeBag->isFilled()) {
            $this->logChanges($safeBag, $canonized);

            if ($this->employee->isDriverOrRM() && $safeBag->isFilled()) {
                $newMoneyStatus = $values->moneyStatus;
                $newPapersStatus = $values->papersStatus;

                try {
                    if ($safeBag->getMoneyStatus() !== $newMoneyStatus) {
                        $this->safeBagService->markMoneyStatus($safeBag, $newMoneyStatus);
                    }

                    if ($safeBag->getPapersStatus() !== $newPapersStatus) {
                        $this->safeBagService->markPapersStatus($safeBag, $newPapersStatus);
                    }
                } catch (\InvalidArgumentException $e) {
                    $form->addError($e->getMessage());
                    return;
                }
            }
        } else {
            $safeBagLog = new SafeBagLog($safeBag, 'Byl vyplněn safebag', $this->employee);
            $this->entityManager->persist($safeBagLog);

            $safeBag->setFilledBy($this->employee);
        }

        $safeBag->setNumber($canonized->number);
        $safeBag->setInWrongBag($canonized->inWrongBag);

        $safeBag->setOneCrown($canonized->oneCrown);
        $safeBag->setTwoCrown($canonized->twoCrown);
        $safeBag->setFiveCrown($canonized->fiveCrown);
        $safeBag->setTenCrown($canonized->tenCrown);
        $safeBag->setTwentyCrown($canonized->twentyCrown);
        $safeBag->setFiftyCrown($canonized->fiftyCrown);
        $safeBag->setOneHundredCrown($canonized->oneHundredCrown);
        $safeBag->setTwoHundredCrown($canonized->twoHundredCrown);
        $safeBag->setFiveHundredCrown($canonized->fiveHundredCrown);
        $safeBag->setOneThousandCrown($canonized->oneThousandCrown);
        $safeBag->setTwoThousandCrown($canonized->twoThousandCrown);
        $safeBag->setFiveThousandCrown($canonized->fiveThousandCrown);

        $safeBag->setOneEuroCent($canonized->oneEuroCent);
        $safeBag->setTwoEuroCent($canonized->twoEuroCent);
        $safeBag->setFiveEuroCent($canonized->fiveEuroCent);
        $safeBag->setTenEuroCent($canonized->tenEuroCent);
        $safeBag->setTwentyEuroCent($canonized->twentyEuroCent);
        $safeBag->setFiftyEuroCent($canonized->fiftyEuroCent);
        $safeBag->setOneEuro($canonized->oneEuro);
        $safeBag->setTwoEuro($canonized->twoEuro);
        $safeBag->setFiveEuro($canonized->fiveEuro);
        $safeBag->setTenEuro($canonized->tenEuro);
        $safeBag->setTwentyEuro($canonized->twentyEuro);
        $safeBag->setFiftyEuro($canonized->fiftyEuro);

        $safeBag->setCards($canonized->cards);
        $safeBag->setMealVouchers($canonized->mealVouchers);
        $safeBag->setPromoVouchers($canonized->promoVouchers);
        $safeBag->setWithdrawals($canonized->withdrawals);

        $this->entityManager->flush();
        $this->onSuccess($this);
    }


    public function uniqueSafeBagNumber(IControl $control)
    {
        $queryBuilder = $this->entityManager->getRepository(SafeBag::class)->createQueryBuilder('sb')
            ->andWhere('sb.number = :requestedNumber')->setParameter('requestedNumber', $control->getValue())
            ->setMaxResults(1);

        $safeBagId = $this->safeBag->getId();
        if ($safeBagId !== NULL) {
            $queryBuilder->andWhere('sb.id != :id')->setParameter('id', $safeBagId);
        }

        /** @var SafeBag|NULL $duplicate */
        $duplicate = $queryBuilder->getQuery()->getOneOrNullResult();
        return $duplicate === NULL;
    }



    private function canonizeValues(ArrayHash $values): ArrayHash
    {
        $canonized = $values;

        if (!$this->isCzk()) {
            $canonized['oneCrown'] = 0;
            $canonized['twoCrown'] = 0;
            $canonized['fiveCrown'] = 0;
            $canonized['tenCrown'] = 0;
            $canonized['twentyCrown'] = 0;
            $canonized['fiftyCrown'] = 0;
            $canonized['oneHundredCrown'] = 0;
            $canonized['twoHundredCrown'] = 0;
            $canonized['fiveHundredCrown'] = 0;
            $canonized['oneThousandCrown'] = 0;
            $canonized['twoThousandCrown'] = 0;
            $canonized['fiveThousandCrown'] = 0;
        }

        $canonized->cards = (int) round($values->cards * 100);
        $canonized->mealVouchers = (int) round($values->mealVouchers * 100);
        $canonized->promoVouchers = (int) round($values->promoVouchers * 100);
        $canonized->withdrawals = (int) round($values->withdrawals * 100);

        return $canonized;
    }



    private function logChanges(SafeBag $safeBag, ArrayHash $canonizedValues)
    {
        if ($safeBag->getNumber() !== $canonizedValues->number) {
            $log = sprintf('Číslo safebagu z %s na %s', $safeBag->getNumber(), $canonizedValues->number);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        if ($this->isCzk()) {
            if ($safeBag->getOneCrown() !== $canonizedValues->oneCrown) {
                $log = sprintf('Korunové mince z %d na %d', $safeBag->getOneCrown(), $canonizedValues->oneCrown);
                $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
                $this->entityManager->persist($safeBagLog);
            }

            if ($safeBag->getTwoCrown() !== $canonizedValues->twoCrown) {
                $log = sprintf('Dvoukorunové mince z %d na %d', $safeBag->getTwoCrown(), $canonizedValues->twoCrown);
                $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
                $this->entityManager->persist($safeBagLog);
            }

            if ($safeBag->getFiveCrown() !== $canonizedValues->fiveCrown) {
                $log = sprintf('Pětikorunové mince z %d na %d', $safeBag->getFiveCrown(), $canonizedValues->fiveCrown);
                $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
                $this->entityManager->persist($safeBagLog);
            }

            if ($safeBag->getTenCrown() !== $canonizedValues->tenCrown) {
                $log = sprintf('Desetikorunové mince z %d na %d', $safeBag->getTenCrown(), $canonizedValues->tenCrown);
                $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
                $this->entityManager->persist($safeBagLog);
            }

            if ($safeBag->getTwentyCrown() !== $canonizedValues->twentyCrown) {
                $log = sprintf('Dvacetikorunové mince z %d na %d', $safeBag->getTwentyCrown(), $canonizedValues->twentyCrown);
                $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
                $this->entityManager->persist($safeBagLog);
            }

            if ($safeBag->getFiftyCrown() !== $canonizedValues->fiftyCrown) {
                $log = sprintf('Padesátikorunové mince z %d na %d', $safeBag->getFiftyCrown(), $canonizedValues->fiftyCrown);
                $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
                $this->entityManager->persist($safeBagLog);
            }

            if ($safeBag->getOneHundredCrown() !== $canonizedValues->oneHundredCrown) {
                $log = sprintf('Stokoruny z %d na %d', $safeBag->getOneHundredCrown(), $canonizedValues->oneHundredCrown);
                $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
                $this->entityManager->persist($safeBagLog);
            }

            if ($safeBag->getTwoHundredCrown() !== $canonizedValues->twoHundredCrown) {
                $log = sprintf('Dvousetkoruny z %d na %d', $safeBag->getTwoHundredCrown(), $canonizedValues->twoHundredCrown);
                $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
                $this->entityManager->persist($safeBagLog);
            }

            if ($safeBag->getFiveHundredCrown() !== $canonizedValues->fiveHundredCrown) {
                $log = sprintf('Pětisetkoruny z %d na %d', $safeBag->getFiveHundredCrown(), $canonizedValues->fiveHundredCrown);
                $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
                $this->entityManager->persist($safeBagLog);
            }

            if ($safeBag->getOneThousandCrown() !== $canonizedValues->oneThousandCrown) {
                $log = sprintf('Tisícikoruny z %d na %d', $safeBag->getOneThousandCrown(), $canonizedValues->oneThousandCrown);
                $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
                $this->entityManager->persist($safeBagLog);
            }

            if ($safeBag->getTwoThousandCrown() !== $canonizedValues->twoThousandCrown) {
                $log = sprintf('Dvoutisíckoruny z %d na %d', $safeBag->getTwoThousandCrown(), $canonizedValues->twoThousandCrown);
                $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
                $this->entityManager->persist($safeBagLog);
            }

            if ($safeBag->getFiveThousandCrown() !== $canonizedValues->fiveThousandCrown) {
                $log = sprintf('Pětitisícikoruny z %d na %d', $safeBag->getFiveThousandCrown(), $canonizedValues->fiveThousandCrown);
                $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
                $this->entityManager->persist($safeBagLog);
            }
        }

        if ($safeBag->getOneEuroCent() !== $canonizedValues->oneEuroCent) {
            $log = sprintf('Jednoeurocentové mince z %d na %d', $safeBag->getOneEuroCent(), $canonizedValues->oneEuroCent);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        if ($safeBag->getTwoEuroCent() !== $canonizedValues->twoEuroCent) {
            $log = sprintf('Dvoueurocentové mince z %d na %d', $safeBag->getTwoEuroCent(), $canonizedValues->twoEuroCent);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        if ($safeBag->getFiveEuroCent() !== $canonizedValues->fiveEuroCent) {
            $log = sprintf('Pětieurocentové mince z %d na %d', $safeBag->getFiveEuroCent(), $canonizedValues->fiveEuroCent);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        if ($safeBag->getTenEuroCent() !== $canonizedValues->tenEuroCent) {
            $log = sprintf('Desetieurocentové mince z %d na %d', $safeBag->getTenEuroCent(), $canonizedValues->tenEuroCent);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        if ($safeBag->getTwentyEuroCent() !== $canonizedValues->twentyEuroCent) {
            $log = sprintf('Dvacetieurocentové mince z %d na %d', $safeBag->getTwentyEuroCent(), $canonizedValues->twentyEuroCent);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        if ($safeBag->getFiftyEuroCent() !== $canonizedValues->fiftyEuroCent) {
            $log = sprintf('Padesátieurocentové mince z %d na %d', $safeBag->getFiftyEuroCent(), $canonizedValues->fiftyEuroCent);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        if ($safeBag->getOneEuro() !== $canonizedValues->oneEuro) {
            $log = sprintf('Jednoeurové mince z %d na %d', $safeBag->getOneEuro(), $canonizedValues->oneEuro);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        if ($safeBag->getTwoEuro() !== $canonizedValues->twoEuro) {
            $log = sprintf('Dvoueurové mince z %d na %d', $safeBag->getTwoEuro(), $canonizedValues->twoEuro);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        if ($safeBag->getFiveEuro() !== $canonizedValues->fiveEuro) {
            $log = sprintf('Pětieurové bankovky z %d na %d', $safeBag->getFiveEuro(), $canonizedValues->fiveEuro);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        if ($safeBag->getTenEuro() !== $canonizedValues->tenEuro) {
            $log = sprintf('Desetieurové bankovky z %d na %d', $safeBag->getTenEuro(), $canonizedValues->tenEuro);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        if ($safeBag->getTwentyEuro() !== $canonizedValues->twentyEuro) {
            $log = sprintf('Dvacetieurové bankovky z %d na %d', $safeBag->getTwentyEuro(), $canonizedValues->twentyEuro);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        if ($safeBag->getFiftyEuro() !== $canonizedValues->fiftyEuro) {
            $log = sprintf('Padesátieurové bankovky z %d na %d', $safeBag->getFiftyEuro(), $canonizedValues->fiftyEuro);
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        $newCards = new Money($canonizedValues->cards, $this->getCurrency());
        if (!$safeBag->getCards()->equals($newCards)) {
            $log = sprintf('Karty z %s na %s', Filters::money($safeBag->getCards()), Filters::money($newCards));
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        $newMealVouchers = new Money($canonizedValues->mealVouchers, $this->getCurrency());
        if (!$safeBag->getMealVouchers()->equals($newMealVouchers)) {
            $log = sprintf('Stravenky z %s na %s', Filters::money($safeBag->getMealVouchers()), Filters::money($newMealVouchers));
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        $newPromoVouchers = new Money($canonizedValues->promoVouchers, $this->getCurrency());
        if (!$safeBag->getPromoVouchers()->equals($newPromoVouchers)) {
            $log = sprintf('Promo vouchery z %s na %s', Filters::money($safeBag->getPromoVouchers()), Filters::money($newPromoVouchers));
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }

        $newWithdrawals = new Money($canonizedValues->withdrawals, $this->getCurrency());
        if (!$safeBag->getWithdrawals()->equals($newWithdrawals)) {
            $log = sprintf('Výběry z %s na %s', Filters::money($safeBag->getWithdrawals()), Filters::money($newWithdrawals));
            $safeBagLog = new SafeBagLog($safeBag, $log, $this->employee);
            $this->entityManager->persist($safeBagLog);
        }
    }



    private function isCzk(): bool
    {
        return $this->safeBag->getShop()->getWarehouse()->hasCzkCurrency();
    }



    private function getCurrency(): Currency
    {
        return $this->safeBag->getShop()->getCurrency();
    }



    private function getCashRegisterBaseAmount(): int
    {
        if ($this->isCzk()) {
            return 2000;
        }

        return 50;
    }



    private function getLoyaltyCardsCount(): int
    {
        return $this->entityManager->getRepository(Order::class)
            ->createQueryBuilder('o')
            ->select('COUNT(o)')
            ->andWhere('o.discount = :discount')->setParameter('discount', Order::DISCOUNT_LOYALTY)
            ->andWhere('o.shop = :shop')->setParameter('shop', $this->safeBag->getShop())
            ->andWhere('o.createdAtCashDesk >= :startOfDay')->setParameter('startOfDay', (clone $this->safeBag->getDate())->setTime(0, 0, 0))
            ->andWhere('o.createdAtCashDesk <= :endOfDay')->setParameter('endOfDay', (clone $this->safeBag->getDate())->setTime(23, 59, 59))
            ->getQuery()->getSingleScalarResult();
    }

}
