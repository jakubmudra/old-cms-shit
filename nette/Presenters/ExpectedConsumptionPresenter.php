<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\Queries\IngredientsQuery;
use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\Services\ExpectedConsumptionService;
use App\Entities\Warehouses\Services\WarehouseService;
use App\Entities\Warehouses\Warehouse;
use App\Services\DateService;
use Nette\Utils\DateTime;



class ExpectedConsumptionPresenter extends BasePresenter
{

    /**
     * @inject
     * @var ExpectedConsumptionService
     */
    public $expectedConsumptionService;

    /**
     * @inject
     * @var WarehouseService
     */
    public $warehouseService;

    /**
     * @var DateTime
     */
    private $date;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isRegionalManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->date = $this->date;
        $this->template->dayBefore = $this->date->modifyClone('-1 day');
        $this->template->dayAfter = $this->date->modifyClone('+1 day');
        $this->template->today = (new DateTime())->setTime(0, 0, 0);
    }



    public function actionDefault(string $date = null)
    {
        $this->date = DateService::parseDateOrToday($date);
        $items = $this->expectedConsumptionService->calculateConsumption($this->date);

        $this->template->items = $items;
        $this->template->sum = $this->sumItems($items);
        $this->template->warehouses = $this->fetchWarehouses();
        $this->template->ingredients = $this->fetchIngredients();
    }



    /**
     * @param array $items result from $this->createItems()
     * @return array
     */
    private function sumItems(array $items) : array
    {
        $sum = [];

        foreach ($items as $warehouseId => $ingredients) {
            foreach ($ingredients as $ingredientId => $amount) {
                $sum[$ingredientId] = isset($sum[$ingredientId]) ? $sum[$ingredientId] + $amount : $amount;
            }
        }

        return $sum;
    }



    /**
     * @return Ingredient[]
     */
    private function fetchIngredients() : array
    {
        return $this->entityManager->fetch(
            IngredientsQuery::ingredientShrinkQuery(FALSE, TRUE, $this->getEmployee()->isManagement())
        )->toArray();
    }



    /**
     * @return Warehouse[]
     */
    private function fetchWarehouses()
    {
        return $this->warehouseService->getMySubordinateShopWarehouses();
    }

}
