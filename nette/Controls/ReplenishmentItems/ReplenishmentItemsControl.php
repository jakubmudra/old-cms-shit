<?php

namespace App\WarehouseModule\Controls\ReplenishmentItems;

use App\Entities\Employees\Employee;
use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\IngredientRepository;
use App\Entities\Warehouses\Replenishment;
use App\Entities\Warehouses\ReplenishmentItem;
use App\Entities\Warehouses\Services\SafeBagService;
use App\Forms\BaseForm;
use App\Helpers\Filters;
use App\Security\User;
use Doctrine\ORM\EntityManager;
use Money\Currency;
use Money\Money;
use Nette\Application\UI\Control;
use Nette\Utils\ArrayHash;



/**
 * @method onSuccess(ReplenishmentItemsControl $this, string $message)
 * @method onError(ReplenishmentItemsControl $this, string $message)
 */
class ReplenishmentItemsControl extends Control
{

    /**
     * @var array
     */
    public $onSuccess = [];

    /**
     * @var array
     */
    public $onError = [];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var SafeBagService
     */
    private $safeBagService;

    /**
     * @var Replenishment
     */
    private $replenishment;



    public function __construct(
        EntityManager $entityManager,
        User $user,
        SafeBagService $safeBagService,
        Replenishment $replenishment
    )
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->employee = $user->getEntity();
        $this->safeBagService = $safeBagService;
        $this->replenishment = $replenishment;
    }



    public function render()
    {
        $this->template->replenishment = $this->replenishment;
        $this->template->employee = $this->employee;

        $this->template->setFile(__DIR__ . '/default.latte');
        $this->template->render();
    }



    public function handleRemoveReplenishmentItem(int $replenishmentItemId)
    {
        if (!$this->employee->isShopManager()) {
            $this->onError($this, 'Na mazání položek nemáte dostatečná práva. Napište prosím vedení');
            return;
        }

        /** @var ReplenishmentItem|null $replenishmentItem */
        $replenishmentItem = $this->entityManager->find(ReplenishmentItem::class, $replenishmentItemId);
        if ($replenishmentItem === null) {
            $this->onError($this, 'Tato položka neexistuje');
            return;
        }

        if ($replenishmentItem->getReplenishment()->isCentralWarehouse() && !$this->employee->isManagement()) {
            $this->onError($this, 'Toto naskladnění nemáte právo editovat');
            return;
        }

        if ($replenishmentItem->getReplenishment()->isCheckedByFinances() && !$this->employee->isFinances()) {
            $this->onError($this, 'Tato faktura už byla potvrzená financemi. Manipulovat s ním už můžou opět pouze ty');
            return;

        } else if ($replenishmentItem->getReplenishment()->isCheckedByShopManager() && !$this->employee->isShopManager()) {
            $this->onError($this, 'Tato faktura už byla potvrzená vedoucím. Manipulovat s ním už můžou opět pouze MB nebo management');
            return;
        }

        $replenishment = $replenishmentItem->getReplenishment();
        $replenishment->removeItem($replenishmentItem);
        $this->entityManager->remove($replenishmentItem);
        $replenishment->updateTotalPrice();
        $this->entityManager->flush();

        if ($replenishment->getWarehouse()->isShop()) {
            $this->safeBagService->findOrCreateSafeBag($replenishment->getWarehouse()->getShop(), $replenishment->getDate()); // update price of replenishments
        }
        $this->entityManager->flush();

        $this->onSuccess($this, 'Položka smazána');
    }



    protected function createComponentAddReplenishmentItemForm()
    {
        $form = new BaseForm();
        $form->addProtection();
        $form->addSelect('ingredientId', 'Položka', $this->fetchIngredients())
            ->setPrompt('Vyberte položku')
            ->setRequired('Musíte vybrat položku');

        $form->addText('pieces', 'Počet')
            ->setRequired('Vyplňte prosím počet kusů')
            ->setType('number')
            ->setAttribute('step', 'any')
            ->addRule($form::FLOAT, 'Počet musí být číslo')
            ->addRule($form::NOT_EQUAL, 'Množství nemůže být 0', 0)
            ->addRule($form::RANGE, 'Množství musí být v rozsahu %d.něco a %d', [0, 1e6]);

        $allowedRange = $this->isCzk() ? [1, 1e6] : [1, 1e5];
        $form->addText('totalPrice', 'Celková cena (vč. DPH)')
            ->setRequired('Vyplňte prosím celkovou cenu (vč. DPH)')
            ->setType('number')
            ->setOption('currency', $this->getCurrency())
            ->setAttribute('step', 'any')
            ->addRule($form::FLOAT, 'Cena musí být číslo')
            ->addConditionOn($form['ingredientId'], $form::NOT_EQUAL, Ingredient::PRICE_CHANGE_ID)
            ->addRule(
                $form::RANGE,
                sprintf(
                    'Cena musí být v rozsahu %s - %s',
                    Filters::money(new Money($allowedRange[0], $this->getCurrency())),
                    Filters::money(new Money($allowedRange[1], $this->getCurrency()))
                ),
                $allowedRange
            );
        $form->addText('vat', 'Sazba DPH')
            ->setRequired('Sazba DPH je povinná')
            ->setType('number')
            ->addRule($form::INTEGER, 'Sazba DPH musí být číslo')
            ->addRule(
                $form::RANGE,
                sprintf(
                    'Sazba DPH musí být v rozsahu %s - %s',
                    0,
                    100
                ),
                [0, 100]
            );

        $form->addText('note', 'Poznámka')
            ->setAttribute('placeholder', 'Např. pokud nakupujete něco nestandardního / naskladňujete položku "Ostatní"')
            ->addConditionOn($form['ingredientId'], $form::EQUAL, Ingredient::OTHER_ID)
            ->setRequired('Pro položku "Ostatní" je pole "Poznámky" povinné');

        $form->addSubmit('send', 'Přidat');

        $form->onValidate[] = function (BaseForm $form, ArrayHash $values) {
            $replenishment = $this->replenishment;

            if ($replenishment->isCheckedByFinances() && !$this->employee->isFinances()) {
                $this->onError($this, 'Toto naskladnění už bylo zkontrolováno financemi. Manipulovat s ním už můžou opět pouze ty');
                return;

            } else if ($replenishment->isCheckedByShopManager() && !$this->employee->isShopManager()) {
                $this->onError($this, 'Toto naskladnění už bylo zkontrolováno vedoucím. Manipulovat s ním už můžou opět pouze MB nebo management');
                return;
            }

            /** @var Ingredient|null $ingredient */
            $ingredient = $this->entityManager->find(Ingredient::class, $values->ingredientId);

            if ($ingredient === null) {
                $this->onError($this, 'Tato ingredience neexistuje');
                return;
            }

            if ($ingredient->getVat() !== null && $ingredient->getVat() !== intval($values->vat)) {
                $this->onError($this, sprintf('Hodnota DPH u %s je stanovena na %d, nemůžete ji tedy upravovat', $ingredient->getName(), $ingredient->getVat()));
                return;
            }
        };

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $replenishment = $this->replenishment;
            /** @var Ingredient $ingredient */
            $ingredient = $this->entityManager->find(Ingredient::class, $values->ingredientId);
            $totalPrice = new Money((int) ($values->totalPrice * 100), $this->getCurrency());
            $replenishmentItem = new ReplenishmentItem($replenishment, $ingredient, $values->pieces, $totalPrice, $values->vat, $values->note);
            $this->entityManager->persist($replenishmentItem);
            $this->entityManager->flush();

            $replenishment = $replenishmentItem->getReplenishment();
            if ($replenishment->getWarehouse()->isShop()) {
                $this->safeBagService->findOrCreateSafeBag($replenishment->getWarehouse()->getShop(), $replenishment->getDate()); // update price of replenishments
            }
            $this->entityManager->flush();

            $this->onSuccess($this, sprintf('Předmět %s přidán.', $ingredient->getName()));
            return;
        };

        return $form;
    }



    protected function createComponentReplenishmentItemsForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        foreach ($this->replenishment->getItems() as $replenishmentItem) {
            $id = $replenishmentItem->getId();
            $form->addSelect('ingredientId' . $id, 'Položka', $this->fetchIngredients())
                ->setValue($replenishmentItem->getIngredient()->getId());

            $form->addText('pieces' . $id, 'Počet')
                ->setValue($replenishmentItem->getPieces())
                ->setRequired('Vyplňte prosím počet kusů')
                ->setType('number')
                ->setAttribute('step', 'any')
                ->addRule($form::FLOAT, 'Počet musí být číslo')
                ->addRule($form::NOT_EQUAL, 'Množství nemůže být 0', 0)
                ->addRule($form::RANGE, 'Množství musí být v rozsahu %d.něco a %d', [0, 1e6]);

            $allowedRange = $this->isCzk() ? [1, 1e6] : [1, 1e5];
            $form->addText('totalPrice' . $id, 'Cena (vč. DPH)')
                ->setValue($replenishmentItem->getTotalPrice()->getAmount() / 100)
                ->setRequired('Vyplňte prosím celkovou cenu (vč. DPH)')
                ->setOption('currency', $this->getCurrency())
                ->setType('number')
                ->setAttribute('step', 'any')
                ->addRule($form::FLOAT, 'Cena musí být číslo')
                ->addConditionOn($form['ingredientId' . $id], $form::NOT_EQUAL, Ingredient::PRICE_CHANGE_ID)
                ->addRule(
                    $form::RANGE,
                    sprintf(
                        'Cena musí být v rozsahu %s - %s',
                        Filters::money(new Money($allowedRange[0], $this->getCurrency())),
                        Filters::money(new Money($allowedRange[1], $this->getCurrency()))
                    ),
                    $allowedRange
                );

            $form->addText('vat' . $id, 'Sazba DPH')
                ->setRequired('Sazba DPH je povinná')
                ->setAttribute('readonly', $replenishmentItem->getIngredient()->getVat() !== null)
                ->setDefaultValue($replenishmentItem->getVat())
                ->setType('number')
                ->addRule($form::INTEGER, 'Sazba DPH musí být číslo')
                ->addRule(
                    $form::RANGE,
                    sprintf(
                        'Sazba DPH musí být v rozsahu %s - %s',
                        0,
                        100
                    ),
                    [0, 100]
                );

            $form->addText('note' . $id, 'Poznámka')
                ->setValue($replenishmentItem->getNote())
                ->addConditionOn($form['ingredientId' . $id], $form::EQUAL, Ingredient::OTHER_ID)
                ->setRequired('Pro položku "Ostatní" je pole "Poznámky" povinné');
        }

        $form->onValidate[] = function (BaseForm $form, ArrayHash $values) {
            if ($this->replenishment->isCheckedByFinances() && !$this->employee->isFinances()) {
                $this->onError($this, 'Toto naskladnění už bylo zkontrolováno financemi. Manipulovat s ní už můžou opět pouze ty');
                return;

            } else if ($this->replenishment->isCheckedByShopManager() && !$this->employee->isShopManager()) {
                $this->onError($this, 'Toto naskladnění už bylo zkontrolováno vedoucím. Manipulovat s ním už můžou opět pouze MB nebo management');
                return;
            }

            $replenishment = $this->replenishment;

            foreach ($replenishment->getItems() as $replenishmentItem) {
                $id = $replenishmentItem->getId();

                if (!isset($values['vat' . $id])) {
                    return;
                }

                /** @var Ingredient $ingredient */
                $ingredient = $this->entityManager->find(Ingredient::class, $values['ingredientId' . $id]);

                if ($ingredient->getVat() !== null && $ingredient->getVat() !== intval($values['vat' . $id])) {
                    $this->onError($this, sprintf('Hodnota DPH u %s je stanovena na %d, nemůžete ji tedy upravovat', $ingredient->getName(), $ingredient->getVat()));
                    return;
                }
            }

        };

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $replenishment = $this->replenishment;

            foreach ($replenishment->getItems() as $replenishmentItem) {
                $id = $replenishmentItem->getId();

                /** @var Ingredient $ingredient */
                $ingredient = $this->entityManager->find(Ingredient::class, $values['ingredientId' . $id]);

                $replenishmentItem->updateIngredient($ingredient);
                $replenishmentItem->updatePieces($values['pieces' . $id]);
                $replenishmentItem->setTotalPrice(new Money((int) ($values['totalPrice' . $id] * 100), $this->getCurrency()));
                $replenishmentItem->setNote($values['note' . $id]);

                if (isset($values['vat' . $id])) {
                    $replenishmentItem->setVat(intval($values['vat' . $id]));
                }
            }

            $replenishment->updateTotalPrice();
            $this->entityManager->flush();

            if ($replenishment->getWarehouse()->isShop()) {
                $this->safeBagService->findOrCreateSafeBag($replenishment->getWarehouse()->getShop(), $replenishment->getDate()); // update price of replenishments
            }
            $this->entityManager->flush();

            $this->onSuccess($this, 'Uloženo');
            return;
        };

        $form->addSubmit('send', 'Změnit');

        return $form;
    }



    /**
     * @return array - [id:int => name:string]
     */
    private function fetchIngredients(): array
    {
        /** @var IngredientRepository $ingredientsRepository */
        $ingredientsRepository = $this->entityManager->getRepository(Ingredient::class);
        return $ingredientsRepository->fetchIngredientsForSelect(
            $this->employee->isManagement(),
            $this->employee->isManagement(),
            $this->employee->isRegionalManager() || !$this->replenishment->getWarehouse()->isShop()
        );
    }



    private function isCzk(): bool
    {
        return $this->replenishment->getWarehouse()->hasCzkCurrency();
    }



    private function getCurrency(): Currency
    {
        return $this->replenishment->getWarehouse()->getCurrency();
    }

}
