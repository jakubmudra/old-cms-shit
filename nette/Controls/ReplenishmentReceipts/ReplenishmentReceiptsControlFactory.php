<?php

namespace App\WarehouseModule\Controls\ReplenishmentReceipts;

use App\Entities\Warehouses\Replenishment;



interface ReplenishmentReceiptsControlFactory
{

    function create(Replenishment $replenishment): ReplenishmentReceiptsControl;

}
