<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Employees\Employee;
use App\Entities\Employees\Login;
use App\Entities\Shops\Services\ShopService;
use App\Services\DateService;
use Nette\Utils\DateTime;



class LoginsDashboardPresenter extends BasePresenter
{

    /**
     * @inject
     * @var ShopService
     */
    public $shopService;

    /**
     * @var DateTime
     */
    private $date;



    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->date = $this->date;
        $this->template->dayBefore = $this->date->modifyClone('-1 day');
        $this->template->dayAfter = $this->date->modifyClone('+1 day');
        $this->template->today = (new DateTime())->setTime(0, 0, 0);
    }



    public function actionDefault(string $date = NULL)
    {
        $this->date = DateService::parseDateOrToday($date);

        $logins = $this->entityManager->getRepository(Login::class)->createQueryBuilder('l')
            ->addSelect('s')
            ->innerJoin('l.employee', 'e')
            ->innerJoin('l.shop', 's')
            ->andWhere('l.loginAtApp >= :today')->setParameter('today', $this->date)
            ->andWhere('l.loginAtApp < :tomorrow')->setParameter('tomorrow', $this->date->modifyClone('+1 day'))
            ->andWhere('l.logoutAtApp - l.loginAtApp > 60 OR l.logoutAtApp IS NULL') // only logins with at least one minute
            ->andWhere('s.deleted != TRUE')
            ->andWhere('s.id IN (:shopIds)')->setParameter('shopIds', array_keys($this->shopService->getMySubordinateShops()))
            ->orderBy('s.code')
            ->addOrderBy('l.loginAtApp')
            ->getQuery()->getResult();
        $this->template->regions = $this->transformByRegionsAndShops($logins);
    }



    /**
     * @param Login[] $logins
     * @return array
     */
    private function transformByRegionsAndShops(array $logins)
    {
        $regions = [];

        foreach ($logins as $login) {
            $shop = $login->getShop();
            $regional = $shop->getRegionalManager();
            $regions[$regional->getId()][$shop->getCode()][] = $login;
        }

        return $regions;
    }

}
