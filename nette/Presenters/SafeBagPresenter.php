<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Keepers\Keeper;
use App\Entities\Orders\Order;
use App\Entities\Shops\Services\ShopService;
use App\Entities\Shops\Shop;
use App\Entities\Warehouses\SafeBag;
use App\Entities\Warehouses\SafeBagFile;
use App\Entities\Warehouses\SafeBagLog;
use App\Entities\Warehouses\Services\SafeBagService;
use App\Entities\Warehouses\Warehouse;
use App\Entities\Employees\Employee;
use App\Forms\BaseForm;
use App\Services\DateService;
use App\WarehouseModule\Controls\SafeBag\SafeBagControl;
use App\WarehouseModule\Controls\SafeBag\SafeBagControlFactory;
use App\WarehouseModule\DTOs\DepositForm;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use App\Security\User;
use Money\Currency;
use Money\Money;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;
use Tracy\ILogger;


/**
 * Note: there used to be camera recording for counting money - new browser versions broke it
 * Since it is not currently used I've nuked it.
 * Relevant commit e912a390 or history of file `/App/WarehouseModule/templates/SafeBag/detail.latte`
 */
class SafeBagPresenter extends BasePresenter
{

    /**
     * @inject
     * @var ShopService
     */
    public $shopService;

    /**
     * @inject
     * @var SafeBagService
     */
    public $safeBagService;

    /**
     * @inject
     * @var SafeBagControlFactory
     */
    public $safeBagComponentFactory;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * Caching purposes
     * @var SafeBag
     */
    private $safeBag;

    /**
     * @var Employee
     */
    private $employee;

    /**
     * @inject
     * @var User
     */
    public $user;

    /**
     * @inject
     * @var ILogger
     */
    public $logger;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isSeller() && !$this->getEmployee()->isDriver()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function createComponentSearchSafeBagNumberForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addText('number', 'Číslo sáčku')
            ->setRequired();
        $form->addSubmit('send', 'Vyhledat');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            /** @var SafeBag|NULL $safeBag */
            $safeBag = $this->entityManager->getRepository(SafeBag::class)->findOneBy(['number' => $values->number]);
            if ($safeBag === null) {
                $this->dangerFlashMessage('Tento safebag neexistuje');
                return;
            }

            $this->successFlashMessage('SafeBag nalezen. Přesměrovávám...');
            $this->redirect('detail', [
                'id' => $safeBag->getShop()->getCode(),
                'date' => $safeBag->getDate()->format('Y-m-d')
            ]);
        };

        return $form;
    }


    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->date = $this->date;
        $this->template->dayBefore = $this->date->modifyClone('-1 day');
        $this->template->dayAfter = $this->date->modifyClone('+1 day');
        $this->template->today = (new DateTime())->setTime(0, 0, 0);
    }



    public function actionDefault(string $date = NULL)
    {
        $this->date = DateService::parseDateOrToday($date);

        if ($this->date->format('Y-m-d') !== (new \DateTime())->format('Y-m-d') && !$this->getEmployee()->isDriver() && !$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Safebagy jdou vždy vyplňovat pouze v současném dni, přesměrovávám...');
            $this->redirect('this', ['date' => (new \DateTime())->format('Y-m-d')]);
        }

        $shops = $this->shopService->getAvailableShops();
        $this->template->shops = $this->entityManager->getRepository(Shop::class)->createQueryBuilder('s', 's.id')
            ->addSelect('w, sb')
            ->innerJoin('s.warehouse', 'w')
            ->leftJoin('s.safeBags', 'sb', Join::WITH, 'sb.date = :today')->setParameter('today', $this->date)
            ->andWhere('s.id IN (:shops)')->setParameter('shops', $shops)
            ->getQuery()->getResult();
    }



    public function actionDetail(string $id, string $date = NULL)
    {
        $this->date = DateService::parseDateOrToday($date);

        if ($this->date->format('Y-m-d') !== (new \DateTime())->format('Y-m-d') && !$this->getEmployee()->isShopManager() && !$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Safebagy lze vždy vyplňovat pouze v současném dni, přesměrovávám...');
            $this->redirect('this', ['id' => $id, 'date' => (new \DateTime())->format('Y-m-d')]);
        }

        $this->template->warehouse = $this->getWarehouse();
        $this->template->safeBag = $this->getSafeBag();
        $this->template->safeBagLogs = $this->getSafeBagLogs();

        $this->template->sales = $this->getSales();
        $this->template->lastOrderDate = $this->getLastOrderDate();
    }



    public function renderDetail()
    {
        $filledBy = $this->getSafeBag()->getFilledBy();
        $constantNumber = $filledBy !== NULL ? $filledBy->getId() : NULL;
        $name = $filledBy !== NULL ? $filledBy->getName() : NULL;
        $birthDate = $filledBy !== NULL ? $filledBy->getBirthDate() : NULL;
        $address = $filledBy !== NULL ? $filledBy->getFullAddress() : NULL;
        $this->template->reports = $this->entityManager->getRepository(SafeBagFile::class)
            ->findBy(['safeBag' => $this->safeBag], ['uploadedAt' => 'DESC']);
        $this->template->employee = $this->user->getEntity();

        $keeper = $this->getKeeper();

        if ($this->getSafeBag()->isFilled() && $keeper->hasSafeBagToBank()) {
            $this->template->bankAccountCzk = [
                'currency' => 'CZK',
                'bankAccountNumber' => $keeper->getBankAccountNumberCzk(),
                'bankAccountCode' => $keeper->getBankAccountCodeCzk(),
                'name' => $name,
                'birthDate' => $birthDate->format('d.m.Y'),
                'address' => $address,
                'variableNumber' => $this->getSafeBag()->getVariableNumber(),
                'constantNumber' => $constantNumber,
                'company' => $keeper->getCompany(),
                'identityCard' => 'číslo vašeho OP / ŘP',
            ];

            $this->template->bankAccountEur = [
                'currency' => 'EUR',
                'bankAccountNumber' => $keeper->getBankAccountNumberEur(),
                'bankAccountCode' => $keeper->getBankAccountCodeEur(),
                'name' => $name,
                'birthDate' => $birthDate->format('d.m.Y'),
                'address' => $address,
                'variableNumber' => $this->getSafeBag()->getVariableNumber(),
                'constantNumber' => $constantNumber,
                'company' => $keeper->getCompany(),
                'identityCard' => 'číslo vašeho OP / ŘP',
            ];

            $this->template->depositForm = new DepositForm($this->getSafeBag());
        }
    }



    public function createComponentChangeStatusForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $safeBag = $this->getSafeBag();

            $this->entityManager->flush();
            $this->successFlashMessage('Stavy změněny');
            $this->redirect('this');
        };


        return $form;
    }



    protected function createComponentSafeBag(): SafeBagControl
    {
        $control = $this->safeBagComponentFactory->create($this->getSafeBag());
        $control->onSuccess[] = function (SafeBagControl $safeBagControl) {
            $this->successFlashMessage('SafeBag byl uložen');
            $this->redirect('this');
        };

        return $control;
    }



    private function getWarehouse() : Warehouse
    {
        /** @var Warehouse|null $warehouse */
        $warehouse = $this->entityManager->getRepository(Warehouse::class)->findOneBy(['code' => $this->getParameter('id')]);
        if ($warehouse === null) {
            $this->dangerFlashMessage('Tento sklad/obchod neexistuje');
            $this->redirect('default');
        }

        return $warehouse;
    }



    private function getSafeBag(): SafeBag
    {
        if ($this->safeBag === null) {
            $this->safeBag = $this->safeBagService->findOrCreateSafeBag($this->getWarehouse()->getShop(), $this->date);
        }

        return $this->safeBag;
    }



    /**
     * @return SafeBagLog[]
     */
    private function getSafeBagLogs(): array
    {
        return $this->entityManager->getRepository(SafeBagLog::class)->createQueryBuilder('sbl')
            ->andWhere('sbl.safeBag = :safeBagId')->setParameter('safeBagId', $this->getSafeBag()->getId())
            ->orderBy('sbl.createdAt', 'DESC')
            ->getQuery()->getResult();
    }


    protected function createComponentUploadForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addMultiUpload('files', 'Uzávěrka z terminálu')
            ->addRule(Form::IMAGE, 'Uzávěrka musí být JPEG, PNG nebo GIF.')
            ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 10 MB.', 10 * 1024 * 1024)
            ->setAttribute('accept', 'image/*')
            ->setRequired('Pro nahrání uzávěrky zvolte soubor');

        $form->addSubmit('send', 'Nahrát uzávěrku');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            /** @var \Nette\Http\FileUpload[] $files */
            $files = $values['files'];

            $folder = sprintf('uploaded-safeBagReports/%s/', $this->safeBag->getId());

            foreach ($files as $file) {
                $extension = Strings::lower(Strings::substring($file->getSanitizedName(), strrpos($file->getSanitizedName(), ".")));
                $randomNamePart = uniqid(rand(0, 20), TRUE);
                $fileName = sprintf('Report_%s%s', $randomNamePart, $extension);
                $serverPath = $folder . $fileName;

                $file->move($serverPath);

                $report = new SafeBagFile(
                    $this->safeBag,
                    $file->getSanitizedName(),
                    $serverPath,
                    $this->user->getEntity()
                );
                $this->entityManager->persist($report);
                $this->entityManager->flush();

                $logMsg = sprintf('Employee %s uploaded a report %s for safebag %s',
                    $this->employee, $report->getOriginalName(), $this->safeBag->getId());
                $this->logger->log($logMsg);
            }

        };

        return $form;
    }



    public function handleDeleteReport(int $reportId, int $userId)
    {
        /** @var SafeBagFile|NULL $report */
        $report = $this->entityManager->find(SafeBagFile::class, $reportId);
        if ($report === NULL) {
            return;
        }
        $originalName = $report->getOriginalName();

        unlink($report->getFilePath());
        $this->entityManager->remove($report);
        $this->entityManager->flush();

        $logMsg = sprintf('Employee %s deleted a report %s for replenishment %s', $userId, $originalName, $this->safeBag->getId());
        $this->logger->log($logMsg);
    }



    private function baseSalesQuery(): QueryBuilder
    {
        return $this->entityManager->getRepository(Order::class)->createQueryBuilder('o')
            ->innerJoin('o.shop', 's')
            ->andWhere('o.createdAtCashDesk >= :todayStart')->setParameter('todayStart', $this->date->modifyClone('today'))
            ->andWhere('o.createdAtCashDesk <= :todayEnd')->setParameter('todayEnd', $this->date->modifyClone('tomorrow'))
            ->andWhere('o.shop = :shop')->setParameter('shop', $this->getSafeBag()->getShop())
            ->andWhere('o.totalPrice < 1000000')
            ->andWhere('o.paidTrdlomoney < 100000')
            ->andWhere('o.paidCash < 1000000')
            ->andWhere('o.paidCash != 99900')
            ->andWhere('o.paidTrdlomoney != 99900');
    }



    /**
     * @return array(id:string => amount:Money)
     */
    private function getSales()
    {
        $sales = $this->baseSalesQuery()
            ->select('
                SUM(o.paidCard) AS card,
                SUM(o.paidTrdlomoney) AS trdlomoney,
                SUM(o.paidMealVoucher) AS mealVoucher,
                SUM(o.paidEUR) AS eur,
                SUM(o.totalPrice - o.paidWarrantyClaim) AS total
            ')
            ->getQuery()->getSingleResult();

        $sales['card'] = new Money($sales['card'], $this->getSafeBag()->getShop()->getCurrency());
        $sales['trdlomoney'] = new Money($sales['trdlomoney'], $this->getSafeBag()->getShop()->getCurrency());
        $sales['mealVoucher'] = new Money($sales['mealVoucher'], $this->getSafeBag()->getShop()->getCurrency());
        $sales['eur'] = new Money($sales['eur'], new Currency(Warehouse::CURRENCY_EUR));
        $sales['total'] = new Money($sales['total'], $this->getSafeBag()->getShop()->getCurrency());

        if ($this->getSafeBag()->getShop()->hasEurCurrency()) {
            unset($sales['eur']);
        }

        return $sales;
    }



    private function getLastOrderDate(): ?DateTime
    {
        /** @var Order|NULL $order */
        $order = $this->entityManager->getRepository(Order::class)->createQueryBuilder('o')
            ->andWhere('o.createdAtCashDesk >= :todayStart')->setParameter('todayStart', $this->date->modifyClone('today'))
            ->andWhere('o.createdAtCashDesk <= :todayEnd')->setParameter('todayEnd', $this->date->modifyClone('tomorrow'))
            ->andWhere('o.shop = :shop')->setParameter('shop', $this->getSafeBag()->getShop())
            ->orderBy('o.createdAtCashDesk', 'DESC')
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();

        if ($order === NULL) {
            return NULL;
        }

        return $order->getCreatedAtCashDesk();
    }



    private function getKeeper(): Keeper
    {
        // Twistcafe has its account since 2019-01-28, till then we want to show Trdlokafe
        if ($this->getSafeBag()->getShop()->getKeeper()->getId() === 2 && $this->getSafeBag()->getDate()->format('Y-m-d') < '2019-01-28') {
            return $this->entityManager->find(Keeper::class, 1);
        }

        return $this->getSafeBag()->getShop()->getKeeper();
    }

}
