<?php

namespace App\WarehouseModule\Controls\SafeBag;

use App\Entities\Warehouses\SafeBag;



interface SafeBagControlFactory
{

    function create(SafeBag $safeBag): SafeBagControl;

}
