<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\InventoryItem;
use App\Entities\Warehouses\Recipe;
use App\Forms\BaseForm;
use Nette\Utils\ArrayHash;



class RecipePresenter extends BasePresenter
{

    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isManagement()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function renderDefault()
    {
        $this->template->inventoryItems = $this->fetchInventoryItems();
    }



    public function renderDetail(int $id)
    {
        $this->template->inventoryItem = $this->getInventoryItem();
    }



    protected function createComponentForm()
    {
        $inventoryItem = $this->getInventoryItem();

        $form = new BaseForm();
        $form->addProtection();

        for ($i = 0; $i < 20; $i++) {
            $form->addSelect('ingredient' . $i, 'Ingredience')
                ->setPrompt('Vyberte surovinu')
                ->setItems($this->fetchIngredients())
                ->setRequired(false);

            $form->addText('amount' . $i, 'Ingredience ' . $i)
                ->setRequired(false)
                ->addRule($form::FLOAT)
                ->addRule($form::RANGE, 'Surovin může být od %d do %d', [0, 1e5]);

            /** @var Recipe|null $recipe */
            $recipe = $inventoryItem->getRecipes()->get($i);
            if ($recipe !== null) {
                $form['ingredient' . $i]->setValue($recipe->getIngredient()->getId());
                $form['amount' . $i]->setValue($recipe->getAmount());
            }
        }

        $form->addSubmit('send', 'Uložit');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) use ($inventoryItem) {
            foreach ($inventoryItem->getRecipes() as $recipe) {
                $this->entityManager->remove($recipe);
            }

            for ($i = 0; $i < 20; $i++) {
                $selectId = 'ingredient' . $i;
                $ingredientId = $values->$selectId;

                $inputId = 'amount' . $i;
                $amount = $values->$inputId;

                if ($ingredientId === null || $amount <= 0) {
                    continue;
                }

                /** @var Ingredient $ingredient */
                $ingredient = $this->entityManager->find(Ingredient::class, $ingredientId);

                $recipe = new Recipe($inventoryItem, $ingredient, $amount);
                $this->entityManager->persist($recipe);
            }

            $this->entityManager->flush();
            $this->successFlashMessage('Receptura uložena');
            $this->redirect('this');
        };

        return $form;
    }



    private function getInventoryItem() : InventoryItem
    {
        /** @var InventoryItem|null $inventoryItem */
        $inventoryItem = $this->entityManager->find(InventoryItem::class, $this->getParameter('id'));
        if ($inventoryItem === null) {
            $this->dangerFlashMessage('Tento předmět neexistuje');
            $this->redirect('default');
        }

        return $inventoryItem;
    }



    /**
     * @return InventoryItem[]
     */
    private function fetchInventoryItems() : array
    {
        return $this->entityManager->getRepository(InventoryItem::class)->findBy(['deleted' => FALSE]);
    }



    /**
     * @return [id:int => name:string]
     */
    private function fetchIngredients(): array
    {
        $items = [];

        /** @var Ingredient[] $ingredients */
        $ingredients = $this->entityManager->getRepository(Ingredient::class)->findBy([], ['deleted' => 'ASC', 'name' => 'ASC']);
        foreach ($ingredients as $ingredient) {
            $items[$ingredient->getId()] = $ingredient->getName() . " [" . $ingredient->getUnit() . "]";
        }

        return $items;
    }

}
