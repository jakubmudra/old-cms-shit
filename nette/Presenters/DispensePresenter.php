<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Shops\Services\ShopService;
use App\Entities\Shops\Shop;
use App\Entities\Warehouses\InventoryItem;
use App\Entities\Warehouses\Sale;
use App\Entities\Warehouses\Shrink;
use App\Forms\BaseForm;
use App\Services\DateService;
use Doctrine\ORM\Query\Expr\Join;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;



class DispensePresenter extends BasePresenter
{

    /**
     * @inject
     * @var ShopService
     */
    public $shopService;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * Only for caching
     *
     * @var Sale[]
     */
    private $sales;

    /**
     * Only for caching
     *
     * @var Shrink[]
     */
    private $shrinks;



    public function startup()
    {
        parent::startup();

        if (!$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->date = $this->date;
        $this->template->dayBefore = $this->date->modifyClone('-1 day');
        $this->template->dayAfter = $this->date->modifyClone('+1 day');
        $this->template->today = (new DateTime())->setTime(0, 0, 0);
    }



    public function actionDefault(string $date = NULL)
    {
        $this->date = DateService::parseDateOrToday($date);
        $this->template->shops = $this->getShops();
    }



    public function actionDetail(string $id, string $date = NULL)
    {
        $this->date = DateService::parseDateOrToday($date);
        $this->template->shop = $this->getShop();
        $this->template->inventoryItems = $this->getInventoryItems();
    }



    protected function createComponentDispensesForm()
    {
        $shop = $this->getShop();
        $inventoryItems = $this->getInventoryItems();

        $form = new BaseForm();
        $form->addProtection();

        $form->addGroup('Obecné');
        $form->addText('shop', 'Prodejna')
            ->setDisabled()
            ->setValue($shop->getName());

        $form->addText('date', 'Datum inventury')
            ->setDisabled()
            ->setValue($this->date->format('Y-m-d'));

        $form->addGroup('Suroviny');
        foreach ($inventoryItems as $inventoryItem) {
            $sale = $this->getSaleForInventoryItem($inventoryItem);
            $shrink = $this->getShrinkForInventoryItem($inventoryItem);

            $form->addText('sale' . $inventoryItem->getId(), 'Prodej')
                ->setDisabled(!$this->hasWriteAccess())
                ->setValue($sale->getAmount());
            $form->addText('shrink' . $inventoryItem->getId(), 'Odpad')
                ->setDisabled(!$this->hasWriteAccess())
                ->setValue($shrink->getAmount());
        }

        $form->addSubmit('send', 'Aktualizovat')
            ->setDisabled(!$this->hasWriteAccess());

        $form->onSuccess[] = function (Form $form, ArrayHash $values) use ($inventoryItems) {
            if (!$this->hasWriteAccess()) {
                $this->dangerFlashMessage('Na editaci položek nemáte dostatečná práva. Napište prosím vedení');
                return;
            }

            foreach ($inventoryItems as $inventoryItem) {
                $saleInputName = 'sale' . $inventoryItem->getId();
                $shrinkInputName = 'shrink' . $inventoryItem->getId();

                $sale = $this->getSaleForInventoryItem($inventoryItem);
                $sale->setAmount($values->$saleInputName);
                $this->entityManager->persist($sale);

                $shrink = $this->getShrinkForInventoryItem($inventoryItem);
                $shrink->setAmount($values->$shrinkInputName);
                $this->entityManager->persist($shrink);
            }

            $this->entityManager->flush();
            $this->successFlashMessage('Množství aktualizováno');
        };

        return $form;
    }



    /**
     * @return Shop[]
     */
    private function getShops(): array
    {
        $shops = $this->shopService->getMySubordinateShops();

        return $this->entityManager->getRepository(Shop::class)->createQueryBuilder('s')
            ->addSelect('s, w, sale, shrink')
            ->innerJoin('s.warehouse', 'w')
            ->leftJoin('w.sales', 'sale', Join::WITH, 'sale.date = :date')
            ->leftJoin('w.shrinks', 'shrink', Join::WITH, 'shrink.date = :date')
            ->setParameter('date', $this->date)
            ->andWhere('s.id IN (:shops)')->setParameter('shops', $shops)
            ->getQuery()->getResult();
    }



    /**
     * @TODO: optimize to query builder with post fetch
     */
    private function getShop(): Shop
    {
        /** @var Shop|null $shop */
        $shop = $this->entityManager->getRepository(Shop::class)->findOneBy(['code' => $this->getParameter('id')]);
        if ($shop === NULL) {
            $this->dangerFlashMessage('Tato prodejna neexistuje');
            $this->redirect('default');
        }

        if (!in_array($shop, $this->shopService->getMySubordinateShops(), true)) {
            $this->dangerFlashMessage('Tuto prodejnu nespravujete vy, nemůžete se dívat na její prodeje.');
            $this->redirect('default');
        }

        return $shop;
    }



    /**
     * @return InventoryItem[]
     */
    private function getInventoryItems(): array
    {
        return $this->entityManager->getRepository(InventoryItem::class)->findBy(['deleted' => FALSE]);
    }



    private function getSaleForInventoryItem(InventoryItem $inventoryItem): Sale
    {
        foreach ($this->getSales() as $sale) {
            if ($sale->getInventoryItem() === $inventoryItem) {
                return $sale;
            }
        }

        return new Sale($inventoryItem, $this->getShop()->getWarehouse(), $this->date);
    }



    /**
     * @return Sale[]
     */
    private function getSales(): array
    {
        if ($this->sales === NULL) {
            $this->sales = $this->entityManager->getRepository(Sale::class)->findBy([
                'date' => $this->date,
                'warehouse' => $this->getShop()->getWarehouse(),
            ]);;
        }

        return $this->sales;
    }



    private function getShrinkForInventoryItem(InventoryItem $inventoryItem): Shrink
    {
        foreach ($this->getShrinks() as $shrink) {
            if ($shrink->getInventoryItem() === $inventoryItem) {
                return $shrink;
            }
        }

        return new Shrink($inventoryItem, $this->getShop()->getWarehouse(), $this->date);
    }



    /**
     * @return Shrink[]
     */
    private function getShrinks(): array
    {
        if ($this->shrinks === NULL) {
            $this->shrinks = $this->entityManager->getRepository(Shrink::class)->findBy([
                'date' => $this->date,
                'warehouse' => $this->getShop()->getWarehouse(),
            ]);
        }

        return $this->shrinks;
    }



    private function hasWriteAccess(): bool
    {
        return $this->getEmployee()->isManagement();
    }

}
