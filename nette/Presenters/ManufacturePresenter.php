<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\Manufacture;
use App\Entities\Warehouses\Services\WarehouseService;
use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;



class ManufacturePresenter extends BasePresenter
{

    /**
     * @inject
     * @var WarehouseService
     */
    public $warehouseService;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isJirinka()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function renderDefault()
    {
        $this->template->manufactures = $this->entityManager->getRepository(Manufacture::class)
            ->findBy([], ['date' => 'DESC', 'id' => 'DESC'], 50);
    }



    protected function createComponentManufactureForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addSelect('warehouseId', 'Sklad', $this->fetchWarehouses())
            ->setPrompt('Vyberte sklad')
            ->setDefaultValue(Warehouse::CENTRAL_WAREHOUSE_ID)
            ->setRequired();

        $form->addInteger('producedMixture', 'Vyprodukované směsi')
            ->setOption('unit', 'ks')
            ->setRequired(FALSE)
            ->addRule($form::RANGE, 'Vyrobených směsí musí být od %d do %d ks', [0, 1e3]);

        $form->addSubmit('send', 'Vyrobit');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            /** @var Warehouse|null $warehouse */
            $warehouse = $this->entityManager->find(Warehouse::class, $values->warehouseId);
            if ($warehouse === null) {
                $form->addError('Sklad neexistuje');
                return;
            }

            $canonized = $this->canonizeValues($values);

            $manufacture = new Manufacture($warehouse, new \DateTime());
            $manufacture->setProducedMixture($canonized['producedMixture']);

            $this->entityManager->persist($manufacture);
            $this->entityManager->flush();

            $this->successFlashMessage('Výroba zadána do systému');
            $this->redirect('this');
        };

        return $form;
    }



    private function canonizeValues(ArrayHash $values)
    {
        $canonized = [];
        $canonized['producedMixture'] = $values->producedMixture ?: 0;
        return $canonized;
    }



    /**
     * @return Warehouse[]
     */
    private function fetchWarehouses() : array
    {
        return $this->warehouseService->getAllWarehouses();
    }

}
