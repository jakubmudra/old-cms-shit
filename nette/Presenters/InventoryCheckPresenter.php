<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\InventoryCheck;
use App\Entities\Warehouses\Queries\IngredientsQuery;
use App\Entities\Warehouses\Replenishment;
use App\Entities\Warehouses\ReplenishmentItem;
use App\Entities\Warehouses\Services\WarehouseService;
use App\Entities\Warehouses\Stock;
use App\Entities\Warehouses\Transfer;
use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use App\Services\DateService;
use Kdyby\Doctrine\ResultSet;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;



class InventoryCheckPresenter extends BasePresenter
{

    const GROUP_PIECES = 0;
    const GROUP_PACKAGES = 1;
    const GROUP_WEIGHT = 2;
    const GROUP_DECIMAL = 3;
    const GROUP_DURABLE = 4;

    /**
     * @inject
     * @var WarehouseService
     */
    public $warehouseService;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * Only for caching
     * @var Ingredient[]
     */
    private $ingredients;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isSeller() && !$this->getEmployee()->isWarehouseman()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function actionDefault()
    {
        $this->template->warehouses = $this->warehouseService->getAvailableWarehouses();
    }



    public function actionDetail(string $id, string $date = null)
    {
        $this->date = DateService::parseDateOrToday($date);

        if ($this->date->format('Y-m-d') !== (new \DateTime())->format('Y-m-d') && !$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Inventura lze vždy vyplňovat pouze v současném dni, přesměrovávám...');
            $this->redirect('this', ['id' => $id, 'date' => (new \DateTime())->format('Y-m-d')]);
        }

        if (!$this->getWarehouse()->isShop() && !$this->getEmployee()->isWarehouseman()) {
            $this->warningFlashMessage('Na tento sklad nemáte přístup.');
            $this->redirect('default', ['id' => $id, 'date' => (new \DateTime())->format('Y-m-d')]);
        }

        $this->template->warehouse = $this->getWarehouse();
        $this->template->date = $this->date;
        $this->template->dayBefore = $this->date->modifyClone('-1 day');
        $this->template->dayAfter = $this->date->modifyClone('+1 day');
        $this->template->today = (new DateTime())->setTime(0, 0, 0);
        $this->template->inventoryCheck = $this->fetchOrCreateInventoryCheck($this->getWarehouse(), $this->date);
        $this->template->ingredients = $this->fetchIngredients();
        $this->template->ingredientGroups = $this->createGroups();
        $this->template->stock = $this->entityManager->getRepository(Stock::class)->createQueryBuilder('s')
            ->addSelect('si')
            ->innerJoin('s.items', 'si')
            ->andWhere('s.date = :today')->setParameter('today', $this->date->modifyClone('-1 day'))
            ->andWhere('s.warehouse = :warehouse')->setParameter('warehouse', $this->getWarehouse())
            ->getQuery()->getOneOrNullResult() ?: new Stock($this->getWarehouse(), $this->date->modifyClone('-1 day'));
    }



    protected function createComponentInventoryCheckForm()
    {
        $warehouse = $this->getWarehouse();
        $inventoryCheck = $this->fetchOrCreateInventoryCheck($warehouse, $this->date);

        $form = new BaseForm();
        $form->addProtection();

        $form->addGroup('Inventura');

        $form->addText('warehouse', 'Prodejna')
            ->setDisabled()
            ->setValue($warehouse->getName());

        $form->addText('date', 'Datum inventury')
            ->setDisabled()
            ->setValue($this->date->format('Y-m-d'));

        $form->addGroup('Suroviny');
        foreach ($this->fetchIngredients() as $ingredient) {
            if (!$ingredient->hasUnitsOnly()) {
                $inputName = 'itemPieces' . $ingredient->getId();
                $inputType = $ingredient->hasDecimalPieces() ? $form::FLOAT : $form::INTEGER;
                $maxAmount = $ingredient->hasWeight() && $this->getWarehouse()->isShop() ? 30 : 1e6;
                $form->addText($inputName, $ingredient->getName())
                    ->setOption('unit', $ingredient->getUnitForInventoryCheck())
                    ->setOption('descriptionLeft', $this->generateDescriptionLeft($ingredient))
                    ->setOption('descriptionRight', $this->generateDescriptionRight($ingredient))
                    ->setType('number')
                    ->setAttribute('step', 'any')
                    ->setRequired()
                    ->addRule($inputType)
                    ->addRule($form::RANGE, 'Rozsah ks ' . $ingredient->getName() . ' musí být od %d do %d', [0, $maxAmount]);
            }

            if ($ingredient->hasWeight()) {
                $inputName = 'itemUnits' . $ingredient->getId();
                $maxWeight = $ingredient->getUnitsPerPieceBrutto() - 1;
                if ($ingredient->hasUnitsOnly() || $ingredient->getUnitsPerPieceTara() === 0) {
                    $maxWeight = 1e6;
                }

                $form->addText($inputName, $ingredient->getName())
                    ->setOption('unit', $ingredient->getUnit())
                    ->setOption('descriptionLeft', $this->generateDescriptionLeft($ingredient))
                    ->setOption('descriptionRight', $this->generateDescriptionRight($ingredient))
                    ->setRequired()
                    ->setType('number')
                    ->setAttribute('step', 'any')
                    ->addRule($form::FLOAT)
                    ->addCondition($form::NOT_EQUAL, 0)
                        ->addRule($form::RANGE,
                            'Rozsah ' . $ingredient->getUnit() . ' '  . $ingredient->getName() . ' musí být od %d do %d',
                            [$ingredient->getUnitsPerPieceTara(), $maxWeight]
                        );
            }
        }

        $form->addTextArea('note', 'Další přání')
            ->setAttribute('placeholder', 'Kniha přání a stížností na věci, které nepodléhají inventuře. (Tj. věci typu bedna na těsto, miska na posypky...)')
            ->setDefaultValue($inventoryCheck->getNote())
            ->setRequired(FALSE);

        foreach ($inventoryCheck->getItems() as $inventoryCheckItem) {
            $ingredient = $inventoryCheckItem->getIngredient();
            $inputName = 'itemPieces' . $ingredient->getId();
            if (isset($form[$inputName])) {
                $form[$inputName]->setValue($inventoryCheckItem->getPieces());
            }

            $inputName = 'itemUnits' . $ingredient->getId();
            if (isset($form[$inputName])) {
                $form[$inputName]->setValue($inventoryCheckItem->getUnits());
            }
        }

        $form->addSubmit('send', 'Odeslat');

        $this->fillValuesFromPreviousInventoryCheck($warehouse, $form);

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) use ($inventoryCheck) {
            $inventoryCheck->setNote($values->note);

            foreach ($this->fetchIngredients() as $ingredient) {
                $inventoryCheckItem = $inventoryCheck->findOrCreateItemByIngredient($ingredient);

                $inputName = 'itemPieces' . $ingredient->getId();
                if (isset($form[$inputName])) {
                    $inventoryCheckItem->setPieces($values->$inputName);
                }

                $inputName = 'itemUnits' . $ingredient->getId();
                if (isset($form[$inputName])) {
                    $inventoryCheckItem->setUnits($values->$inputName);
                }
            }

            if (!$inventoryCheck->isFilled()) {
                $inventoryCheck->setFilledBy($this->getEmployee());
            }

            $this->entityManager->persist($inventoryCheck);
            $this->entityManager->flush();

            $this->successFlashMessage('Inventura uložena');
            $this->redirect('this');
        };

        return $form;
    }



    private function fetchOrCreateInventoryCheck(Warehouse $warehouse, \DateTime $date) : InventoryCheck
    {
        $inventoryCheck = $this->entityManager->getRepository(InventoryCheck::class)->createQueryBuilder('ic')
            ->addSelect('ici')
            ->addSelect('i')
            ->leftJoin('ic.items', 'ici')
            ->leftJoin('ici.ingredient', 'i')
            ->andWhere('ic.date = :date')->setParameter('date', $date)
            ->andWhere('ic.warehouse = :warehouse')->setParameter('warehouse', $warehouse)
            ->getQuery()->getOneOrNullResult();

        if ($inventoryCheck === null) {
            $inventoryCheck = new InventoryCheck($warehouse, $date);
        }

        return $inventoryCheck;
    }



    private function getWarehouse() : Warehouse
    {
        /** @var Warehouse|null $warehouse */
        $warehouse = $this->entityManager->getRepository(Warehouse::class)->findOneBy(['code' => $this->getParameter('id')]);
        if ($warehouse === null) {
            $this->dangerFlashMessage('Tento sklad/obchod neexistuje');
            $this->redirect('default');
        }

        return $warehouse;
    }



    /**
     * @return Ingredient[]
     */
    private function fetchIngredients() : array
    {
        if ($this->ingredients === null) {
            $qb = (new IngredientsQuery())
                ->excludingHiddenFromInventoryCheck()
                ->sortByDurability();

            if ($this->getWarehouse()->isShop()) {
                $qb->excludingClosing();
                $qb->excludingOnlyInCentralWarehouse();
            }

            if ($this->getWarehouse()->getId() === Warehouse::CENTRAL_WAREHOUSE_ID) {
                $qb->excludingBidFood();
                $qb->excludingSprinkles();
            }

            $this->ingredients = $this->entityManager->fetch($qb)->toArray();
        }

        return $this->ingredients;
    }



    private function generateDescriptionLeft(Ingredient $ingredient): string
    {
        return str_replace(
            ['{unitsPerPieceTara}', '{piecesPerPackage}'],
            [$ingredient->getUnitsPerPieceTara(), $ingredient->getPiecesPerPackage()],
            $ingredient->getInventoryItemDescriptionLeft()
        );
    }



    private function generateDescriptionRight(Ingredient $ingredient): string
    {
        return str_replace(
            ['{unitsPerPieceTara}', '{piecesPerPackage}'],
            [$ingredient->getUnitsPerPieceTara(), $ingredient->getPiecesPerPackage()],
            $ingredient->getInventoryItemDescriptionRight()
        );
    }



    /**
     * @param Warehouse $warehouse
     * @param \DateTime $date
     * @return InventoryCheck|null
     */
    private function fetchPreviousInventoryCheck(Warehouse $warehouse, \DateTime $date)//: ?InventoryCheck
    {
        /** @var InventoryCheck|null $inventoryCheck */
        $inventoryCheck = $this->entityManager->getRepository(InventoryCheck::class)->createQueryBuilder('i')
            ->andWhere('i.warehouse = :warehouse')->setParameter('warehouse', $warehouse)
            ->andWhere('i.date < :date')->setParameter('date', $date)
            ->orderBy('i.date', 'DESC')
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();

        return $inventoryCheck;
    }



    protected function fillValuesFromPreviousInventoryCheck(Warehouse $warehouse, BaseForm $form)
    {
        $previousInventoryCheck = $this->fetchPreviousInventoryCheck($warehouse, $this->date);
        if ($previousInventoryCheck === null) {
            return;
        }

        foreach ($previousInventoryCheck->getItems() as $item) {
            if ($item->getIngredient()->isDurable() && isset($form['itemPieces' . $item->getIngredient()->getId()]) && $form['itemPieces' . $item->getIngredient()->getId()]->getValue() === "") {
                $form['itemPieces' . $item->getIngredient()->getId()]->setValue($item->getPieces());
            }
        }
    }



    private function createGroups()
    {
        $groups = [
            self::GROUP_PIECES => [
                'name' => 'Kusové položky',
                'description' => 'Spočítejte počet zbývajících kusů.',
                'ingredients' => [],
            ],
            self::GROUP_PACKAGES => [
                'name' => 'Balení',
                'description' => 'Pokud máte např. tři balení ořechů (dvě uzavřené, jedno otevřené), pak zadejte [2] a váhu třetího balení.',
                'ingredients' => [],
            ],
            self::GROUP_WEIGHT =>[
                'name' => 'Vážené položky',
                'description' => '',
                'ingredients' => [],
            ],
            self::GROUP_DECIMAL => [
                'name' => 'Desetinná místa',
                'description' => 'Tyto položky můžete zadat s desetinnou čárkou. Zkuste prosím odhadnout, kolik vám zbývá.',
                'ingredients' => [],
            ],
            self::GROUP_DURABLE => [
                'name' => 'Trvanlivé',
                'description' => 'Tyto položky by měly vydržet delší dobu, měňte tedy pouze pokud je chcete v příštím závozu dovézt.',
                'ingredients' => [],
            ],
        ];

        foreach ($this->ingredients as $ingredient) {
            $group = $this->getGroupForIngredient($ingredient);
            $groups[$group]['ingredients'][] = $ingredient;
        }

        return $groups;
    }



    private function getGroupForIngredient(Ingredient $ingredient)
    {
        if ($ingredient->hasUnitsOnly()) {
            return self::GROUP_WEIGHT;
        }

        if ($ingredient->hasDecimalPieces()) {
            return self::GROUP_DECIMAL;
        }

        if ($ingredient->isDurable()) {
            return self::GROUP_DURABLE;
        }

        if ($ingredient->hasWeight()) {
            return self::GROUP_PACKAGES;
        }

        return self::GROUP_PIECES;
    }

}
