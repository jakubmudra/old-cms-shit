<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Shrinks\IngredientShrink;
use App\Entities\Warehouses\Closing;
use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\InventoryItem;
use App\Entities\Warehouses\Queries\IngredientsQuery;
use App\Entities\Warehouses\Recipe;
use App\Entities\Warehouses\Services\TransfersService;
use App\Entities\Warehouses\Services\TransformIngredientToHtmlElementService;
use App\Entities\Warehouses\Services\WarehouseService;
use App\Entities\Warehouses\Shrink;
use App\Entities\Warehouses\Transfer;
use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use Money\Money;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nextras\Forms\Controls\DateTimePicker;
use Ramsey\Uuid\Uuid;



class IngredientShrinksPresenter extends BasePresenter
{

    /**
     * @inject
     * @var TransfersService
     */
    public $transfersService;

    /**
     * @inject
     * @var WarehouseService
     */
    public $warehouseService;

    /**
     * @var Warehouse[]
     */
    private $warehouses = [];

    /**
     * @var DateTime
     */
    private $startDateTime;

    /**
     * @var DateTime
     */
    private $endDateTime;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    /**
     * @param string|NULL $startDateTime
     * @param string|NULL $endDateTime
     * @param int[] $warehouseIds
     */
    public function actionDefault(string $startDateTime = NULL, string $endDateTime = NULL, array $warehouseIds)
    {
        if ($endDateTime === NULL) {
            $this->endDateTime = (new DateTime())->setTime(23, 59, 59);
        } else {
            $this->endDateTime = (new DateTime($endDateTime))->setTime(23, 59, 59);
        }

        if ($startDateTime === NULL) {
            $this->startDateTime = $this->endDateTime->modifyClone('-7 days')->setTime(0, 0, 0);
        } else {
            $this->startDateTime = (new DateTime($startDateTime))->setTime(0, 0, 0);
        }

        if (count($warehouseIds) > 0) {
            $this->warehouses = $this->entityManager->getRepository(Warehouse::class)->createQueryBuilder('w', 'w.id')
                ->andWhere('w.id IN (:warehouseIds)')->setParameter('warehouseIds', $warehouseIds)
                ->getQuery()->getResult();

        } else {
            $this->warehouses = $this->warehouseService->getMySubordinateWarehouses();
        }

        $shrinks = $this->fetchShrinks();
        $this->template->shrinks = $shrinks;
        $this->template->totalShrinkPrice = $this->calculateTotalShrinksPrice($shrinks);

        $inventoryItemShrinks = $this->getInventoryItemShrinks();
        $this->template->inventoryItemShrinks = $inventoryItemShrinks;
        $this->template->inventoryItemShrinksSummary = $this->calculateSummaryForInventoryShrinks($inventoryItemShrinks);
    }



    public function handleMarkIngredientShrinkChecked(string $shrinkUuid)
    {
        /** @var IngredientShrink|NULL $shrink */
        $shrink = $this->entityManager->find(IngredientShrink::class, $shrinkUuid);
        if ($shrink === NULL) {
            $this->dangerFlashMessage('Tento odpis neexistuje');
            $this->redirect('this');
        }

        $shrink->markChecked($this->getEmployee());
        $this->successFlashMessage('Odpis potvrzen');
        $this->entityManager->flush();
        $this->redirect('this');
    }



    public function handleRemoveIngredientShrink(string $shrinkUuid)
    {
        /** @var IngredientShrink|NULL $shrink */
        $shrink = $this->entityManager->find(IngredientShrink::class, $shrinkUuid);
        if ($shrink === NULL) {
            $this->dangerFlashMessage('Tento odpis neexistuje');
            $this->redirect('this');
        }

        $deletingInSubordinatedShop = in_array($shrink->getWarehouse(), $this->warehouseService->getMySubordinateWarehouses(), TRUE);
        if (!$deletingInSubordinatedShop && !$this->getEmployee()->isManagement()) {
            $this->dangerFlashMessage('Tento odpis nemáte právo mazat');
            $this->redirect('this');
        }

        // @FIXME: This is some dirty shit... but we will get rid of this kind of Transfers probably...
        /** @var Transfer|NULL $transfer */
        $transfer = $this->entityManager->getRepository(Transfer::class)->createQueryBuilder('t')
            ->andWhere('t.oldWarehouse = :oldWarehouse')->setParameter('oldWarehouse', $shrink->getWarehouse())
            ->andWhere('t.newWarehouse = :shrinkWarehouse')->setParameter('shrinkWarehouse', Warehouse::SHRINK_WAREHOUSE_ID)
            ->andWhere('t.date = :date')->setParameter('date', $shrink->getCreatedAt()->setTime(0, 0, 0))
            ->andWhere('t.ingredient = :ingredient')->setParameter('ingredient', $shrink->getIngredient())
            ->andWhere('t.pieces = :pieces')->setParameter('pieces', $shrink->getAmount())
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();

        $this->entityManager->remove($transfer);
        $this->entityManager->remove($shrink);
        $this->entityManager->flush();

        $this->successFlashMessage('Odpis smazán');
        $this->redirect('this');
    }



    protected function createComponentDateFilterForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addCheckboxList('warehouseIds', 'Sklad', $this->warehouseService->getMySubordinateWarehouses())
            ->setValue(array_keys($this->warehouses))
            ->setRequired('Zvolte nějaký sklad');

        $startDateTime = (new DateTimePicker())
            ->setValue($this->startDateTime)
            ->setRequired('Datum od je povinné');
        $form->addComponent($startDateTime, 'startDateTime');

        $endDateTime = (new DateTimePicker())
            ->setValue($this->endDateTime)
            ->setRequired('Datum do je povinné');
        $form->addComponent($endDateTime, 'endDateTime');

        $form->addSubmit('send', 'Vyfiltrovat');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            /** @var DateTime $startDateTime */
            $startDateTime = $values->startDateTime;
            $endDateTime = $values->endDateTime ?: new DateTime();

            $this->redirect('this', [
                'startDateTime' => $startDateTime->format('Y-m-d H:i'),
                'endDateTime' => $endDateTime->format('Y-m-d H:i'),
                'warehouseIds' => $values->warehouseIds,
            ]);
        };

        return $form;
    }



    protected function createComponentIngredientShrinkForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addSelect('warehouseId', 'Sklad', $this->warehouseService->getMySubordinateWarehouses())
            ->setPrompt('Vyberte sklad')
            ->setRequired('Vyberte sklad');

        $form->addSelect('ingredientId', 'Surovina', $this->fetchIndexedIngredients())
            ->setPrompt('Vyberte surovinu')
            ->setRequired('Vyberte prosím surovinu');

        $form->addText('pieces', 'Množství')
            ->setRequired('Zadejte prosím množství')
            ->setType('number')
            ->setAttribute('step', 'any')
            ->addRule($form::FLOAT)
            ->addRule($form::RANGE, 'Množství musí být v rozsahu %d a %d', [0, 1e6]);

        $form->addText('reason', 'Důvod')
            ->setRequired('Zadejte prosím důvod');

        $dateTime = (new DateTimePicker())
            ->setValue(new \DateTime());
        $form->addComponent($dateTime, 'dateTime');

        $form->addSubmit('send', 'Odepsat');

        $form->onValidate[] = function (BaseForm $form, ArrayHash $values) {
            /** @var Warehouse|NULL $warehouse */
            $warehouse = $this->entityManager->find(Warehouse::class, $values->warehouseId);
            if ($warehouse === NULL) {
                $form->addError('Sklad, ze kterého se snažíte odepsat surovinu, neexistuje');
                return;
            }

            /** @var Ingredient|NULL $ingredient */
            $ingredient = $this->entityManager->find(Ingredient::class, $values->ingredientId);
            if ($ingredient === NULL) {
                $form->addError('Surovina neexistuje');
                return;
            }
        };

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            /** @var Warehouse|NULL $warehouse */
            $warehouse = $this->entityManager->find(Warehouse::class, $values->warehouseId);

            /** @var Ingredient|NULL $ingredient */
            $ingredient = $this->entityManager->find(Ingredient::class, $values->ingredientId);

            /** @var IngredientShrink $shrink */
            $shrink = new IngredientShrink(Uuid::uuid4(), $warehouse, $ingredient);
            $shrink->setCreatedAtCashDesk($values->dateTime);
            $shrink->setAmount($values->pieces);
            $shrink->setReason($values->reason);
            $shrink->markChecked($this->getEmployee());

            $this->entityManager->persist($shrink);
            $this->entityManager->persist($this->transfersService->createTransfer($shrink, $this->getEmployee()));
            $this->entityManager->flush($shrink);

            $this->successFlashMessage('Odpis vytvořen');
            $this->redirect('this');
        };

        return $form;
    }



    /**
     * @return array[id:int => ingredient:\Nette\Utils\Html]
     */
    private function fetchIndexedIngredients(): array
    {
        $query = new IngredientsQuery(FALSE);
        $query->excludingByIds([
            Ingredient::WATER_ID,
            Ingredient::PRICE_CHANGE_ID
        ]);

        if (!$this->getEmployee()->isManagement()) {
            $query->excludingByIds([Ingredient::COFFEE_ID, Ingredient::MIXTURE_ID]);
            $query->excludingOnlyInCentralWarehouse();
        }

        $query->excludingHiddenFromInventoryCheck();
        $query->sortByName();

        /** @var Ingredient[] $ingredients */
        $ingredients = $this->entityManager->fetch($query)->toArray();

        return TransformIngredientToHtmlElementService::transformArray($ingredients);
    }



    /**
     * @return IngredientShrink[]
     */
    private function fetchShrinks(): array
    {
        return $this->entityManager->getRepository(IngredientShrink::class)->createQueryBuilder('s')
            ->andWhere('s.warehouse IN (:warehouses)')->setParameter('warehouses', $this->warehouses)
            ->andWhere('s.createdAtServer >= :startDateTime')->setParameter('startDateTime', $this->startDateTime)
            ->andWhere('s.createdAtServer <= :endDateTime')->setParameter('endDateTime', $this->endDateTime)
            ->orderBy('s.createdAtServer', 'DESC')
            ->getQuery()->getResult();
    }



    /**
     * @param IngredientShrink[] $ingredientShrinks
     * @return Money
     */
    private function calculateTotalShrinksPrice(array $ingredientShrinks): Money
    {
        /** @var Money $totalPrice */
        $totalPrice = Money::CZK(0);

        foreach ($ingredientShrinks as $ingredientShrink) {
            $totalPrice = $totalPrice->add($ingredientShrink->getPrice());
        }

        return $totalPrice;
    }



    private function getInventoryItemShrinks(): array
    {
        /** @var Shrink[] $shrinks */
        $shrinks = $this->entityManager->getRepository(Shrink::class)->createQueryBuilder('s')
            ->addSelect('i')
            ->innerJoin('s.warehouse', 'w')
            ->innerJoin('s.inventoryItem', 'i')
            ->andWhere('s.warehouse IN (:warehouses)')->setParameter('warehouses', $this->warehouses)
            ->andWhere('s.date >= :startDate')->setParameter('startDate', (clone $this->startDateTime)->setTime(0, 0, 0))
            ->andWhere('s.date <= :endDate')->setParameter('endDate', (clone $this->endDateTime)->setTime(0, 0, 0))
            ->andWhere('s.amount > 0')
            ->orderBy('s.date')
            ->addOrderBy('w.code')
            ->getQuery()->getResult();

        $items = [];

        foreach ($shrinks as $shrink) {
            $pricePerItem = $this->calculatePriceForInventoryItem($shrink->getInventoryItem());

            $items[] = [
                'date' => $shrink->getDate(),
                'warehouse' => $shrink->getWarehouse(),
                'name' => $shrink->getInventoryItem()->getName(),
                'amount' => $shrink->getAmount(),
                'price' => $pricePerItem->multiply($shrink->getAmount()),
            ];
        }

        return $items;
    }



    private function calculatePriceForInventoryItem(InventoryItem $inventoryItem): Money
    {
        /** @var Money $itemPrice */
        $itemPrice = Money::CZK(0);

        /** @var Recipe[] $recipes */
        $recipes = $this->entityManager->getRepository(Recipe::class)->createQueryBuilder('r')
            ->addSelect('i')
            ->innerJoin('r.ingredient', 'i')
            ->andWhere('r.inventoryItem = :inventoryItem')->setParameter('inventoryItem', $inventoryItem)
            ->getQuery()->getResult();

        foreach ($recipes as $recipe) {
            $ingredient = $recipe->getIngredient();
            $pieces = $recipe->getAmount() / $ingredient->getUnitsPerPieceNetto();
            $itemPrice = $itemPrice->add($ingredient->getPricePerPiece()->multiply($pieces));
        }

        return $itemPrice;
    }



    /**
     * @param array $shrinks, result from $this->getInventoryItemShrinks()
     * @return Money
     */
    private function calculateSummaryForInventoryShrinks(array $shrinks): Money
    {
        /** @var Money $totalPrice */
        $totalPrice = Money::CZK(0);

        foreach ($shrinks as $shrink) {
            $totalPrice = $totalPrice->add($shrink['price']);
        }

        return $totalPrice;
    }

}
