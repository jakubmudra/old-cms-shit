<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\Queries\IngredientsQuery;
use App\Entities\Warehouses\Stock;
use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\InventoryCheck;
use App\Services\DateService;
use Money\Money;
use Nette\Utils\DateTime;



class StockWorthPresenter extends BasePresenter
{

    /**
     * @var DateTime
     */
    private $date;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isManagement()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function beforeRender()
    {
        parent::beforeRender();
    }



    public function actionDefault(string $date = null)
    {
        $this->date = DateService::parseDateOrToday($date);
        $this->prefetchIngredients();
        $this->template->stocks = $this->fetchStocks();
        $this->template->summary = $this->calculateSummary($this->fetchStocks());

        $this->template->date = $this->date;
        $this->template->dayBefore = $this->date->modifyClone('-1 day');
        $this->template->dayAfter = $this->date->modifyClone('+1 day');
        $this->template->today = (new DateTime())->setTime(0, 0, 0);
    }



    /**
     * @return Ingredient[]
     */
    private function prefetchIngredients() : array
    {
        return $this->entityManager->fetch(new IngredientsQuery())->toArray();
    }



    /**
     * @return Stock[]
     */
    private function fetchStocks() : array
    {
        return $this->entityManager->getRepository(Stock::class)->createQueryBuilder('es')
            ->addSelect('esi')
            ->addSelect('w')
            ->leftJoin('es.items', 'esi')
            ->leftJoin('es.warehouse', 'w')
            ->leftJoin('esi.ingredient', 'i')
            ->andWhere('es.date = :date')->setParameter('date', $this->date)
            ->andWhere('i.deleted = false')
            ->orderBy('es.warehouse', 'ASC')
            ->addOrderBy('esi.ingredient', 'ASC')
            ->getQuery()->getResult();
    }



    /**
     * @param Stock[] $stocks
     * @return array
     */
    private function calculateSummary(array $stocks) : array
    {
        $summary = [0 => Money::CZK(0)];

        foreach ($stocks as $stock) {
            foreach ($stock->getItems() as $stockItem) {
                $ingredientId = $stockItem->getIngredient()->getId();
                if (!array_key_exists($ingredientId, $summary)) {
                    $summary[$ingredientId] = Money::CZK(0);
                }

                if ($stockItem->calculateFinalPrice()->getAmount() < 0) {
                    continue;
                }

                $summary[$ingredientId] = $summary[$ingredientId]->add($stockItem->calculateFinalPrice());
            }

            $summary[0] = $summary[0]->add($stock->calculateFinalPrice());
        }

        return $summary;
    }

}
