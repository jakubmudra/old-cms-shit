<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Shops\Services\ShopService;
use App\Entities\Shops\Shop;
use App\Entities\Warehouses\Closing;
use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\Services\ClosingService;
use App\Entities\Warehouses\Services\SafeBagService;
use App\Entities\Warehouses\Services\WarehouseService;
use App\Entities\Warehouses\Transfer;
use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use App\Services\DateService;
use App\WarehouseModule\Forms\ClosingFormInterface;
use Doctrine\ORM\Query\Expr\Join;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;



class ClosingPresenter extends BasePresenter
{

    /**
     * @inject
     * @var ShopService
     */
    public $shopService;

    /**
     * @inject
     * @var ClosingFormInterface
     */
    public $closingFormInterface;

    /**
     * @inject
     * @var ClosingService
     */
    public $closingService;

    /**
     * @inject
     * @var WarehouseService
     */
    public $warehouseService;

    /**
     * @inject
     * @var SafeBagService
     */
    public $safeBagService;

    /**
     * @var DateTime
     */
    private $date;

    /**
     * Caching purposes
     * @var Closing
     */
    private $todayClosing;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isSeller() && !$this->getEmployee()->isDriver()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->date = $this->date;
        $this->template->dayBefore = $this->date->modifyClone('-1 day');
        $this->template->dayAfter = $this->date->modifyClone('+1 day');
        $this->template->today = (new DateTime())->setTime(0, 0, 0);
    }



    public function actionDefault(string $date = NULL)
    {
        $this->date = DateService::parseDateOrToday($date);

        if ($this->date->format('Y-m-d') !== (new \DateTime())->format('Y-m-d') && !$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Uzávěrka lze vždy vyplňovat pouze v současném dni, přesměrovávám...');
            $this->redirect('this', ['date' => (new \DateTime())->format('Y-m-d')]);
        }

        $shops = $this->shopService->getAvailableShops();
        $this->template->shops = $this->entityManager->getRepository(Shop::class)->createQueryBuilder('s', 's.id')
            ->addSelect('w, c')
            ->innerJoin('s.warehouse', 'w')
            ->leftJoin('w.closings', 'c', Join::WITH, 'c.date = :today')->setParameter('today', $this->date)
            ->andWhere('s.id IN (:shops)')->setParameter('shops', $shops)
            ->getQuery()->getResult();
    }



    public function actionDetail(string $id, string $date = NULL)
    {
        $this->date = DateService::parseDateOrToday($date);

        if ($this->date->format('Y-m-d') !== (new \DateTime())->format('Y-m-d') && !$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Uzávěrka lze vždy vyplňovat pouze v současném dni, přesměrovávám...');
            $this->redirect('this', ['id' => $id, 'date' => (new \DateTime())->format('Y-m-d')]);
        }

        $this->template->warehouse = $this->getWarehouse();
        $this->template->closing = $this->getTodayClosing();
        $this->template->transfers = $this->fetchTransfers();
        $this->template->moneySafeBags = $this->safeBagService->findMoneySafeBagsInShop($this->getShop());
        $this->template->paperSafeBags = $this->safeBagService->findPaperSafeBagsInShop($this->getShop());

        $this->template->pastryCoefficient = Closing::MIXTURE_COEFICIENT;

        $this->template->coffeeNettoWeight = Ingredient::COFFEE_NETTO_WEIGHT;
        $this->template->coffeeTaraWeight = Ingredient::COFFEE_TARA_WEIGHT;

        $this->template->milkNettoWeight = Ingredient::MILK_NETTO_WEIGHT;
        $this->template->milkTaraWeight = Ingredient::MILK_TARA_WEIGHT;

        $this->template->oilBottleNettoWeight = Ingredient::OIL_BOTTLE_NETTO_WEIGHT;
        $this->template->oilBottleTaraWeight = Ingredient::OIL_BOTTLE_TARA_WEIGHT;
        $this->template->oilBowlTaraWeight = Ingredient::OIL_BOWL_TARA_WEIGHT;

        $this->template->sugarBottleNettoWeight = Ingredient::SUGAR_BOTTLE_NETTO_WEIGHT;
        $this->template->sugarBottleTaraWeight = Ingredient::SUGAR_BOTTLE_TARA_WEIGHT;
        $this->template->sugarBowlTaraWeight = Ingredient::SUGAR_BOWL_TARA_WEIGHT;

        $this->template->nutellaNettoWeight = Ingredient::NUTELLA_NETTO_WEIGHT;
        $this->template->nutellaTaraWeight = Ingredient::NUTELLA_TARA_WEIGHT;
        $this->template->nutellaBowlTaraWeight = Closing::NUTELLA_BOWL_TARA_WEIGHT;
    }



    public function createComponentClosingForm()
    {
        $form = $this->closingFormInterface->create($this->getWarehouse(), $this->getTodayClosing(), $this->getYesterdayClosing(), $this->getTomorrowClosing());
        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $this->successFlashMessage('Uzávěrka uložena');
            $this->redirect('this');
        };

        return $form;
    }



    private function getWarehouse() : Warehouse
    {
        /** @var Warehouse|null $warehouse */
        $warehouse = $this->entityManager->getRepository(Warehouse::class)->findOneBy(['code' => $this->getParameter('id')]);
        if ($warehouse === null) {
            $this->dangerFlashMessage('Tento sklad neexistuje');
            $this->redirect('default');
        }

        return $warehouse;
    }



    private function getShop() : Shop
    {
        /** @var Shop|null $shop */
        $shop = $this->entityManager->getRepository(Shop::class)->findOneBy(['code' => $this->getParameter('id')]);
        if ($shop === null) {
            $this->dangerFlashMessage('Tento obchod neexistuje');
            $this->redirect('default');
        }

        return $shop;
    }



    private function getYesterdayClosing() : Closing
    {
        $yesterday = $this->date->modifyClone('-1 day');
        return $this->closingService->findOrCreateClosing($this->getWarehouse(), $yesterday);
    }



    private function getTodayClosing() : Closing
    {
        if ($this->todayClosing === null) {
            $today = $this->date;
            $this->todayClosing = $this->closingService->findOrCreateClosing($this->getWarehouse(), $today);
        }

        return $this->todayClosing;
    }



    private function getTomorrowClosing() : Closing
    {
        $tomorrow = $this->date->modifyClone('+1 day');
        return $this->closingService->findOrCreateClosing($this->getWarehouse(), $tomorrow);
    }



    private function fetchTransfers()
    {
        $transfers = [
            'mixture' => [
                'id' => Ingredient::MIXTURE_ID,
            ],
            'pastry' => [
                'id' => Ingredient::PASTRY_ID,
            ],
            'coffee' => [
                'id' => Ingredient::COFFEE_ID,
            ],
            'milk' => [
                'id' => Ingredient::MILK_ID,
            ],
            'oil' => [
                'id' => Ingredient::OIL_ID,
            ],
            'sugar' => [
                'id' => Ingredient::SUGAR_ID,
            ],
        ];

        foreach ($transfers as &$values) {
            $amountFrom = (float) $this->entityManager->getRepository(Transfer::class)->createQueryBuilder('t')
                ->select('SUM(t.pieces)')
                ->andWhere('t.oldWarehouse = :warehouse')->setParameter('warehouse', $this->getWarehouse())
                ->andWhere('t.date = :date')->setParameter('date', $this->date)
                ->andWhere('t.ingredient = :ingredientId')->setParameter('ingredientId', $values['id'])
                ->andWhere('t.supplyPackageItem IS NULL')
                ->getQuery()->getSingleScalarResult();

            $amountTo = (float) $this->entityManager->getRepository(Transfer::class)->createQueryBuilder('t')
                ->select('SUM(t.pieces)')
                ->andWhere('t.newWarehouse = :warehouse')->setParameter('warehouse', $this->getWarehouse())
                ->andWhere('t.date = :date')->setParameter('date', $this->date)
                ->andWhere('t.ingredient = :ingredientId')->setParameter('ingredientId', $values['id'])
                ->andWhere('t.supplyPackageItem IS NULL')
                ->getQuery()->getSingleScalarResult();

            $values['from'] = $amountFrom;
            $values['to'] = $amountTo;
        }

        return $transfers;
    }

}
