<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Shops\Services\ShopService;
use App\Entities\Shops\Shop;
use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\Queries\IngredientsQuery;
use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use Nette\Utils\ArrayHash;
use Tracy\ILogger;



class IngredientMinimalAmountPresenter extends BasePresenter
{

    /**
     * @inject
     * @var ShopService
     */
    public $shopService;

    /**
     * Only for caching
     * @var Ingredient[]
     */
    private $ingredients;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function renderDefault()
    {
        $this->template->shopGroups = $this->fetchShopGroups();
        $this->template->ingredients = $this->fetchIngredients();
        $this->template->amounts = $this->createAmounts();
    }



    protected function createComponentForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addSubmit('send', 'Uložit');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $httpData = $form->getHttpData();
            $amounts = $httpData['amount'];

            foreach ($amounts as $warehouseId => $values) {
                /** @var Shop|NULL $shop */
                $shop = $this->entityManager->find(Shop::class, $warehouseId);

                if ($shop === NULL) {
                    $this->dangerFlashMessage(sprintf('Obchod ID %d neexistuje', $warehouseId));
                    $this->redirect('this');
                }

                if (!$this->canUserEditShop($shop)) {
                    $this->dangerFlashMessage(sprintf(
                        'Minimální množství u skladu %s může měnit pouze %s, %s nebo management',
                        $shop->getName(),
                        $shop->getRegionalManager()->getName(),
                        $shop->getShopManager()->getName())
                    );
                    $this->redirect('this');
                }
            }

            foreach ($this->shopService->getMySubordinateShops() as $shop) {
                foreach ($this->fetchIngredients() as $ingredient) {
                    if (!isset($amounts[$shop->getId()][$ingredient->getId()])) {
                        continue; // current form didn't have this value
                    }

                    $amount = (float) str_replace(',', '.', $amounts[$shop->getId()][$ingredient->getId()]);
                    $ingredientMinimalAmount = $shop->getWarehouse()->findOrCreateIngredientMinimalAmountByIngredient($ingredient);
                    $ingredientMinimalAmount->setAmount($amount);
                    $this->entityManager->persist($ingredientMinimalAmount);
                }
            }

            $this->entityManager->flush();
            $this->successFlashMessage('Minimální hodnoty změněny');
        };

        return $form;
    }



    /**
     * @return Ingredient[]
     */
    private function fetchIngredients() : array
    {
        if ($this->ingredients === null) {
            $qb = (new IngredientsQuery())
                ->excludingOnlyInCentralWarehouse()
                ->excludingHiddenFromInventoryCheck();

            $this->ingredients = $this->entityManager->fetch($qb)->toArray();
        }

        return $this->ingredients;
    }



    /**
     * @return array 2D field of ints, indexed by Warehouse.Id & Ingredient.Id
     */
    private function createAmounts() : array
    {
        $amounts = [];

        foreach ($this->shopService->getMySubordinateShops() as $shop) {
            $amounts[$shop->getId()] = [];
            foreach ($this->fetchIngredients() as $ingredient) {
                $ingredientMinimalAmount = $shop->getWarehouse()->findOrCreateIngredientMinimalAmountByIngredient($ingredient);
                $amounts[$shop->getId()][$ingredient->getId()] = $ingredientMinimalAmount->getAmount();
            }
        }

        return $amounts;
    }



    /**
     * Wedos is stupid and they don't allow changing "max_input_vars" in php.ini
     * => we need to have max 1k of inputs per form => we create groups of 10 warehouses...
     */
    private function fetchShopGroups()
    {
        $regions = [];

        foreach ($this->shopService->getMySubordinateShops() as $shop) {
            $regionalManagerId = $shop->getRegionalManager()->getId();
            if (!isset($regions[$regionalManagerId])) {
                $regions[$regionalManagerId] = [];
            }

            $regions[$regionalManagerId][] = $shop;
        }

        return $regions;
    }



    private function canUserEditShop(Shop $shop): bool
    {
        return $this->getEmployee()->isManagement()
            || $shop->getRegionalManager()->getId() === $this->getEmployee()->getId()
            || $shop->getShopManager()->getId() === $this->getEmployee()->getId();
    }

}
