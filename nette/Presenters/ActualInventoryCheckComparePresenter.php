<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\ExpectedConsumption;
use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\InventoryCheck;
use App\Entities\Warehouses\InventoryCheckItem;
use App\Entities\Warehouses\ReplenishmentItem;
use App\Entities\Warehouses\Services\WarehouseService;
use App\Entities\Warehouses\Transfer;
use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nextras\Forms\Controls\DatePicker;



class ActualInventoryCheckComparePresenter extends BasePresenter
{

    /**
     * @inject
     * @var WarehouseService
     */
    public $warehouseService;

    /**
     * @var DateTime
     */
    private $startDate;

    /**
     * @var DateTime
     */
    private $endDate;

    /**
     * Only for caching
     * @var Warehouse[]
     */
    private $warehouses;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isRegionalManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    protected function createComponentDateFilterForm()
    {
        $form = new BaseForm();
        $form->setMethod('GET');

        $startDate = new DatePicker();
        $startDate->setValue($this->startDate);
        $form->addComponent($startDate, 'startDate');

        $endDate = new DatePicker();
        $endDate->setValue($this->endDate);
        $form->addComponent($endDate, 'endDate');

        $form->addSubmit('send', 'Vybrat');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $this->redirect('this', [
                'startDate' => $this->startDate->format('Y-m-d'),
                'endDate' => $this->endDate->format('Y-m-d')]
            );
        };

        return $form;
    }



    protected function beforeRender()
    {
        parent::beforeRender();
        $this->template->startDate = $this->startDate;
        $this->template->endDate = $this->endDate;
    }



    public function actionDefault(string $type = 'brno', string $endDate = NULL, string $startDate = NULL)
    {
        if ($endDate === NULL) {
            $this->endDate = (new DateTime())->setTime(0, 0, 0);
        } else {
            $this->endDate = (new DateTime($endDate))->setTime(0, 0, 0);
        }

        if ($startDate === NULL) {
            $this->startDate = $this->endDate->modifyClone('-10 days');
        } else {
            $this->startDate = (new DateTime($startDate))->setTime(0, 0, 0);
        }

        $this->template->ingredients = $this->fetchIngredients();
        $this->template->summary = $this->createSummary($this->startDate, $this->endDate);
        $this->template->warehouses = $this->fetchWarehouses();
    }



    private function createSummary(\DateTime $start, \DateTime $end)
    {
        $summary = [];

        foreach ($this->fetchIngredients() as $ingredient) {
            $summary[$ingredient->getId()] = [];

            foreach ($this->fetchWarehouses() as $warehouse) {
                $summary[$ingredient->getId()][$warehouse->getId()] = [
                    'actual' => 0.0,
                    'expected' => 0.0,
                    'start' => $start,
                    'end' => $end,
                ];

                /** @var InventoryCheckItem|null $startingInventoryCheckItem */
                $startingInventoryCheckItem = $this->entityManager->getRepository(InventoryCheckItem::class)->createQueryBuilder('ici')
                    ->innerJoin('ici.inventoryCheck', 'ic')
                    ->andWhere('ici.ingredient = :ingredient')->setParameter('ingredient', $ingredient)
                    ->andWhere('ic.date >= :startDate')->setParameter('startDate', $start)
                    ->andWhere('ic.date <= :endDate')->setParameter('endDate', $end)
                    ->andWhere('ic.warehouse = :warehouse')->setParameter('warehouse', $warehouse)
                    ->orderBy('ic.date', 'ASC')
                    ->setMaxResults(1)
                    ->getQuery()->getOneOrNullResult();

                /** @var InventoryCheckItem|null $endingInventoryCheckItem */
                $endingInventoryCheckItem = $this->entityManager->getRepository(InventoryCheckItem::class)->createQueryBuilder('ici')
                    ->innerJoin('ici.inventoryCheck', 'ic')
                    ->andWhere('ici.ingredient = :ingredient')->setParameter('ingredient', $ingredient)
                    ->andWhere('ic.date >= :startDate')->setParameter('startDate', $start)
                    ->andWhere('ic.date <= :endDate')->setParameter('endDate', $end)
                    ->andWhere('ic.warehouse = :warehouse')->setParameter('warehouse', $warehouse)
                    ->orderBy('ic.date', 'DESC')
                    ->setMaxResults(1)
                    ->getQuery()->getOneOrNullResult();

                if ($startingInventoryCheckItem === null || $endingInventoryCheckItem === null || $startingInventoryCheckItem === $endingInventoryCheckItem) {
                    continue;
                }

                $replenishmentItems = $this->entityManager->getRepository(ReplenishmentItem::class)->createQueryBuilder('ri')
                    ->innerJoin('ri.replenishment', 'r')
                    ->andWhere('r.date >= :startDate')->setParameter('startDate', $startingInventoryCheckItem->getInventoryCheck()->getDate())
                    ->andWhere('r.date < :endDate')->setParameter('endDate', $endingInventoryCheckItem->getInventoryCheck()->getDate())
                    ->andWhere('r.warehouse = :warehouse')->setParameter('warehouse', $warehouse)
                    ->andWhere('ri.ingredient = :ingredient')->setParameter('ingredient', $ingredient)
                    ->getQuery()->getResult();

                $transfers = $this->entityManager->getRepository(Transfer::class)->createQueryBuilder('t')
                    ->andWhere('t.date >= :startDate')->setParameter('startDate', $startingInventoryCheckItem->getInventoryCheck()->getDate())
                    ->andWhere('t.date < :endDate')->setParameter('endDate', $endingInventoryCheckItem->getInventoryCheck()->getDate())
                    ->andWhere('t.newWarehouse = :warehouse OR t.oldWarehouse = :warehouse')->setParameter('warehouse', $warehouse)
                    ->andWhere('t.ingredient = :ingredient')->setParameter('ingredient', $ingredient)
                    ->getQuery()->getResult();

                $actualUsage = $this->calculateActualUsage($startingInventoryCheckItem, $endingInventoryCheckItem, $replenishmentItems, $transfers);
                $expectedUsage = $this->calculateExpectedUsage(
                    $endingInventoryCheckItem->getInventoryCheck()->getWarehouse(),
                    $ingredient,
                    $startingInventoryCheckItem->getInventoryCheck()->getDate(),
                    $endingInventoryCheckItem->getInventoryCheck()->getDate());

                $summary[$ingredient->getId()][$warehouse->getId()] = [
                    'actual' => $actualUsage,
                    'expected' => $expectedUsage,
                    'start' => $startingInventoryCheckItem->getInventoryCheck()->getDate(),
                    'end' => $endingInventoryCheckItem->getInventoryCheck()->getDate(),
                ];
            }
        }

        return $summary;
    }



    /**
     * @param InventoryCheckItem $start
     * @param InventoryCheckItem $end
     * @param ReplenishmentItem[] $replenishmentItems
     * @param Transfer[] $transfers
     * @return float
     */
    private function calculateActualUsage(InventoryCheckItem $start, InventoryCheckItem $end, array $replenishmentItems, array $transfers): float
    {
        $pieces = $start->calculateTotalPieces();
        foreach ($replenishmentItems as $replenishmentItem) {
            $pieces += $replenishmentItem->getPieces();
        }

        foreach ($transfers as $transfer) {
            if ($transfer->getOldWarehouse() === $start->getInventoryCheck()->getWarehouse()) {
                $pieces -= $transfer->getPieces();
            } else {
                $pieces += $transfer->getPieces();
            }
        }

        $usage = $pieces - $end->calculateTotalPieces();

        return $usage;
    }



    private function calculateExpectedUsage(Warehouse $warehouse, Ingredient $ingredient, \DateTime $start, \DateTime $end): float
    {
        return $this->entityManager->getRepository(ExpectedConsumption::class)->createQueryBuilder('ec')
            ->select('SUM(ec.pieces) AS usage')
            ->andWhere('ec.warehouse = :warehouse')->setParameter('warehouse', $warehouse)
            ->andWhere('ec.ingredient = :ingredient')->setParameter('ingredient', $ingredient)
            ->andWhere('ec.date >= :startDate')->setParameter('startDate', $start)
            ->andWhere('ec.date < :endDate')->setParameter('endDate', $end)
            ->getQuery()->getSingleScalarResult();
    }



    /**
     * @return Ingredient[]
     */
    private function fetchIngredients() : array
    {
        return $this->entityManager->getRepository(Ingredient::class)->createQueryBuilder('i', 'i.id')
            ->andWhere('i.id IN (:ids)')->setParameter('ids', [34, 35, 36, 37, 38, 39, 40, 41])
            ->getQuery()->getResult();
    }



    /**
     * @return Warehouse[]
     */
    private function fetchWarehouses(): array
    {
        if ($this->warehouses === null) {
            $this->warehouses = $this->warehouseService->getAvailableWarehouses();
        }

        return $this->warehouses;
    }

}
