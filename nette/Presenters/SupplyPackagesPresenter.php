<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Shops\Shop;
use App\Entities\Warehouses\InventoryCheck;
use App\Entities\Warehouses\Services\SupplyPackageService;
use App\Entities\Warehouses\SupplyPackage;
use App\Entities\Warehouses\Warehouse;
use App\WarehouseModule\Forms\SupplyPackageForm;
use App\WarehouseModule\Forms\SupplyPackageFormInterface;
use Nette\Forms\Controls\SubmitButton;
use Tracy\ILogger;



class SupplyPackagesPresenter extends BasePresenter
{

    /**
     * @inject
     * @var SupplyPackageService
     */
    public $supplyPackageService;

    /**
     * @inject
     * @var SupplyPackageFormInterface
     */
    public $supplyPackageFormInterface;



    public function renderDefault()
    {
        $this->template->waitingForEditingPackages = $this->supplyPackageService->fetchWaitingForEditing();
        $this->template->waitingForPreparationPackages = $this->supplyPackageService->fetchWaitingForPreparation();
        $this->template->waitingForExpeditionPackages = $this->supplyPackageService->fetchWaitingForExpedition();
        $this->template->enRoutePackages = $this->supplyPackageService->fetchEnRoutePackages();
        $this->template->deliveredPackages = $this->supplyPackageService->fetchDeliveredPackages();
    }



    public function actionDetail(int $id)
    {
        $this->template->supplyPackage = $this->getSupplyPackage();
        $this->template->lastInventoryCheck = $this->fetchLastInventoryCheck();
    }



    public function renderPrintPreparationPackages()
    {
        $this->template->packages = $this->supplyPackageService->fetchWaitingForPreparation();
        $this->template->inventoryChecks = $this->fetchLastInventoryChecks($this->template->packages);
    }



    public function handleDelete(int $id)
    {
        /** @var SupplyPackage|null $supplyPackage */
        $supplyPackage = $this->entityManager->find(SupplyPackage::class, $id);
        if ($supplyPackage === null) {
            $this->warningFlashMessage('Tento balík neexistuje');
            $this->redirect('this');
        }

        if (!$supplyPackage->getWarehouse()->isShop() && $this->getEmployee()->isManagement()) {
            $this->warningFlashMessage('Balíky do jiných skladů může mazat pouze management');
            $this->redirect('this');
        }

        /** @var Shop $shop */
        $shop = $supplyPackage->getWarehouse()->getShop();
        if ($shop->getRegionalManager() !== $this->getEmployee() && $supplyPackage->getCreatedBy() !== $this->getEmployee() && !$this->getEmployee()->isManagement()) {
            $this->warningFlashMessage('Tento balík nemůžete mazat');
            $this->redirect('this');
        }

        $this->entityManager->remove($supplyPackage);
        foreach ($supplyPackage->getItems() as $supplyPackageItem) {
            $this->entityManager->remove($supplyPackageItem);
            $this->entityManager->remove($supplyPackageItem->getTransfers()->toArray());
        }

        $this->entityManager->flush();
        $this->successFlashMessage('Balík smazán');
        $this->redirect('this');
    }



    protected function createComponentForm()
    {
        $form = $this->supplyPackageFormInterface->create($this->getSupplyPackage());
        $form->onSuccess[] = function (SupplyPackageForm $form) {
            /** @var SubmitButton|bool $submitButton */
            $submitButton = $form->isSubmitted();

            if ($submitButton instanceof SubmitButton && $submitButton->getName() === $form::SUBMIT_BUTTON_MARK_EDITED) {
                $this->successFlashMessage('Balík byl označen jako schválený prodejnou.');
                $this->redirect('default');
            }

            if ($submitButton instanceof SubmitButton && $submitButton->getName() === $form::SUBMIT_BUTTON_MARK_PREPARED) {
                $this->successFlashMessage('Balík byl označen jako vychystaný.');
                $this->redirect('default');
            }

            if ($submitButton instanceof SubmitButton && $submitButton->getName() === $form::SUBMIT_BUTTON_MARK_SENT) {
                $this->successFlashMessage('Balík byl označen jako odeslaný.');
                $this->redirect('default');
            }

            if ($submitButton instanceof SubmitButton && $submitButton->getName() === $form::SUBMIT_BUTTON_MARK_RECEIVED) {
                $this->successFlashMessage('Balík byl označen jako převzatý.');
                $this->redirect('default');
            }
        };

        return $form;
    }



    private function getSupplyPackage(): SupplyPackage
    {
        /** @var SupplyPackage|null $supplyPackage */
        $supplyPackage = $this->entityManager->find(SupplyPackage::class, $this->getParameter('id'));
        if ($supplyPackage === null) {
            $this->dangerFlashMessage('Tento balík neexistuje');
            $this->redirect('default');
        }

        if (!$supplyPackage->isEdited() && !$this->getEmployee()->isShopManager()) {
            $this->dangerFlashMessage('Tento balík nemůžete upravovat');
            $this->redirect('default');
        }

        if (!$supplyPackage->isSent() && !$this->getEmployee()->isWarehouseman() && !$this->getEmployee()->isShopManager()) {
            $this->dangerFlashMessage('Tento balík nemůžete upravovat');
            $this->redirect('default');
        }

        if ($supplyPackage->isSent() && !$this->getEmployee()->isSellerOrDriver()) {
            $this->dangerFlashMessage('Tento balík nemůžete upravovat');
            $this->redirect('default');
        }

        return $supplyPackage;
    }



    private function getWarehouse(): Warehouse
    {
        return $this->getSupplyPackage()->getWarehouse();
    }



    private function fetchLastInventoryCheck(): ?InventoryCheck
    {
        return $this->entityManager->getRepository(InventoryCheck::class)->createQueryBuilder('ic')
            ->andWhere('ic.warehouse = :warehouse')->setParameter('warehouse', $this->getWarehouse())
            ->andWhere('ic.date <= :createdAt')->setParameter('createdAt', $this->getSupplyPackage()->getCreatedAt())
            ->orderBy('ic.date', 'DESC')
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();
    }



    /**
     * @param SupplyPackage[]
     * @return InventoryCheck[]
     */
    private function fetchLastInventoryChecks(array $packages): array
    {
        $inventoryChecks = [];

        foreach ($packages as $package) {
            /** @var InventoryCheck|null $inventoryCheck */
            $inventoryCheck = $this->entityManager->getRepository(InventoryCheck::class)->createQueryBuilder('ic')
                ->andWhere('ic.warehouse = :warehouse')->setParameter('warehouse', $package->getWarehouse())
                ->andWhere('ic.date <= :date')->setParameter('date', $package->getCreatedAt())
                ->orderBy('ic.date', 'DESC')
                ->setMaxResults(1)
                ->getQuery()->getOneOrNullResult();

            if ($inventoryCheck === null) {
                $message = sprintf('LastInventoryCheck does not exist. Supply list ID %s', $package->getId());
                \Tracy\Debugger::log($message, ILogger::ERROR);
            } else {
                $inventoryChecks[$inventoryCheck->getWarehouse()->getId()] = $inventoryCheck;
            }
        }

        return $inventoryChecks;
    }

}
