<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Shops\Services\ShopService;
use App\Entities\SlovakiaVrp\Exceptions\NoMappingException;
use App\Entities\SlovakiaVrp\Services\SlovakiaVrpMappingService;
use App\Forms\BaseForm;
use Nette\Http\FileUpload;
use Nette\Utils\ArrayHash;



class SlovakiaVrpOrderImportPresenter extends BasePresenter
{

    /**
     * @var ShopService
     * @inject
     */
    public $shopService;

    /**
     * @inject
     * @var SlovakiaVrpMappingService
     */
    public $vrpMappingService;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }

        $hasSlovakSubordinateShops = FALSE;
        foreach ($this->shopService->getMySubordinateShops() as $subordinateShop) {
            if ($subordinateShop->isSlovak()) {
                $hasSlovakSubordinateShops = TRUE;
            }
        }

        if (!$hasSlovakSubordinateShops) {
            $this->warningFlashMessage('Nejste RM ani VP slovenských prodejen. Do této sekce nemáte přístup.');
            $this->redirect(':Homepage:default');
        }
    }



    protected function createComponentForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addUpload('upload', 'Rozšířený report z VRP')
            ->setRequired();
        $form->addSubmit('submit', 'Nahrát');

        $form->onSuccess[] = [$this, 'formSuccess'];

        return $form;
    }



    public function formSuccess(BaseForm $form, ArrayHash $values)
    {
        /** @var FileUpload $fileUpload */
        $fileUpload = $values['upload'];

        if ($fileUpload->isOk()) {
            try {
                $this->vrpMappingService->parseAdvancedReport($fileUpload->getTemporaryFile());
                $this->successFlashMessage('Objednávky nahrány');
                $this->redirect('this');

            } catch (NoMappingException $e) {
                $this->dangerFlashMessage(sprintf(
                    'Produkt "%s" nemá nastavené mapování pro prodejnu "%s", prosím přidejte jej a proveďte import znovu.',
                    $e->getVrpProductName(),
                    $e->getShop()->getName()
                ));
            }
        }
    }

}
