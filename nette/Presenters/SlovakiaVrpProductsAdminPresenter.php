<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Products\Addition;
use App\Entities\Products\Product;
use App\Entities\Products\ProductsRepository;
use App\Entities\Shops\Services\ShopService;
use App\Entities\Shops\Shop;
use App\Entities\SlovakiaVrp\Exceptions\UnknownVrpIdException;
use App\Entities\SlovakiaVrp\Services\SlovakiaVrpMappingService;
use App\Entities\SlovakiaVrp\SlovakiaVrpMappingProduct;
use App\Entities\SlovakiaVrp\SlovakiaVrpMappingProductAddition;
use App\Entities\SlovakiaVrp\SlovakiaVrpProduct;
use App\Forms\BaseForm;
use App\WarehouseModule\Forms\SlovakiaVrpProductsAdminMappingForm;
use App\WarehouseModule\Forms\SlovakiaVrpProductsAdminMappingFormInterface;
use Nette\Http\FileUpload;
use Nette\Utils\ArrayHash;



class SlovakiaVrpProductsAdminPresenter extends BasePresenter
{

    /**
     * @var ShopService
     * @inject
     */
    public $shopService;

    /**
     * @inject
     * @var SlovakiaVrpMappingService
     */
    public $vrpMappingService;

    /**
     * @inject
     * @var SlovakiaVrpProductsAdminMappingFormInterface
     */
    public $slovakiaVrpProductsAdminMappingFormInterface;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }

        $hasSlovakSubordinateShops = FALSE;
        foreach ($this->shopService->getMySubordinateShops() as $subordinateShop) {
            if ($subordinateShop->isSlovak()) {
                $hasSlovakSubordinateShops = TRUE;
            }
        }

        if (!$hasSlovakSubordinateShops) {
            $this->warningFlashMessage('Nejste RM ani VP slovenských prodejen. Do této sekce nemáte přístup.');
            $this->redirect(':Homepage:default');
        }
    }



    public function renderDefault()
    {
        $this->template->shops = $this->getShops();
    }



    public function renderDetail(string $id)
    {
        $shop = $this->getShop();
        $this->template->shop = $shop;
    }



    protected function createComponentProductsHistoryReportForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addUpload('upload', 'Export tovaru a služieb')
            ->setRequired();
        $form->addSubmit('submit', 'Nahrát');

        $form->onSuccess[] = [$this, 'productsHistoryReportFormSuccess'];

        return $form;
    }



    public function productsHistoryReportFormSuccess(BaseForm $form, ArrayHash $values)
    {
        /** @var FileUpload $fileUpload */
        $fileUpload = $values['upload'];

        if ($fileUpload->isOk()) {
            try {
                $newProductsCount = $this->vrpMappingService->parseProductsHistoryReport($fileUpload->getTemporaryFile());
                $this->successFlashMessage(sprintf('Import úspěšný. Bylo vytvořeno %d nových produktů. Nezapomeňte zkontrolovat, že se správně mapují.', $newProductsCount));
                $this->redirect('this');

            } catch (UnknownVrpIdException $e) {
                $this->dangerFlashMessage('Nepodařilo se spárovat VRP id s prodejnou v systému. Prosím kontaktujte IT.');
                $this->redirect('this');
            }
        }
    }



    protected function createComponentProductMappingForm(): SlovakiaVrpProductsAdminMappingForm
    {
        $form = $this->slovakiaVrpProductsAdminMappingFormInterface->create($this->getShop());
        $form->onSuccess[] = function () {
            $this->successFlashMessage('Mapování aktualizováno');
            $this->redirect('this');
        };

        return $form;
    }



    /**
     * @return Shop[]
     */
    private function getShops(): array
    {
        $shops = $this->shopService->getMySubordinateShops();
        return array_filter($shops, function (Shop $shop) {
            return $shop->isSlovak();
        });
    }



    private function getShop() : Shop
    {
        /** @var Shop|null $shop */
        $shop = $this->entityManager->getRepository(Shop::class)->findOneBy(['code' => $this->getParameter('id')]);
        if ($shop === null) {
            $this->dangerFlashMessage('Tento obchod neexistuje');
            $this->redirect('default');
        }

        return $shop;
    }

}
