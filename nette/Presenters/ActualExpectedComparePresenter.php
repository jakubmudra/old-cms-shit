<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\Closing;
use App\Entities\Warehouses\ExpectedConsumption;
use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\Services\WarehouseService;
use App\Entities\Warehouses\Warehouse;
use App\Helpers\Unit;
use App\Services\DateService;
use Nette\Utils\DateTime;



class ActualExpectedComparePresenter extends BasePresenter
{

    /**
     * @inject
     * @var WarehouseService
     */
    public $warehouseService;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * Only for caching
     * @var Warehouse[]
     */
    private $warehouses;



    public function actionDefault(string $date = NULL)
    {
        $this->date = DateService::parseDateOrToday($date);

        $this->template->warehouses = $this->fetchWarehouses();
        $this->template->date = $this->date;
        $this->template->dayBefore = $this->date->modifyClone('-1 day');
        $this->template->dayAfter = $this->date->modifyClone('+1 day');
        $this->template->today = (new DateTime())->setTime(0, 0, 0);

        /** @var Ingredient $pastry */
        $pastry = $this->entityManager->find(Ingredient::class, Ingredient::PASTRY_ID);
        $this->template->pastryExpected = $this->fetchExpected($pastry);
        $this->template->pastryActual = $this->fetchActual($pastry);

        /** @var Ingredient $coffee */
        $coffee = $this->entityManager->find(Ingredient::class, Ingredient::COFFEE_ID);
        $this->template->coffeeExpected = $this->fetchExpected($coffee);
        $this->template->coffeeActual = $this->fetchActual($coffee);

        /** @var Ingredient $milk */
        $milk = $this->entityManager->find(Ingredient::class, Ingredient::MILK_ID);
        $this->template->milkExpected = $this->fetchExpected($milk);
        $this->template->milkActual = $this->fetchActual($milk);

        /** @var Ingredient $oil */
        $oil = $this->entityManager->find(Ingredient::class, Ingredient::OIL_ID);
        $this->template->oilExpected = $this->fetchExpected($oil);
        $this->template->oilActual = $this->fetchActual($oil);
    }



    public function actionDetail(int $id)
    {
        $warehouse = $this->getWarehouse();
        $startDate = (new \DateTime())->modify('-7 days')->setTime(0, 0, 0);
        $endDate = (new \DateTime())->modify('-1 day')->setTime(0, 0, 0);

        $this->template->warehouse = $warehouse;

        /** @var Ingredient $pastry */
        $pastry = $this->entityManager->find(Ingredient::class, Ingredient::PASTRY_ID);
        $this->template->pastryExpected = $this->fetchExpectedForWarehouse($warehouse, $pastry, $startDate, $endDate);
        $this->template->pastryActual = $this->fetchActualForWarehouse($warehouse, $pastry, $startDate, $endDate);

        /** @var Ingredient $coffee */
        $coffee = $this->entityManager->find(Ingredient::class, Ingredient::COFFEE_ID);
        $this->template->coffeeExpected = $this->fetchExpectedForWarehouse($warehouse, $coffee, $startDate, $endDate);
        $this->template->coffeeActual = $this->fetchActualForWarehouse($warehouse, $coffee, $startDate, $endDate);

        /** @var Ingredient $milk */
        $milk = $this->entityManager->find(Ingredient::class, Ingredient::MILK_ID);
        $this->template->milkExpected = $this->fetchExpectedForWarehouse($warehouse, $milk, $startDate, $endDate);
        $this->template->milkActual = $this->fetchActualForWarehouse($warehouse, $milk, $startDate, $endDate);

        /** @var Ingredient $oil */
        $oil = $this->entityManager->find(Ingredient::class, Ingredient::OIL_ID);
        $this->template->oilExpected = $this->fetchExpectedForWarehouse($warehouse, $oil, $startDate, $endDate);
        $this->template->oilActual = $this->fetchActualForWarehouse($warehouse, $oil, $startDate, $endDate);

        $this->template->dates = $this->generateDates($startDate, $endDate);
    }



    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return string[]
     */
    private function generateDates(\DateTime $startDate, \DateTime $endDate) : array
    {
        if ($startDate > $endDate) {
            return [];
        }

        $dates = [];

        while ($startDate <= $endDate) {
            $dates[] = $startDate->format('Y-m-d');
            $startDate->modify("+1 day");
        }

        return $dates;
    }



    /**
     * @return int[]
     */
    private function initializeValuesForWarehouses(): array
    {
        $values = [];

        $warehouses = $this->fetchWarehouses();
        foreach ($warehouses as $warehouse) {
            $values[$warehouse->getId()] = 0;
        }

        return $values;
    }



    /**
     * @param Ingredient $ingredient
     * @return int[]
     */
    private function fetchExpected(Ingredient $ingredient) : array
    {
        $values = $this->initializeValuesForWarehouses();

        /** @var ExpectedConsumption[] $result */
        $result = $this->entityManager->getRepository(ExpectedConsumption::class)->createQueryBuilder('ec')
            ->innerJoin('ec.warehouse', 'w')
            ->andWhere('w.shop IS NOT NULL')
            ->andWhere('ec.ingredient = :ingredient')->setParameter('ingredient', $ingredient)
            ->andWhere('ec.date = :date')->setParameter('date', $this->date)
            ->andWhere('ec.warehouse IN (:warehouses)')->setParameter('warehouses', $this->fetchWarehouses())
            ->orderBy('ec.warehouse')
            ->getQuery()->getResult();

        foreach ($result as $expectedConsumption) {
            $warehouseId = $expectedConsumption->getWarehouse()->getId();
            if ($ingredient->getId() === Ingredient::COFFEE_ID) {
                $values[$warehouseId] = round($expectedConsumption->getPieces() * $ingredient->getUnitsPerPieceNetto());
            } else {
                $values[$warehouseId] = round($expectedConsumption->getPieces(), 2);
            }
        }

        return $values;
    }



    /**
     * @param Ingredient $ingredient
     * @return int[]
     */
    private function fetchActual(Ingredient $ingredient) : array
    {
        $values = $this->initializeValuesForWarehouses();

        /** @var Closing[] $result */
        $result = $this->entityManager->getRepository(Closing::class)->createQueryBuilder('c')
            ->innerJoin('c.warehouse', 'w')
            ->andWhere('w.shop IS NOT NULL')
            ->andWhere('c.date = :date')->setParameter('date', $this->date)
            ->andWhere('w IN (:warehouses)')->setParameter('warehouses', $this->fetchWarehouses())
            ->orderBy('c.warehouse')
            ->getQuery()->getResult();

        foreach ($result as $closing) {
            $warehouseId = $closing->getWarehouse()->getId();
            switch ($ingredient->getId()) {
                case Ingredient::PASTRY_ID:
                    $values[$warehouseId] = round($closing->getPastryUsed(Unit::KG), 2);
                    break;

                case Ingredient::COFFEE_ID:
                    $values[$warehouseId] = round($closing->getCoffeeGroundedUsed());
                    break;

                case Ingredient::MILK_ID:
                    $values[$warehouseId] = round($closing->getMilkUsedPieces() + $closing->getMilkUsedPartial() / $ingredient->getUnitsPerPieceNetto(), 2);
                    break;

                case Ingredient::OIL_ID:
                    $values[$warehouseId] = round($closing->getOilUsedPieces() + $closing->getOilUsedPartial() / $ingredient->getUnitsPerPieceNetto(), 2);
                    break;
            }
        }

        return $values;
    }



    /**
     * @param Warehouse $warehouse
     * @param Ingredient $ingredient
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return int[]
     */
    private function fetchExpectedForWarehouse(Warehouse $warehouse, Ingredient $ingredient, \DateTime $startDate, \DateTime $endDate) : array
    {
        $values = [];

        /** @var ExpectedConsumption[] $result */
        $result = $this->entityManager->getRepository(ExpectedConsumption::class)->createQueryBuilder('ec')
            ->andWhere('ec.warehouse = :warehouse')->setParameter('warehouse', $warehouse)
            ->andWhere('ec.ingredient = :ingredient')->setParameter('ingredient', $ingredient)
            ->andWhere('ec.date >= :startDate')->setParameter('startDate', $startDate)
            ->andWhere('ec.date <= :endDate')->setParameter('endDate', $endDate)
            ->orderBy('ec.date')
            ->getQuery()->getResult();

        foreach ($result as $expectedConsumption) {
            if ($ingredient->getId() === Ingredient::COFFEE_ID) {
                $values[] = round($expectedConsumption->getPieces() * $ingredient->getUnitsPerPieceNetto());
            } else {
                $values[] = round($expectedConsumption->getPieces(), 2);
            }
        }

        return $values;
    }



    /**
     * @param Warehouse $warehouse
     * @param Ingredient $ingredient
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return int[]
     */
    private function fetchActualForWarehouse(Warehouse $warehouse, Ingredient $ingredient, \DateTime $startDate, \DateTime $endDate) : array
    {
        $values = [];

        /** @var Closing[] $result */
        $result = $this->entityManager->getRepository(Closing::class)->createQueryBuilder('c')
            ->andWhere('c.warehouse = :warehouse')->setParameter('warehouse', $warehouse)
            ->andWhere('c.date >= :startDate')->setParameter('startDate', $startDate)
            ->andWhere('c.date <= :endDate')->setParameter('endDate', $endDate)
            ->orderBy('c.date')
            ->getQuery()->getResult();

        foreach ($result as $closing) {
            switch ($ingredient->getId()) {
                case Ingredient::PASTRY_ID:
                    $values[] = round($closing->getPastryUsed(Unit::KG), 2);
                    break;

                case Ingredient::COFFEE_ID:
                    $values[] = round($closing->getCoffeeGroundedUsed());
                    break;

                case Ingredient::MILK_ID:
                    $values[] = round($closing->getMilkUsedPieces() + $closing->getMilkUsedPartial() / $ingredient->getUnitsPerPieceNetto(), 2);
                    break;

                case Ingredient::OIL_ID:
                    $values[] = round($closing->getOilUsedPieces() + $closing->getOilUsedPartial() / $ingredient->getUnitsPerPieceNetto(), 2);
                    break;
            }
        }

        return $values;
    }



    private function getWarehouse() : Warehouse
    {
        /** @var Warehouse|null $warehouse */
        $warehouse = $this->entityManager->find(Warehouse::class, $this->getParameter('id'));
        if ($warehouse === null) {
            $this->dangerFlashMessage('Tento sklad/obchod neexistuje');
            $this->redirect('default');
        }

        return $warehouse;
    }



    /**
     * @return Warehouse[]
     */
    private function fetchWarehouses(): array
    {
        if ($this->warehouses === null) {
            $this->warehouses = $this->warehouseService->fetchShopWarehouses();
        }

        return $this->warehouses;
    }

}
