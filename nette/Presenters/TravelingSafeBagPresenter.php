<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Shops\Services\ShopService;
use App\Entities\Warehouses\SafeBag;
use App\Entities\Warehouses\Services\SafeBagService;
use App\Forms\BaseForm;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Utils\ArrayHash;



class TravelingSafeBagPresenter extends BasePresenter
{

    /**
     * @inject
     * @var SafeBagService
     */
    public $safeBagService;

    /**
     * @inject
     * @var ShopService
     */
    public $shopService;

    /**
     * @var Session|SessionSection
     */
    private $sessionSection;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isShopManager() && !$this->getEmployee()->isDriver()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }

        $this->sessionSection = $this->getSession('safebags');
    }



    protected function createComponentFilterForm()
    {
        $form = new BaseForm();
        $form->setMethod('GET');
        $form->addProtection();

        $form->addCheckboxList('statuses', 'Stavy', [
            SafeBag::STATUS_IN_SHOP => 'Na prodejně',
            SafeBag::STATUS_TRAVELING => 'Na cestě',
            SafeBag::STATUS_IN_OFFICE => 'V kanceláři',
            SafeBag::STATUS_STOLEN => 'Ukradený',
            SafeBag::STATUS_MISSING => 'Pohřešovaný',
            SafeBag::STATUS_IN_BANK => 'V bance',
            SafeBag::STATUS_COUNTED => 'Přepočítáno v kanceláři',
        ])->setDefaultValue([SafeBag::STATUS_IN_SHOP]);

        $form->addCheckboxList('shopIds', 'Prodejny', $this->shopService->getMySubordinateShops());

        $form->addSubmit('send', 'Vyfiltrovat');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $this->sessionSection->sent = TRUE;
            $this->sessionSection->shopIds = $values->shopIds;
            $this->sessionSection->statuses = $values->statuses;

            $this->redirect('step2');
        };

        return $form;
    }



    public function actionStep2()
    {
        if ($this->sessionSection->sent === NULL) {
            $this->dangerFlashMessage('Nejprve vyfiltrujte lidi');
            $this->redirect('step1');
        }
    }



    protected function createComponentStep2Form()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addSelect('newStatus', 'Nový stav', [
            SafeBag::STATUS_IN_SHOP => 'Na prodejně',
            SafeBag::STATUS_TRAVELING => 'Na cestě',
            SafeBag::STATUS_IN_OFFICE => 'V kanceláři',
            SafeBag::STATUS_MISSING => 'Pohřešovaný',
            SafeBag::STATUS_STOLEN => 'Ukradený',
            SafeBag::STATUS_IN_BANK => 'V bance',
            SafeBag::STATUS_COUNTED => 'Přepočítáno v kanceláři',
        ])->setPrompt('Vyberte nový stav')
            ->setRequired();

        $moneySafeBags = $this->getMoneySafeBags();
        $papersSafeBags = $this->getPapersSafeBags();

        $form->addCheckboxList('moneySafeBagIds', 'SafeBag s penězy', $moneySafeBags);
        $form->addCheckboxList('papersSafeBagIds', 'Ostatní', $papersSafeBags);

        $form->addSubmit('send', 'Změnit stav');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $newStatus = $values->newStatus;

            try {
                foreach ($values->moneySafeBagIds as $moneySafeBagId) {
                    /** @var SafeBag $safeBag */
                    $safeBag = $this->entityManager->find(SafeBag::class, $moneySafeBagId);
                    $this->safeBagService->markMoneyStatus($safeBag, $newStatus);
                }

                foreach ($values->papersSafeBagIds as $moneySafeBagId) {
                    /** @var SafeBag $safeBag */
                    $safeBag = $this->entityManager->find(SafeBag::class, $moneySafeBagId);
                    $this->safeBagService->markPapersStatus($safeBag, $newStatus);
                }
            } catch (\InvalidArgumentException $e) {
                $form->addError($e->getMessage());
                return;
            }

            $this->entityManager->flush();
            $this->successFlashMessage('Stav změněn');
            $this->redirect('step1');
        };

        return $form;
    }



    private function getMoneySafeBags(): array
    {
        $qb = $this->entityManager->getRepository(SafeBag::class)->createQueryBuilder('sb', 'sb.id')
            ->innerJoin('sb.shop', 's')
            ->innerJoin('s.warehouse', 'w')
            ->andWhere('sb.number IS NOT NULL AND sb.number != :empty')->setParameter('empty', '');


        $shopIds = is_array($this->sessionSection->shopIds) && !empty($this->sessionSection->shopIds) ? $this->sessionSection->shopIds : $this->shopService->getMySubordinateShops();
        $qb->andWhere('sb.shop IN (:shopIds)')->setParameter('shopIds', $shopIds);

        if (is_array($this->sessionSection->statuses) && !empty($this->sessionSection->statuses)) {
            $qb->andWhere('sb.moneyStatus IN (:statuses)')->setParameter('statuses', $this->sessionSection->statuses);
        }

        $qb->orderBy('w.code');
        $qb->addOrderBy('sb.date');

        return $qb->getQuery()->getResult();
    }



    private function getPapersSafeBags(): array
    {
        $qb = $this->entityManager->getRepository(SafeBag::class)->createQueryBuilder('sb', 'sb.id')
            ->innerJoin('sb.shop', 's')
            ->innerJoin('s.warehouse', 'w')
            ->andWhere('sb.number IS NOT NULL AND sb.number != :empty')->setParameter('empty', '');

        $shopIds = is_array($this->sessionSection->shopIds) && !empty($this->sessionSection->shopIds) ? $this->sessionSection->shopIds : $this->shopService->getMySubordinateShops();
        $qb->andWhere('sb.shop IN (:shopIds)')->setParameter('shopIds', $shopIds);

        if (is_array($this->sessionSection->statuses) && !empty($this->sessionSection->statuses)) {
            $qb->andWhere('sb.papersStatus IN (:statuses)')->setParameter('statuses', $this->sessionSection->statuses);
        }

        $qb->orderBy('w.code');
        $qb->addOrderBy('sb.date');

        return $qb->getQuery()->getResult();
    }

}
