<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\Queries\IngredientsQuery;
use App\Entities\Warehouses\Services\ActualExpectedIngredientExpensesAllWarehouses;
use App\Traits\PresenterDateWarehouseFilterForm;
use Kdyby\Doctrine\QueryObject;
use Money\Money;
use Nette\Utils\DateTime;



class ActualExpectedExpensesTablePresenter extends BasePresenter
{

    use PresenterDateWarehouseFilterForm;

    /**
     * @inject
     * @var ActualExpectedIngredientExpensesAllWarehouses
     */
    public $actualExpectedExpenses;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    protected function beforeRender()
    {
        parent::beforeRender();
        $this->template->startDate = $this->startDate;
        $this->template->endDate = $this->endDate;
    }



    public function actionDefault(string $endDate = NULL, string $startDate = NULL, array $warehouseIds = [])
    {
        $this->setDate($endDate, $startDate);
        if ($this->endDate->format('Y-m-d') >= (new \DateTime())->format('Y-m-d')) {
            $this->endDate = (new DateTime())->modify('-1 day')->setTime(0, 0, 0);
        }
        $this->setWarehouses($warehouseIds);

        if ($this->getEmployee()->isManagement()) {
            $this->template->manufactureItems = $this->getItems((new IngredientsQuery())->filterManufactureType());
            $this->template->manufactureItemsSum = $this->sumPrices($this->template->manufactureItems);
        }

        $this->template->cookingItems = $this->getItems((new IngredientsQuery())->filterCookingType());
        $this->template->cleaningItems = $this->getItems((new IngredientsQuery())->filterCleaningType());
        $this->template->otherItems = $this->getItems((new IngredientsQuery())->filterOtherType());

        $this->template->cookingItemsSum = $this->sumPrices($this->template->cookingItems);
        $this->template->cleaningItemsSum = $this->sumPrices($this->template->cleaningItems);
        $this->template->otherItemsSum = $this->sumPrices($this->template->otherItems);
    }



    private function getItems(QueryObject $qb): array
    {
        $ingredients = $this->entityManager->fetch($qb)->toArray();
        return $this->actualExpectedExpenses->getItems($this->startDate, $this->endDate, $this->warehouses, $ingredients);
    }



    private function sumPrices(array $items)
    {
        $sum = [
            'startingStockValue' => Money::CZK(0),
            'endingStockValue' => Money::CZK(0),
            'stockValueChange' => Money::CZK(0),
            'actual' => Money::CZK(0),
            'expected' => Money::CZK(0),
            'difference' => Money::CZK(0),
        ];

        foreach ($items as $item) {
            $sum['startingStockValue'] = $sum['startingStockValue']->add($item['entity']->getPricePerPiece()->multiply($item['startingStock']));
            $sum['endingStockValue'] = $sum['endingStockValue']->add($item['entity']->getPricePerPiece()->multiply($item['endingStock']));
            $sum['actual'] = $sum['actual']->add($item['actualMoney']);
            $sum['expected'] = $sum['expected']->add($item['expectedMoney']);
            $sum['difference'] = $sum['difference']->add($item['differenceMoney']);
        }

        $sum['stockValueChange'] = $sum['endingStockValue']->subtract($sum['startingStockValue']);

        return $sum;
    }

}
