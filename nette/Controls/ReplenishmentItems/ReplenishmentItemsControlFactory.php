<?php

namespace App\WarehouseModule\Controls\ReplenishmentItems;

use App\Entities\Warehouses\Replenishment;



interface ReplenishmentItemsControlFactory
{

    function create(Replenishment $replenishment): ReplenishmentItemsControl;

}
