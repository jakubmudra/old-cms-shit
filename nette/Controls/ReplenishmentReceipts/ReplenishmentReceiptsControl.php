<?php

namespace App\WarehouseModule\Controls\ReplenishmentReceipts;

use App\Entities\Employees\Employee;
use App\Entities\Warehouses\Replenishment;
use App\Entities\Warehouses\ReplenishmentReceiptFile;
use App\Forms\BaseForm;
use App\Security\User;
use Doctrine\ORM\EntityManager;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;
use Nette\Utils\Strings;
use Tracy\ILogger;



/**
 * @method onSuccess(ReplenishmentReceiptsControl $this, string $message)
 * @method onError(ReplenishmentReceiptsControl $this, string $message)
 */
class ReplenishmentReceiptsControl extends Control
{

    /**
     * @var array
     */
    public $onSuccess = [];

    /**
     * @var array
     */
    public $onError = [];

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Employee
     */
    private $employee;

    /**
     * @var Replenishment
     */
    private $replenishment;

    /**
     * @var ILogger
     */
    private $logger;



    public function __construct(
        EntityManager $entityManager,
        ILogger $logger,
        User $user,
        Replenishment $replenishment
    )
    {
        parent::__construct();
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->employee = $user->getEntity();
        $this->replenishment = $replenishment;
    }



    public function render()
    {
        $this->template->replenishment = $this->replenishment;
        $this->template->receipts = $this->entityManager->getRepository(ReplenishmentReceiptFile::class)
            ->findBy(['replenishment' => $this->replenishment], ['uploadedAt' => 'DESC']);
        $this->template->employee = $this->employee;

        $this->template->setFile(__DIR__ . '/default.latte');
        $this->template->render();
    }



    public function handleDeleteReceipt(int $receiptId)
    {
        /** @var ReplenishmentReceiptFile|NULL $receipt */
        $receipt = $this->entityManager->find(ReplenishmentReceiptFile::class, $receiptId);
        if ($receipt === NULL) {
            $this->onError($this, 'Daný bloček neexistuje');
            return;
        }
        $originalName = $receipt->getOriginalName();

        unlink($receipt->getFilePath());
        $this->entityManager->remove($receipt);
        $this->entityManager->flush();

        $lpgMsg = sprintf('Employee %s deleted a receipt %s for replenishment %s', $this->employee, $originalName, $this->replenishment->getId());
        $this->logger->log($lpgMsg, Logger::INFO);
        $this->onSuccess($this, 'Bloček smazán');
    }



    protected function createComponentUploadForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addMultiUpload('files', 'Účtenky')
            ->addRule(Form::IMAGE, 'Účtenka musí být JPEG, PNG nebo GIF.')
            ->addRule(Form::MAX_FILE_SIZE, 'Maximální velikost souboru je 10 MB.', 10 * 1024 * 1024)
            ->setAttribute('accept', 'image/*')
            ->setRequired('Vyberte alespoň jednu účtenku');

        $form->addSubmit('send', 'Nahrát účtenky');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            /** @var \Nette\Http\FileUpload[] $files */
            $files = $values['files'];

            $folder = sprintf('uploaded-receipts/%s/', $this->replenishment->getId());
            foreach ($files as $file) {
                do {
                    $extension = Strings::lower(Strings::substring($file->getSanitizedName(), strrpos($file->getSanitizedName(), ".")));
                    $randomNamePart = uniqid(rand(0, 20), TRUE);
                    $fileName = sprintf('Blocek_%s%s', $randomNamePart, $extension);
                    $serverPath = $folder . $fileName;
                } while (file_exists($serverPath));

                $file->move($serverPath);

                $receipt = new ReplenishmentReceiptFile(
                    $this->replenishment,
                    $file->getSanitizedName(),
                    $serverPath,
                    $this->employee
                );
                $this->entityManager->persist($receipt);
                $this->entityManager->flush();

                $logMsg = sprintf('Employee %s uploaded a receipt %s for replenishment %s',
                    $this->employee, $receipt->getOriginalName(), $this->replenishment->getId());
                $this->logger->log($logMsg);
            }

            $this->onSuccess($this, count($files) > 1 ? 'Účtenky nahrány.' : 'Účtenka nahrána.');
        };

        return $form;
    }

}
