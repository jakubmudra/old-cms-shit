<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use Kdyby\Doctrine\Dql\Join;
use Money\Money;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nextras\Forms\Controls\DatePicker;



class SumReplenishmentPresenter extends BasePresenter
{

    /**
     * @var DateTime
     */
    private $startDate;

    /**
     * @var DateTime
     */
    private $endDate;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isManagement()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    protected function createComponentDateFilterForm()
    {
        $form = new BaseForm();
        $form->setMethod('GET');

        $startDate = new DatePicker();
        $startDate->setValue($this->startDate);
        $form->addComponent($startDate, 'startDate');

        $endDate = new DatePicker();
        $endDate->setValue($this->endDate);
        $form->addComponent($endDate, 'endDate');

        $form->addSubmit('send', 'Vybrat');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $this->redirect('this', [
                'startDate' => $this->startDate->format('Y-m-d'),
                'endDate' => $this->endDate->format('Y-m-d')]
            );
        };

        return $form;
    }



    protected function beforeRender()
    {
        parent::beforeRender();
        $this->template->startDate = $this->startDate;
        $this->template->endDate = $this->endDate;
    }



    public function actionDefault(string $endDate = NULL, string $startDate = NULL)
    {
        if ($endDate === NULL) {
            $this->endDate = (new DateTime())->setTime(0, 0, 0);
        } else {
            $this->endDate = (new DateTime($endDate))->setTime(0, 0, 0);
        }

        if ($startDate === NULL) {
            $this->startDate = $this->endDate->modifyClone('-6 days');
        } else {
            $this->startDate = (new DateTime($startDate))->setTime(0, 0, 0);
        }

        $this->template->warehouses = $this->getWarehouses();
        $this->template->sumReplenishmentTotalPrice = $this->calculateSumReplenishmentTotalPrice();
    }



    public function actionDetail(int $id, string $endDate = NULL, string $startDate = NULL)
    {
        if ($endDate === NULL) {
            $this->endDate = (new DateTime())->setTime(0, 0, 0);
        } else {
            $this->endDate = (new DateTime($endDate))->setTime(0, 0, 0);
        }

        if ($startDate === NULL) {
            $this->startDate = $this->endDate->modifyClone('-6 days');
        } else {
            $this->startDate = (new DateTime($startDate))->setTime(0, 0, 0);
        }

        $warehouse = $this->getWarehouse();
        $this->template->warehouse = $warehouse;
        $this->template->ingredientsSummary = $this->calculateIngredientsSummary($warehouse);
    }



    private function calculateIngredientsSummary(Warehouse $warehouse): array
    {
        $summary = [];

        foreach ($warehouse->getReplenishments() as $replenishment) {
            foreach ($replenishment->getItems() as $replenishmentItem) {
                $ingredient = $replenishmentItem->getIngredient();
                if (!array_key_exists($ingredient->getId(), $summary)) {
                    $summary[$ingredient->getId()] = [
                        'entity' => $ingredient,
                        'amount' => 0,
                        'packages' => 0,
                        'totalPrice' => Money::CZK(0),
                        'replenishmentIds' => [],
                    ];
                }

                $summary[$ingredient->getId()]['amount'] += $replenishmentItem->getPieces();
                $summary[$ingredient->getId()]['packages'] = $summary[$ingredient->getId()]['amount'] / $ingredient->getPiecesPerPackage();
                $summary[$ingredient->getId()]['totalPrice'] = $summary[$ingredient->getId()]['totalPrice']->add($replenishmentItem->getTotalPrice());
                $summary[$ingredient->getId()]['replenishmentIds'][] = $replenishment->getId();
                $summary[$ingredient->getId()]['replenishmentIds'] = array_unique($summary[$ingredient->getId()]['replenishmentIds']);
            }
        }

        ksort($summary);

        return $summary;
    }



    /**
     * @return Warehouse[]
     */
    private function getWarehouses(): array
    {
        return $this->entityManager->getRepository(Warehouse::class)->createQueryBuilder('w')
            ->addSelect('r')
            ->addSelect('ri')
            ->leftJoin('w.replenishments', 'r', Join::WITH, 'r.date >= :startDate AND r.date <= :endDate')
                ->setParameter('startDate', $this->startDate)
                ->setParameter('endDate', $this->endDate)
            ->leftJoin('r.items', 'ri')
            ->orderBy('w.id')
            ->getQuery()->getResult();
    }



    private function getWarehouse(): Warehouse
    {
        $warehouse = $this->entityManager->getRepository(Warehouse::class)->createQueryBuilder('w')
            ->addSelect('r')
            ->addSelect('ri')
            ->leftJoin('w.replenishments', 'r', Join::WITH, 'r.date >= :startDate AND r.date <= :endDate')
                ->setParameter('startDate', $this->startDate)
                ->setParameter('endDate', $this->endDate)
            ->leftJoin('r.items', 'ri')
            ->andWhere('w.id = :warehouse')->setParameter('warehouse', $this->getParameter('id'))
            ->getQuery()->getOneOrNullResult();

        if ($warehouse === NULL) {
            $this->dangerFlashMessage('Tento sklad neexistuje');
            $this->redirect('default');
        }

        return $warehouse;
    }



    private function calculateSumReplenishmentTotalPrice()
    {
        /** @var Money $totalPrice */
        $totalPrice = Money::CZK(0);

        foreach ($this->getWarehouses() as $warehouse) {
            $totalPrice = $totalPrice->add($warehouse->calculateReplenishmentsTotalPrice());
        }

        return $totalPrice;
    }

}
