<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Employees\Login;
use App\Entities\Employees\Pause;
use App\Entities\Shops\Services\ShopService;
use App\Entities\Shops\Shop;
use Nette\Utils\DateTime;



class ShiftDashboardPresenter extends BasePresenter
{

    /**
     * @inject
     * @var ShopService
     */
    public $shopService;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function renderDefault()
    {
        $this->template->dto = $this->createDTO();
    }



    private function createDTO()
    {
        $dto = [];

        foreach ($this->shopService->getMySubordinateShops() as $shop) {
            $shopId = $shop->getId();
            $dto[$shopId]['shop'] = $shop;
            $dto[$shopId]['logins'] = $this->fetchActiveLoginsForShop($shop);
            $dto[$shopId]['pauses'] = $this->fetchActivePausesForShop($shop);
        }

        return $dto;
    }



    /**
     * @param Shop $shop
     * @return Login[]
     */
    private function fetchActiveLoginsForShop(Shop $shop): array
    {
        return $this->entityManager->getRepository(Login::class)->createQueryBuilder('l')
            ->andWhere('l.logoutAtApp IS NULL')
            ->andWhere('l.shop = :shop')->setParameter('shop', $shop)
            ->addOrderBy('l.loginAtApp')
            ->getQuery()->getResult();
    }



    /**
     * @param Shop $shop
     * @return Pause[]
     */
    private function fetchActivePausesForShop(Shop $shop): array
    {
        return $this->entityManager->getRepository(Pause::class)->createQueryBuilder('p')
            ->innerJoin('p.login', 'l')
            ->andWhere('l.logoutAtApp IS NULL')
            ->andWhere('l.shop = :shop')->setParameter('shop', $shop)
            ->andWhere('p.endedAtApp IS NULL')
            ->addOrderBy('p.startedAtApp')
            ->getQuery()->getResult();
    }

}
