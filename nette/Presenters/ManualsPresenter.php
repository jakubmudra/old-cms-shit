<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Shops\Services\ShopService;



class ManualsPresenter extends BasePresenter
{

    /**
     * @inject
     * @var ShopService
     */
    public $shopService;



    public function renderPointOfSale()
    {
        $this->template->shops = $this->shopService->getAllShops();
    }

}
