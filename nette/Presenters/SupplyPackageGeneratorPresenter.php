<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Shops\Shop;
use App\Entities\Warehouses\Services\StockService;
use App\Entities\Warehouses\Services\SupplyPackageService;
use App\Entities\Warehouses\Services\WarehouseService;
use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;



class SupplyPackageGeneratorPresenter extends BasePresenter
{

    /**
     * @inject
     * @var WarehouseService
     */
    public $warehouseService;

    /**
     * @inject
     * @var StockService
     */
    public $stockService;

    /**
     * @inject
     * @var SupplyPackageService
     */
    public $supplyPackageService;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function actionDefault()
    {
        $this->template->dto = $this->getDto();
    }



    protected function createComponentGenerateForm() : BaseForm
    {
        $form = new BaseForm();

        $form->addCheckboxList('warehouseIds', 'Sklady', $this->getWarehouses())
            ->setRequired('Vyberte alespoň jeden sklad');

        $form->addSubmit('submit', 'Generovat');
        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $warehouseIds = $values['warehouseIds'];

            foreach ($warehouseIds as $warehouseId) {
                /** @var Warehouse|NULL $warehouse */
                $warehouse = $this->entityManager->find(Warehouse::class, $warehouseId);
                if ($warehouse === NULL) {
                    $this->dangerFlashMessage('Sklad s Id ' . $warehouseId . ' nebyl nalezen! Prosím informujte o tom IT.');
                    $this->redirect('this');
                }

                $stock = $this->stockService->calculateStockInWarehouse($warehouse, (new \DateTime())->setTime(0, 0, 0));
                $this->entityManager->persist($stock);
                $this->entityManager->flush($stock);

                $supplyPackage = $this->supplyPackageService->generateSupplyPackage($warehouse, $stock);
                $this->entityManager->persist($supplyPackage);
            }

            $this->entityManager->flush();
            $this->successFlashMessage('Vygenerováno');
            $this->redirect(':Warehouse:SupplyPackages:default');
        };

        return $form;
    }



    private function getDto(): array
    {
        $dto = $this->initializeDto();
        $this->addLastOrders($dto);
        $this->addLastClosings($dto);
        $this->addLastInventoryChecks($dto);
        return $dto;
    }



    private function initializeDto(): array
    {
        $dto = [];

        foreach ($this->getWarehouses() as $warehouseId => $warehouse) {
            $dto[$warehouseId] = [
                'shop' => $warehouse->getShop(),
                'lastOrderAt' => NULL,
                'lastClosingAt' => NULL,
                'lastInventoryCheckAt' => NULL,
            ];
        }

        return $dto;
    }



    /**
     * @param array $dto - from $this->initializeDto()
     */
    private function addLastOrders(array &$dto)
    {
        $lastOrders = $this->entityManager->getRepository(Shop::class)->createQueryBuilder('s', 's.id')
            ->select('s.id, MAX(o.createdAtCashDesk) AS lastOrderAt')
            ->innerJoin('s.orders', 'o')
            ->andWhere('s.deleted = FALSE')
            ->groupBy('s.id')
            ->having('lastOrderAt IS NOT NULL')
            ->getQuery()->getResult();

        foreach ($lastOrders as $warehouseId => $lastOrder) {
            if (!isset($dto[$warehouseId])) {
                continue;
            }

            $dto[$warehouseId]['lastOrderAt'] = DateTime::from($lastOrder['lastOrderAt']);
        }
    }



    /**
     * @param array $dto - from $this->initializeDto()
     */
    private function addLastClosings(array &$dto)
    {
        $lastClosings = $this->entityManager->getRepository(Warehouse::class)->createQueryBuilder('w', 'w.id')
            ->select('w.id, GREATEST(MAX(c.morningFilledAt), MAX(c.eveningFilledAt)) AS lastClosingAt')
            ->innerJoin('w.closings', 'c')
            ->innerJoin('w.shop', 's')
            ->andWhere('s.deleted = FALSE')
            ->groupBy('w.id')
            ->having('lastClosingAt IS NOT NULL')
            ->getQuery()->getResult();

        foreach ($lastClosings as $warehouseId => $lastClosing) {
            if (!isset($dto[$warehouseId])) {
                continue;
            }

            $dto[$warehouseId]['lastClosingAt'] = DateTime::from($lastClosing['lastClosingAt']);
        }
    }



    /**
     * @param array $dto - from $this->initializeDto()
     */
    private function addLastInventoryChecks(array &$dto)
    {
        $lastInventoryChecks = $this->entityManager->getRepository(Warehouse::class)->createQueryBuilder('w', 'w.id')
            ->select('w.id, MAX(i.filledAt) AS lastInventoryCheckAt')
            ->innerJoin('w.inventoryChecks', 'i')
            ->innerJoin('w.shop', 's')
            ->andWhere('s.deleted = FALSE')
            ->groupBy('w.id')
            ->having('lastInventoryCheckAt IS NOT NULL')
            ->getQuery()->getResult();

        foreach ($lastInventoryChecks as $warehouseId => $lastInventoryCheck) {
            if (!isset($dto[$warehouseId])) {
                continue;
            }

            $dto[$warehouseId]['lastInventoryCheckAt'] = DateTime::from($lastInventoryCheck['lastInventoryCheckAt']);
        }
    }



    /**
     * @return Warehouse[]
     */
    private function getWarehouses()
    {
        $warehouses = [];

        foreach ($this->warehouseService->getMySubordinateWarehouses() as $warehouse) {
            if ($warehouse->getShop() !== NULL) {
                $warehouses[$warehouse->getId()] = $warehouse;
            }
        }

        return $warehouses;
    }

}
