<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\Ingredient;
use App\Entities\Warehouses\InventoryCheck;
use App\Entities\Warehouses\Manufacture;
use App\Entities\Warehouses\Queries\IngredientsQuery;
use App\Entities\Warehouses\Replenishment;
use App\Entities\Warehouses\Transfer;
use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use Money\Money;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nextras\Forms\Controls\DatePicker;



class ReplenishmentDeliveryComparePresenter extends BasePresenter
{

    /**
     * @var DateTime
     */
    private $startDate;

    /**
     * @var DateTime
     */
    private $endDate;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isManagement()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    protected function createComponentDateFilterForm()
    {
        $form = new BaseForm();
        $form->setMethod('GET');

        $startDate = new DatePicker();
        $startDate->setValue($this->startDate);
        $form->addComponent($startDate, 'startDate');

        $endDate = new DatePicker();
        $endDate->setValue($this->endDate);
        $form->addComponent($endDate, 'endDate');

        $form->addSubmit('send', 'Vybrat');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $this->redirect('this', [
                    'startDate' => $this->startDate->format('Y-m-d'),
                    'endDate' => $this->endDate->format('Y-m-d')]
            );
        };

        return $form;
    }



    public function actionDefault(string $endDate = null, string $startDate = null)
    {
        if ($endDate === null) {
            $this->endDate = (new DateTime())->setTime(0, 0, 0);
        } else {
            $this->endDate = (new DateTime($endDate))->setTime(0, 0, 0);
        }

        if ($startDate === null) {
            $this->startDate = $this->endDate->modifyClone('-6 days');
        } else {
            $this->startDate = (new DateTime($startDate))->setTime(0, 0, 0);
        }

        $dto = $this->createDTO();
        $this->setStartInventoryCheck($dto);
        $this->setEndInventoryCheck($dto);
        $this->addReplenishments($dto);
        $this->addTransfers($dto);
        $this->addManufacture($dto);
        $this->calculateDifference($dto);

        $this->template->ingredientDTO = $dto;
        $this->template->summary = $this->createSummary($dto);
        $this->template->startDate = $this->startDate;
        $this->template->endDate = $this->endDate;
    }



    private function createDTO() : array
    {
        $dto = [];

        foreach ($this->fetchIngredients() as $ingredient) {
            $dto[$ingredient->getId()] = [
                'entity' => $ingredient,
                'startInventoryCheck' => 0.0,
                'endInventoryCheck' => 0.0,
                'replenishment' => 0.0,
                'transfers' => 0.0,
                'shrink' => 0.0,
                'manufacture' => 0.0,
                'difference' => 0.0,
                'differencePrice' => Money::CZK(0),
            ];
        }

        return $dto;
    }



    private function setStartInventoryCheck(array &$dto)
    {
        /** @var InventoryCheck|null $inventoryCheck */
        $inventoryCheck = $this->entityManager->getRepository(InventoryCheck::class)->createQueryBuilder('ic')
            ->addSelect('ici')
            ->innerJoin('ic.items', 'ici')
            ->andWhere('ic.date = :startDate')->setParameter('startDate', $this->startDate)
            ->andWhere('ic.warehouse = :warehouse')->setParameter('warehouse', Warehouse::CENTRAL_WAREHOUSE_ID)
            ->getQuery()->getOneOrNullResult();

        if ($inventoryCheck === null) {
            $this->dangerFlashMessage('V počáteční den neexistuje inventura, prosím zvolte jiné datum.');
            return;
        }

        foreach ($inventoryCheck->getItems() as $inventoryCheckItem) {
            $ingredient = $inventoryCheckItem->getIngredient();
            if (isset($dto[$ingredient->getId()])) {
                $dto[$ingredient->getId()]['startInventoryCheck'] = $inventoryCheckItem->calculateTotalPieces();
            }
        }
    }



    private function setEndInventoryCheck(array &$dto)
    {
        /** @var InventoryCheck|null $inventoryCheck */
        $inventoryCheck = $this->entityManager->getRepository(InventoryCheck::class)->createQueryBuilder('ic')
            ->addSelect('ici')
            ->innerJoin('ic.items', 'ici')
            ->andWhere('ic.date = :endDate')->setParameter('endDate', $this->endDate)
            ->andWhere('ic.warehouse = :warehouse')->setParameter('warehouse', Warehouse::CENTRAL_WAREHOUSE_ID)
            ->getQuery()->getOneOrNullResult();

        if ($inventoryCheck === null) {
            $this->dangerFlashMessage('V konečný den neexistuje inventura, prosím zvolte jiné datum.');
            return;
        }

        foreach ($inventoryCheck->getItems() as $inventoryCheckItem) {
            $ingredient = $inventoryCheckItem->getIngredient();
            if (isset($dto[$ingredient->getId()])) {
                $dto[$ingredient->getId()]['endInventoryCheck'] = $inventoryCheckItem->calculateTotalPieces();
            }
        }
    }



    /**
     * @param array
     * @return void
     */
    private function addTransfers(array &$dto)
    {
        foreach ($this->fetchIngredients() as $ingredient) {
            /** @var Transfer[] $transfers */
            $transfers = $this->entityManager->getRepository(Transfer::class)->createQueryBuilder('t')
                ->andWhere('t.oldWarehouse = :oldWarehouse')->setParameter('oldWarehouse', Warehouse::CENTRAL_WAREHOUSE_ID)
                ->andWhere('t.ingredient = :ingredient')->setParameter('ingredient', $ingredient->getId())
                ->andWhere('t.date >= :startDate')->setParameter('startDate', $this->startDate)
                ->andWhere('t.date < :endDate')->setParameter('endDate', $this->endDate)
                ->getQuery()->getResult();

            foreach ($transfers as $transfer) {
                $type = $transfer->isShrink() ? 'shrink' : 'transfers';
                $dto[$ingredient->getId()][$type] += $transfer->getPieces();
            }
        }
    }



    /**
     * @param array
     * @return void
     */
    private function addReplenishments(array &$dto)
    {
        /** @var Replenishment[] $replenishments */
        $replenishments = $this->entityManager->getRepository(Replenishment::class)->createQueryBuilder('r')
            ->addSelect('ri')
            ->innerJoin('r.items', 'ri')
            ->andWhere('r.date >= :startDate')->setParameter('startDate', $this->startDate)
            ->andWhere('r.date < :endDate')->setParameter('endDate', $this->endDate)
            ->andWhere('r.warehouse = :warehouse')->setParameter('warehouse', Warehouse::CENTRAL_WAREHOUSE_ID)
            ->getQuery()->getResult();

        foreach ($replenishments as $replenishment) {
            foreach ($replenishment->getItems() as $replenishmentItem) {
                $ingredient = $replenishmentItem->getIngredient();
                $dto[$ingredient->getId()]['replenishment'] += $replenishmentItem->getPieces();
            }
        }
    }



    private function addManufacture(array &$dto)
    {
        /** @var Manufacture[] $manufactures */
        $manufactures = $this->entityManager->getRepository(Manufacture::class)->createQueryBuilder('m')
            ->andWhere('m.date >= :startDate')->setParameter('startDate', $this->startDate)
            ->andWhere('m.date < :endDate')->setParameter('endDate', $this->endDate)
            ->andWhere('m.warehouse = :warehouse')->setParameter('warehouse', Warehouse::CENTRAL_WAREHOUSE_ID)
            ->getQuery()->getResult();

        foreach ($manufactures as $manufacture) {
            /** @var Ingredient $ingredient */
            $ingredient = $this->entityManager->find(Ingredient::class, Ingredient::MIXTURE_ID);
            $dto[$ingredient->getId()]['manufacture'] += $manufacture->getProducedMixture() / $ingredient->getUnitsPerPieceNetto();

            /** @var Ingredient $ingredient */
            $ingredient = $this->entityManager->find(Ingredient::class, Ingredient::FLOUR_ID);
            $dto[$ingredient->getId()]['manufacture'] -= $manufacture->getUsedFlour() / $ingredient->getUnitsPerPieceNetto();

            /** @var Ingredient $ingredient */
            $ingredient = $this->entityManager->find(Ingredient::class, Ingredient::BELINIE_ID);
            $dto[$ingredient->getId()]['manufacture'] -= $manufacture->getUsedBelinie() / $ingredient->getUnitsPerPieceNetto();

            /** @var Ingredient $ingredient */
            $ingredient = $this->entityManager->find(Ingredient::class, Ingredient::MIXTURE_BAGS_ID);
            $dto[$ingredient->getId()]['manufacture'] -= $manufacture->getUsedBags() / $ingredient->getUnitsPerPieceNetto();

            /** @var Ingredient $ingredient */
            $ingredient = $this->entityManager->find(Ingredient::class, Ingredient::SK_TAPE_ID);
            $dto[$ingredient->getId()]['manufacture'] -= $manufacture->getUsedTapes() / $ingredient->getUnitsPerPieceNetto();
        }
    }



    private function calculateDifference(array &$dto)
    {
        foreach ($dto as $ingredientId => &$data) {
            $data['difference'] = $data['startInventoryCheck'] + $data['replenishment'] + $data['manufacture'] - $data['transfers'] - $data['shrink'] - $data['endInventoryCheck'];
            $data['differencePrice'] = $data['entity']->getPricePerPiece()->multiply($data['difference']);
        }
    }



    /**
     * @return Ingredient[]
     */
    private function fetchIngredients(): array
    {
        $qb = (new IngredientsQuery(TRUE));
        /** @var Ingredient[] $ingredients */
        $ingredients = $this->entityManager->fetch($qb)->toArray();

        return $ingredients;
    }



    private function createSummary(array $dto): array
    {
        $summary = [
            'wrong' => Money::CZK(0),
            'found' => Money::CZK(0),
            'lost' => Money::CZK(0),
            'sum' => Money::CZK(0),
        ];

        foreach ($dto as $item) {
            $summary['wrong'] = $summary['wrong']->add($item['differencePrice']->absolute());
            $summary['sum'] = $summary['sum']->add($item['differencePrice']);

            if ($item['difference'] < 0) {
                $summary['found'] = $summary['found']->add($item['differencePrice']);
            }

            if ($item['difference'] > 0) {
                $summary['lost'] = $summary['lost']->add($item['differencePrice']);
            }
        }

        return $summary;
    }

}
