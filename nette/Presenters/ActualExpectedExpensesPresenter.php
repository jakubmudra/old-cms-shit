<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Warehouses\Services\ActualExpectedExpensesSummary;
use App\Entities\Warehouses\Services\ActualExpectedExpensesDetailed;
use App\Entities\Warehouses\Services\WarehouseService;
use App\Entities\Warehouses\Warehouse;
use App\Forms\BaseForm;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nextras\Forms\Controls\DatePicker;



class ActualExpectedExpensesPresenter extends BasePresenter
{

    /**
     * @inject
     * @var WarehouseService
     */
    public $warehouseService;

    /**
     * @inject
     * @var ActualExpectedExpensesDetailed
     */
    public $actualExpectedExpensesDetailed;

    /**
     * @inject
     * @var ActualExpectedExpensesSummary
     */
    public $actualExpectedExpensesSummary;

    /**
     * @var DateTime
     */
    private $startDate;

    /**
     * @var DateTime
     */
    private $endDate;

    /**
     * Only for caching
     * @var Warehouse[]
     */
    private $warehouses;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    protected function createComponentDateFilterForm()
    {
        $form = new BaseForm();
        $form->setMethod('GET');

        $startDate = new DatePicker();
        $startDate->setValue($this->startDate);
        $form->addComponent($startDate, 'startDate');

        $endDate = new DatePicker();
        $endDate->setValue($this->endDate);
        $form->addComponent($endDate, 'endDate');

        $form->addSubmit('send', 'Vybrat');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            $this->redirect('this', [
                'startDate' => $this->startDate->format('Y-m-d'),
                'endDate' => $this->endDate->format('Y-m-d')]
            );
        };

        return $form;
    }



    protected function beforeRender()
    {
        parent::beforeRender();
        $this->template->startDate = $this->startDate;
        $this->template->endDate = $this->endDate;
    }



    public function actionDefault(string $endDate = NULL, string $startDate = NULL)
    {
        $this->initDates($endDate, $startDate);

        $this->template->warehouses = $this->fetchWarehouses();
        $this->template->dto = $this->actualExpectedExpensesSummary->getItems($this->startDate, $this->endDate);
    }



    public function actionDetail(int $id, string $endDate = NULL, string $startDate = NULL)
    {
        $this->initDates($endDate, $startDate);

        $this->template->warehouse = $this->getWarehouse();
        $this->template->items = $this->actualExpectedExpensesDetailed->getItems($this->getWarehouse(), $this->startDate, $this->endDate);
    }



    private function getWarehouse() : Warehouse
    {
        /** @var Warehouse|null $warehouse */
        $warehouse = $this->entityManager->find(Warehouse::class, $this->getParameter('id'));
        if ($warehouse === null) {
            $this->dangerFlashMessage('Tento sklad/obchod neexistuje');
            $this->redirect('default');
        }

        return $warehouse;
    }



    /**
     * @param string|NULL $endDate
     * @param string|NULL $startDate
     */
    private function initDates(string $endDate = NULL, string $startDate = NULL)
    {
        if ($endDate === NULL) {
            $this->endDate = (new DateTime())->modify('-1 day')->setTime(0, 0, 0);
        } else {
            $this->endDate = (new DateTime($endDate))->setTime(0, 0, 0);
        }

        if ($startDate === NULL) {
            $this->startDate = $this->endDate->modifyClone('-7 days');
        } else {
            $this->startDate = (new DateTime($startDate))->setTime(0, 0, 0);
        }
    }



    /**
     * @return Warehouse[]
     */
    private function fetchWarehouses(): array
    {
        if ($this->warehouses === null) {
            $this->warehouses = $this->warehouseService->getMySubordinateWarehouses();
        }

        return $this->warehouses;
    }

}
