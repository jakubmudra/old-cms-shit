<?php

namespace App\WarehouseModule\Presenters;

use App\Entities\Shops\LockCodeLog;
use App\Entities\Shops\Services\ShopService;
use App\Entities\Shops\Shop;
use App\Forms\BaseForm;
use Nette\Utils\ArrayHash;



class ShopLockCodesPresenter extends BasePresenter
{

    /**
     * @inject
     * @var ShopService
     */
    public $shopService;



    public function startup()
    {
        parent::startup();
        if (!$this->getEmployee()->isDriver() && !$this->getEmployee()->isShopManager()) {
            $this->warningFlashMessage('Do této sekce nemáte povolený přístup');
            $this->redirect(':Homepage:default');
        }
    }



    public function renderDefault()
    {
        $this->template->lockCodeLogs = $this->entityManager->getRepository(LockCodeLog::class)->createQueryBuilder('lcl')
            ->andWhere('lcl.shop IN (:shops)')->setParameter('shops', $this->shopService->getMySubordinateShops())
            ->orderBy('lcl.createdAt', 'DESC')
            ->setMaxResults(50)
            ->getQuery()->getResult();
    }



    protected function createComponentShowLockCodeForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addSelect('shopId', 'Prodejna', $this->getShops())
            ->setPrompt('Vyberte prodejnu')
            ->setRequired('Prosím vyberte prodejnu');

        $form->addTextArea('reason', 'Důvod')
            ->setAttribute('placeholder', 'Vyplňte proč potřebujete znát kód této prodejny.')
            ->setRequired('Prosím vyplňte důvod');

        $form->addSubmit('send', 'Zobrazit');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            /** @var Shop|NULL $shop */
            $shop = $this->entityManager->find(Shop::class, $values->shopId);

            $lockCodeLog = new LockCodeLog($shop, $this->getEmployee(), $values->reason);
            $this->entityManager->persist($lockCodeLog);
            $this->entityManager->flush();

            if ($shop === NULL) {
                $this->warningFlashMessage('Tato prodejna neexistuje');
                $this->redirect('this');
            }

            $this->template->shop = $shop;
        };

        return $form;
    }



    protected function createComponentChangeLockCodeForm()
    {
        $form = new BaseForm();
        $form->addProtection();

        $form->addSelect('shopId', 'Prodejna', $this->shopService->getMySubordinateShops())
            ->setPrompt('Vyberte prodejnu')
            ->setRequired('Prosím vyberte prodejnu');

        $form->addText('code', 'Nový kód')
            ->setRequired('Prosím vyplňte důvod');

        $form->addSubmit('send', 'Změnit');

        $form->onSuccess[] = function (BaseForm $form, ArrayHash $values) {
            /** @var Shop|NULL $shop */
            $shop = $this->entityManager->find(Shop::class, $values->shopId);

            if ($shop === NULL) {
                $this->warningFlashMessage('Tato prodejna neexistuje');
                $this->redirect('this');
            }

            $shop->setLockCode($values->code);
            $this->entityManager->flush();
            $this->successFlashMessage(sprintf('Kód u prodejny %s změněn', $shop));
            $this->redirect('this');
        };

        return $form;
    }



    /**
     * @return Shop[]
     */
    private function getShops(): array
    {
        return $this->entityManager->getRepository(Shop::class)->createQueryBuilder('s', 's.id')
            ->andWhere('s.deleted = FALSE')
            ->orderBy('s.code')
            ->getQuery()->getResult();
    }

}
